#!/usr/bin/env python3
"""[Testing: krock robot handle with recorded data from cpg controller]
"""
import os 
import time 
import sys

"""@note: In case unable to find ps3joy 
    - robothandle object files should be created inside build
    - build folder of the project should be part of LD library path 
    - make sure the path is visible/reflected in the current terminal

"""

import robothandle
import numpy as np
import ctypes
from tqdm import tqdm


GLOBAL_CONFIG_FNAME="config/GLOBAL.cfg"
KROCK_CONTROLLER_DIR=os.path.dirname(os.path.abspath(__file__)).split('/tests')[0]


def test_krock_on_off():
    """[Test:   switch robot on and off. RobotHandle when created initialises the robot to 
                its zero position and when destructor is called the robot again goes to zero position
                then servos are switched off]

    """
    global GLOBAL_CONFIG_FNAME

    # create a global config object
    krock_global_config = robothandle.GlobalConfig()
    config_path = os.path.join(KROCK_CONTROLLER_DIR, GLOBAL_CONFIG_FNAME)
    assert os.path.isfile(config_path)
    config_real_success = krock_global_config.readFromFile(config_path)
    
    try:
        print('Switching on the robot..')
        # create robot handle and set max speed
        # @note please do not skip max speed, at the time of writing speed was too high
        robot_handle = robothandle.RobotHandle(krock_global_config)
        robot_handle.setMaxSpeed(100)
        # use val(21, numpy) to set the position other than zero positon
        #robot_handle.setAllPositionsThreadV(val)
    except KeyboardInterrupt:
        return
    print('Switching off the robot..')
    # after the function ends, destructor of robot_handle is called (goes out of scope)
    return

def test_cpg_commands(
    file_path='cpg_out_formatted_for_krock.txt',
    iteration_timestep=0.05,
    activate=np.ones(21, dtype=float)):
    global GLOBAL_CONFIG_FNAME
    
    # create a global config object
    krock_global_config = robothandle.GlobalConfig()
    config_path = os.path.join(KROCK_CONTROLLER_DIR, GLOBAL_CONFIG_FNAME)
    assert os.path.isfile(config_path)
    config_real_success = krock_global_config.readFromFile(config_path)
    
    
    try:
        cpg_vals = np.loadtxt(file_path)
        print('Switching on the robot..')
        # create robot handle and set max speed
        # @note please do not skip max speed, at the time of writing speed was too high
        robot_handle = robothandle.RobotHandle(krock_global_config)
        robot_handle.setMaxSpeed(100)

        pv_time = time.time()
        with tqdm(total=len(cpg_vals)) as pbar:
            for i, cpg_val in tqdm(enumerate(cpg_vals)):
                st_time = time.time()
                dt_time = st_time - pv_time
                if dt_time < iteration_timestep:
                    time.sleep(iteration_timestep - dt_time)
                val = np.multiply(cpg_val, activate)
                robot_handle.setAllPositionsThreadV(val)
                pv_time = st_time
                pbar.update(1)
    except KeyboardInterrupt:
        return
    
    print('Switching off the robot..')
    return 

def main():
    # # First: uncomment the below line to test switching on off of the robot 
    # test_krock_on_off()
    # use a larger iteration step to slow down the comand calls 
    test_cpg_commands(iteration_timestep=0.01)
    return 
    



if __name__ == '__main__':
    TIC = time.time()
    main()