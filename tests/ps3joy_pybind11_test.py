#!/usr/bin/env python3
"""[PS3 controller button test code]
"""
import sys
import os

"""@note: In case unable to find ps3joy 
    - ps3joy object files should be created inside build
    - build folder of the project should be part of LD library path 
    - make sure the path is visible/reflected in the current terminal

"""

import time
import ps3joy as ps3joy
import curses



def main():
    """[Main function to call the main loop of the controller]
    """
    #stdscr = curses.initscr()
    JOYSTICK_SHARED_MEMORY="ps3shm"
    JOYSTICK_DEVNAME="/dev/input/js0"

    # using curses to print the values at the same location
    
    #print(JOYSTICK_SHARED_MEMORY)
    ps3joy_obj = ps3joy.PS3joy(JOYSTICK_SHARED_MEMORY)

    stopJoystickThread = True
    joystick_plugged = False
    while stopJoystickThread:
        try:
            if not joystick_plugged:
                joystick_plugged = ps3joy_obj.OpenDevice(JOYSTICK_DEVNAME)
                print(joystick_plugged)
            else:
                ps3joy_obj.GetInput()
                ps3joy_obj.WriteSharedMemory()
                print(ps3joy_obj.joyStruct.axes[0])

                # stdscr prints each line at the same location (given by first two ints) in the terminal 
                # using ljust to left jutify data with some padding, ensuring proper overwite of the data 
                # stdscr.addstr(0, 0, "axes (0) Lx: " + str(ps3joy_obj.joyStruct.axes[0]).ljust(10))
                # stdscr.addstr(1, 0, "axes (1) Ly: " + str(ps3joy_obj.joyStruct.axes[1]).ljust(10))
                # stdscr.addstr(2, 0, "axes (2) L2: " + str(ps3joy_obj.joyStruct.axes[2]).ljust(10))
                # stdscr.addstr(3, 0, "axes (3) Rx: " + str(ps3joy_obj.joyStruct.axes[3]).ljust(10))
                # stdscr.addstr(4, 0, "axes (4) Ry: " + str(ps3joy_obj.joyStruct.axes[4]).ljust(10))
                # stdscr.addstr(5, 0, "axes (5) R2: " + str(ps3joy_obj.joyStruct.axes[5]).ljust(10))
                # stdscr.addstr(6, 0, "buttons (0) : " + str(ps3joy_obj.joyStruct.buttons[0]).ljust(10))
                # stdscr.addstr(7, 0, "buttons (1) : " + str(ps3joy_obj.joyStruct.buttons[1]).ljust(10))
                # stdscr.addstr(8, 0, "buttons (2) : " + str(ps3joy_obj.joyStruct.buttons[2]).ljust(10))
                # stdscr.addstr(9, 0, "buttons (3) : " + str(ps3joy_obj.joyStruct.buttons[3]).ljust(10))
                # stdscr.addstr(10, 0, "buttons (4) : " + str(ps3joy_obj.joyStruct.buttons[4]).ljust(10))
                # stdscr.addstr(11, 0, "buttons (5) : " + str(ps3joy_obj.joyStruct.buttons[5]).ljust(10))
                # stdscr.addstr(12, 0, "buttons (6) : " + str(ps3joy_obj.joyStruct.buttons[6]).ljust(10))
                # stdscr.addstr(13, 0, "buttons (7) : " + str(ps3joy_obj.joyStruct.buttons[7]).ljust(10))
                # stdscr.addstr(14, 0, "buttons (8) : " + str(ps3joy_obj.joyStruct.buttons[8]).ljust(10))
                # stdscr.addstr(15, 0, "buttons (9) : " + str(ps3joy_obj.joyStruct.buttons[9]).ljust(10))
                # stdscr.addstr(16, 0, "buttons (10) : " + str(ps3joy_obj.joyStruct.buttons[10]).ljust(10))
                # stdscr.addstr(17, 0, "buttons (11) : " + str(ps3joy_obj.joyStruct.buttons[11]).ljust(10)) 
                # stdscr.addstr(18, 0, "buttons (12) : " + str(ps3joy_obj.joyStruct.buttons[12]).ljust(10))
                # stdscr.refresh()
            time.sleep(0.5)
        except Exception as e:
            print(e)
            break 
    #curses.endwin()
    return 


if __name__ == '__main__':
    TIC = time.time()
    main()