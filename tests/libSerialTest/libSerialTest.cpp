#include <iostream>
#include <libserial/SerialStream.h>

/**
 * @brief test if libserial is correctly imported and working
 * 
 * @note Use "g++ -Wall -lserial libSerialTest.cpp" to compile this code
 *          If it gives error then need to rectify it
 * @note Use "ldconfig -p | grep libserial" to find libserial's .so files
 * 
 */

using namespace LibSerial;

int main() {
    SerialStream sStream;
    sStream.Open("/dev/ttyUSB0");
    sStream.Close();
}
