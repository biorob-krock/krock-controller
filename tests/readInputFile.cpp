/**
 * @file readInputFile.cpp
 * @author Astha Gupta (astha73@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-11-06
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <iostream>
#include <string>
#include <vector>

#include <Utils/fileReader.hpp>
#include <BoostLogger/boostlogger.hpp>

///@todo write this in CMake's test or add a gcc command for check here


bool testloader(){
    std::stringstream configFilePath;
    configFilePath << PROJECT_DIRECTORY << "/data/input/testNumArrayListRead.txt";
    std::vector<std::vector<float>> test = FileReader::read2DMatrix(configFilePath.str().c_str(), ',');
    LOG_INFO << "test complete" <<  std::endl;
    LOG_INFO << test <<  std::endl;
    return true;
}

int main(int argc, char* argv[]){
    LOG_INFO << "test complete 1" <<  std::endl;
    bool out = testloader();
}