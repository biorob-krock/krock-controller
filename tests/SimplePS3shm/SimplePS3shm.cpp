#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>
#include "utils.hpp"
#include <unistd.h>
#include <SerialStream.h>
#include "ps3joy.hpp"
#include <boost/array.hpp>
#include <boost/asio.hpp>

/** 
 * @brief Test joystick functions with this script
 * 
 * @todo rename the script and its executable
 */

using namespace std;
using boost::asio::ip::udp;

bool isRunning(const char* name)  
{  
	char command[32];  
	sprintf(command, "pgrep %s > /dev/null", name);  
	return 0 == system(command);  
} 


int main(){
	bool joystick_plugged;
	
	PS3joy ps3joy((const char*)"ps3shm");
	joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");
	ps3joy.WriteSharedMemory(); printf("written to a shm\n");


	// init times
	const double Td=20/1000.;
	double t=0, dt, t0, t1, t2;
	t0=get_timestamp();
	while(1){

		dt=get_timestamp()-t0;
		t0=get_timestamp();
		t=t+dt;

		//========= Try to connect if not connected ================
		if(!joystick_plugged){
			joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");	
			cout << " Not connected " << endl; 
		}
		else{

			//========= READ PS3 JOYSTICK ================
			ps3joy.GetInput();
			ps3joy.WriteSharedMemory();
			ps3joy.PrintInput();

			//========= RUN or STOP robotController PROGRAM IF START IS PRESSED ================
			if(ps3joy.joyStruct.start_pressed && !ps3joy.joyStruct.start_pressed_old){
				if(isRunning("robot_main")) {
					printf("MAIN IS RUNNING. STOPING IT NOW!\n");
					 //A process having name robot_main is running.
				}
				else{
					printf("MAIN IS NOT RUNNING. STARTING IT NOW!\n");
					system("cd /home/odroid/PROJECTS/SingleLegController; ./robot_main & ");
				  //A process having name robot_main is NOT running.

				}
			}

			//========= KILL PROGRAM  ================
			if(ps3joy.joyStruct.start_pressed && !ps3joy.joyStruct.start_pressed_old && ps3joy.joyStruct.select_pressed){
				system("pkill robot_main");
			}


			//========= TURN OFF ODROID  ================	
	        if(ps3joy.joyStruct.LR_pressed && ps3joy.joyStruct.select_pressed && !ps3joy.joyStruct.select_pressed_old){
				system("shutdown -h now");
			}
		}	


		//=========================== TIME STEP ===============================
		t1=get_timestamp();
		cout.flush();
		if(Td>(t1-t0)){
		    sleepBoost(Td- (t1-t0));
		}
		t2=get_timestamp();
		
	}

	return 0;
}































