/**
 * @file timeStamp.hpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief Class declaration for AddTimeStamp
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#ifndef TIMESTAMP_HPP
#define TIMESTAMP_HPP

#include <streambuf>

/**
 * @brief Add Timestamp to stdio automatically 
 * 
 * @details Pass basic_io object like cout, clog etc to append timestamp to them
 * 
 */
class AddTimeStamp : public std::streambuf
{
public:
    AddTimeStamp( std::basic_ios< char >& out );
    ~AddTimeStamp();
protected:
    int_type overflow( int_type m);
private:
    // add copy constructor and assignement operator to make object not copyable
    // copy constructor 
    AddTimeStamp( const AddTimeStamp& );
    // copy assignment operator 
    AddTimeStamp& operator=( const AddTimeStamp& );
    // --   Members
    std::basic_ios< char >& out_;
    std::streambuf* sink_;
    bool newline_;
};

#endif
