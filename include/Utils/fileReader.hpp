/**
 * @file fileReader.hpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief Class declaration File Read and Write 
 * @version 0.1
 * @date 2021-02-08
 * 
 * @todo Change filename to reflect read & write 
 * 
 */
#ifndef FILEREADER_HPP
#define FILEREADER_HPP

#include <vector>

/// @todo take a template of int, float, double as input 

/**
 * @brief Class to read and write 2D matrix data
 * 
 * @todo Change filename to reflect read & write 
 * 
 */
class FileReader{
    public:
        static std::vector<std::vector<float>> read2DMatrix(const char* fileAbsPath, char delimeter);
        static bool write2DMatrix(const char* fileAbsPath,char delimeter, std::vector<std::vector<double>> data);

};

#endif