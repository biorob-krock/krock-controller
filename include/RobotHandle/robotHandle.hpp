/**
 * @file robotHandle.hpp
 * @author Legacy
 * @brief Header declarartion for RobotHandle class
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#ifndef ROBOTHANDLE_HPP
#define ROBOTHANDLE_HPP

// cpp
#include <thread>  
#include <mutex> 

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include "Eigen/SVD"

#include <unistd.h>
#include <libgen.h>

// lib headers
#include "vectornav.h"
#include "dxsystem/dxmotorsystem.h"
#include "dxsystem/setup.h"
#include "dxsystem/additionaltools.h"
#include "dxsystem/dxsystem.h"
#include "dxsystem/control_table_constants.h"

#include <RobotComm/ps3joy.hpp>
#include <RobotComm/robotStatus.hpp>
#include <Utils/utils.hpp>
#include <RobotConfig/global_config.hpp>

#define IS_RADIAN true
#define IS_DEGREE false
#define ANGLE_TYPE IS_RADIAN

using namespace boost::interprocess;
using namespace Eigen;

/* Configuration and numbering */
#define MAX_NUM_SERVOS  30

enum{HEAD, SP1, SP2, SP3, SP4, SP5, SP6, SP7, SP8, SP9, SP10,
    LF_YAW, LF_PITCH, LF_ROLL, LF_ELBOW,
    RF_YAW, RF_PITCH, RF_ROLL, RF_ELBOW,
    LH_YAW, LH_PITCH, LH_ROLL, LH_KNEE,
    RH_YAW, RH_PITCH, RH_ROLL, RH_KNEE};

/**
 * @brief Handle the robot from this class and its functions 
 * 
 * @todo 1. Create a global config reader 
 * 
 * @details Dependency
 * 
 *  1.  DXSystem (superclass): contains basic configutaion of the system of motors 
 *      and objects for modified USB2Dynamixel SD$
 *  2.  DXMotorSystem (subclass): adds function for common motor control
 * 
 * @note: uniform initalisation {} is used in the code, instead of default ()
 * 
 */
class RobotHandle
{
    public:
        // adds functions for common motor control 
        // unable to create an empty declaration
        DXMotorSystem dx_motors_sys{0,1, "somethingRandom"};
        // DXMotorSystem dx_motors_sys;
        int numMotors;

        /* Motors' centers */
        double d_center[MAX_NUM_SERVOS];
        double d_posture[MAX_NUM_SERVOS];
        double d_speed[MAX_NUM_SERVOS];

    	int init_max_speed = 200; 
	    int init_max_torque = 300; 

        std::thread robot_comm_thread;

        vector<int> used_IDs;
        int ids_arr[MAX_NUM_SERVOS];

        double fbck_angles[MAX_NUM_SERVOS];
        double fbck_current[MAX_NUM_SERVOS];
        double angles[MAX_NUM_SERVOS];

        bool readPosition, readCurrent, writeAngles, stopThread;

        // shared memories
        // RobotStatus rStatus{(const char*)"rstatus2unity_shm", (const char*)"rstatus2unity_shm"};
        // managed_shared_memory ssshm{open_or_create, "sensorsStruct_shm", 1024};
        // SensorsStruct *sensorsStruct_shm, sensorsStruct;
        RobotStatus rStatus;
        GlobalConfig globalConfigRH;

        // std::vector<int>  motorIDs;
        // std::vector<bool> enabledMotors;
        // int  nbMotors;

        //Robot(int, int, int*);
        // RobotHandle(int speed, int torque, int numMotorsConfig, int used_ids[30], int enabled_ids[30], const char *portName);
        // RobotHandle(std::vector<int> motorIDs, std::vector<bool> enabledMotors, int  nbMotors);
        RobotHandle(GlobalConfig &inGlobalConfig);
        ~RobotHandle();

        bool initMotorSystem(int maxSpeed, int maxTorqueIn, int numMotorsConfig, int usedIDs[30], int enabledMotorIDs[30], const char *portName);
        void initSharedMemory();

        void readIMU(double *imudata);
        void readForceSensors(double *forcedata, double *forcedataRaw);
        void writeRobotStatus(MatrixXd joint_angles, MatrixXd fbck_torques, MatrixXd forRPY, MatrixXd force);

        void setSinglePosition(int, double);
        void setSingleTorque(int, int);
        void setAllPositions(double* angles, const vector<int>& ids);
        void setAllPositionsThread(double* new_angles);
        void setAllPositionsThreadV(vector<double> new_angles);
        void setAllTorques(double *torques, const vector<int>& ids);
        void setTorqueControl(int *torque_ctrl_on, const vector<int>& ids);
        void InitialPosture(int*);
        void setMaxTorque(int);
        void setTorqueLimit(int torque);
        void setMaxSpeed(int);
        void TorqueMode(int*, int*);
        void ServosOn(int*);
        void ServosOff(int*);
        void setAllDelay(int val);
        int  getSinglePosition(int);
        void getAllPositions(double *angles);
        void getAllPositionsThread(double *angles);
        void getIDsPositions(double *angles, const vector<int>& usedIDs);
        void getTorqueEnable();
        void setAllGainP(int Pgain);
        int  translateTorque(double torque);
        void getAllCurrents(double *currents);
        void getAllCurrentsThread(double *currents);
    
};

#endif

















































