/**
 * @file structDefs.hpp
 * @author Legacy
 * @brief Structure definition for gaitParams
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#ifndef STRUCTDEFS_HPP
#define STRUCTDEFS_HPP

#include "Eigen/Dense"
#include "Eigen/Geometry"
#include <stdlib.h>


/**
 * @brief gaitParams struct definition
 * 
 */
struct gaitParams {
	Eigen::Matrix<double, 2, 1> spineCPGscaling;
  	Eigen::Matrix<double, 2, 1> Duty;
    Eigen::Matrix<double, 4, 1> phShifts;
	Eigen::Matrix<double, 3, 4> midStance;
	Eigen::Matrix<double, 1, 4> ellipse_a, ellipse_b;
	Eigen::Matrix<double, 1, 4> swing_height, swing_width;
	
    
    Eigen::Matrix<double, 3, 4> nSurf;
    Eigen::Matrix<double, 3, 4> nLO, nTD;

	Eigen::Matrix<double, 3, 2>  bezierScaling;
	double tSclSwing;
 	
 	Eigen::Matrix<double, 4, 4> qNULL;


};






#endif