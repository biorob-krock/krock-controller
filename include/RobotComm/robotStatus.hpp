/**
 * @file robotStatus.hpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief RobotStatus header declarartions for handling and write for robot's peripherals
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#ifndef ROBOTSTATUS_HPP
#define ROBOTSTATUS_HPP

#include <stdlib.h>
#include <fstream>
#include <vector>
#include <string.h>
#include <iostream>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

using namespace std;
using namespace boost::interprocess;

/**
 * @brief Class to encapsulate all the data structure for Robot's health
 * 
 * @details RobotStatus is responsible to manage status update of the robot peripherals. 
 * RobotStatus encapsulate the data structure related to the robot's health. 
 * 
 */
class RobotStatus
{
	public:	
		#pragma pack(push, 1)  // exact fit - no padding
		/**
		 * @brief struct definition for Robot's status 
		 * 
		 * @details This struct definition should contain only motor status. 
		 * Robot status are written by robotComm thread (thread started by robotComm function)
		 * 
		 * @todo Have only motors in robot status. Keep the motor status and sensor status separate
		 * Maybe it would be better if we rename this stuct to motorstatus?
		 * 
		 * 
		 */
		struct RobotStatusStruct{
			float motor_positions[27];
			float motor_torques[27];
			float imu[3];
			float forces[12];
			bool motorsOn;
		};

		/**
		 * @brief struct definition for Sensors's status
		 * 
		 * @details This struct definition should contain only sensor status. 
		 * Sensors status is written to by comm_slave executable created from RobotStartup script. 
		 * 
		 * @todo - Remove optoforce code and its dependency as they are no longer being used. 
		 * 
		 */
		struct SensorStatusStruct{
			bool optoforce_plugged[4]={0};
			double optoforce[12]={0};
			unsigned short int optoforceRaw[16]={0};
			bool imu_plugged=0;
			double imu[3]={0};
		};
		#pragma pack(pop) //back to whatever the previous packing mode was

		// functions
		// RobotStatus(const char *robotStatusShmName, const char *sensorStatusShmName, bool skipHardware);
		RobotStatus();
		RobotStatus(const char *robotStatusShmName, const char *sensorStatusShmName, bool skipHardware);
		// void WriteSharedMemory();
		// void ReadSensorMemory();
		void WriteToRobotSharedMemory();
		void ReadFromRobotSharedMemory();
		void WriteToSensorSharedMemory();
		void ReadFromSensorSharedMemory();
		// void CreateSharedMemory(const char *sharedMemoryName, bool destruct);

		// variables
		RobotStatusStruct		robot_status_struct;
		SensorStatusStruct		sensor_status_struct;

		// shared memory object (shm)
		RobotStatusStruct 		*robot_status_struct_shm;
		SensorStatusStruct 		*sensor_status_struct_shm;

		// bool flags 
		bool skip_hardware;

	private:
		// shared memory manager objects
		managed_shared_memory 	robot_shm_manager;
		managed_shared_memory 	sensor_shm_manager;

};

#endif
