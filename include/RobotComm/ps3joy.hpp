/**
 * @file ps3joy.hpp
 * @author Legacy
 * @brief PS3joy header declarations
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#ifndef PS3JOY_HPP
#define PS3JOY_HPP

#include "joystick.h"
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <string.h>
#include <iostream>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

using namespace std;
using namespace boost::interprocess;

/** 
 * @brief PS3joy Class for taking controller inputs and 
 * RobotStatus class with shared memory variables
 */
class PS3joy
{
	public:	
		#pragma pack(push, 1)  // exact fit - no padding
		/**
		 * @brief Struct def for handling data from joystick 
		 * 
		 */
		struct JoyStruct
		{
			float axes[6];
			int buttons[16];
			bool start_pressed;
			bool start_pressed_old;
			bool select_pressed;
			bool select_pressed_old;
			bool LR_pressed;
		};	
		#pragma pack(pop) //back to whatever the previous packing mode was
		// functions
		PS3joy(const char *sharedMemoryName);
		
		bool IsReady();
		// JoyStruct GetInput();
		/// @note: GetInput() converted to void for pybind11 
		void GetInput();
		void PrintInput();
		void SetInput(JoyStruct joyData);
		bool OpenDevice(char * name);
		/// @note: Open and create shared memory bot being used 
		// void OpenSharedMemory(const char * name);
		// void CreateSharedMemory(const char *sharedMemoryName, bool destruct);
		void WriteSharedMemory();
		void ReadSharedMemory();

		// variables
		JoyStruct joyStruct;
	private:
		// functions
		managed_shared_memory shm;

		// variables
		Joystick js;
		js_event event;
		JoyStruct *joyStruct_shm;

		float joy_x1, joy_x2, joy_y1, joy_y2, joy_x3, joy_y3;
	    int joy_l1, joy_l2, joy_l3, joy_r1, joy_r2, joy_r3, joy_sel, joy_start, joy_bD, joy_bL, joy_bR, joy_bU;
	    //double joy_lsr, joy_rsr;
	    //double joy_lsphi, joy_rsphi;

};

#endif
