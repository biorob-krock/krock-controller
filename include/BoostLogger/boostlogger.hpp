#ifndef BOOSTLOGGER_HPP
#define BOOSTLOGGER_HPP

#include <boost/log/trivial.hpp>
#include <boost/log/sources/global_logger_storage.hpp>

/**
 * @file boostlogger.hpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-10-30
 * 
 * @brief Logger (based on boost) to simply replace cout
 * 
 * Standalone header and source files
 * 
 * @param LOGFILE: logfile name
 * @param SEVERITY_THRESHOLD: threshold of the output shown 
 * 
 * @todo Set logfile name and severity by taking inout from the user
 */
#define LOGFILE "log/logfile.log"
#define SEVERITY_THRESHOLD logging::trivial::info

BOOST_LOG_GLOBAL_LOGGER(boostlogger, boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level>)

#define LOG_TRACE   BOOST_LOG_SEV(boostlogger::get(),boost::log::trivial::trace)
#define LOG_DEBUG   BOOST_LOG_SEV(boostlogger::get(),boost::log::trivial::debug)
#define LOG_INFO    BOOST_LOG_SEV(boostlogger::get(),boost::log::trivial::info)
#define LOG_WARNING BOOST_LOG_SEV(boostlogger::get(),boost::log::trivial::warning)
#define LOG_ERROR   BOOST_LOG_SEV(boostlogger::get(),boost::log::trivial::error)
#define LOG_FATAL   BOOST_LOG_SEV(boostlogger::get(),boost::log::trivial::fatal)

#endif