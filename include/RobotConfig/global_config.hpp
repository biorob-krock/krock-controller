/**
 * @file global_config.hpp
 * @author Legacy
 * @brief Header declaration for GlobalConfig
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#ifndef GLOBAL_CONFIG_HPP
#define GLOBAL_CONFIG_HPP

#include <vector>
#include <libconfig.h++>

/**
 * @brief Load and store config/GLOBAL.cfg configurations
 * 
 */
class GlobalConfig
{ 

public:
    GlobalConfig();

    bool readFromFile(std::string configPath);

    // Mode
    bool isSimulation();
    bool isOptimization();

    // Inputs
    bool useJoystick();
    int getJoystickMode();
    bool isAutoRun();
    int getAutoRunState();
    bool highLevelControllerEnabled();

    // Robot variants
    bool useTail();
    bool useIMU();
    bool useROS();

    bool swim();
    bool spineCompensatedFeedback();
    bool useReflexes();

    // Motor configuration
    int nbMotors();
    int nbMotorsTrunk();
    int nbLegs();
    int nbMotorsLegs();
    int nbMotorsTail();
    int nbMotorsNeck();

    std::vector<int> getMotorsIDs(); // IDs for Dynamixel motors
    std::vector<bool> getEnabledMotors();

    bool allMotorsDisabled();
    int maxTorque();
    int maxSpeed();

    // Logging
    bool logData();
    std::string getLogDescription();

    // Debug prints
    bool statusPrints();
    bool mattPrints();
    bool configPrints();
    bool walkPrints();
    bool swimPrints();
    bool skipHardware();
    bool printOverwrite(); 

private:
    // GLOBAL CONFIG
    bool m_isSimulation;
    bool m_isOptimization;

    bool m_useJoystick;
    int m_joystickMode;
    bool m_isAutoRun;
    int m_autoRunState;
    bool m_highLevelControllerEnabled;

    bool m_useTail;
    bool m_useIMU;
    bool m_useROS;

    bool m_swim;
    bool m_spineCompensationFeedback;
    bool m_useReflexes;

    int m_nbMotors;
    int m_nbMotorsTrunk;
    int m_nbLegs;
    int m_nbMotorsLegs;
    int m_nbMotorsTail;
    int m_nbMotorsNeck;

    std::vector<int> m_motorIDs;
    std::vector<bool> m_enabledMotors;

    bool m_allMotorsDisabled;
    int m_maxTorque;
    int m_maxSpeed;

    bool m_logData;
    std::string m_logDescription;

    bool m_statusPrints;
    bool m_mattPrints;
    bool m_configPrints;
    bool m_walkPrints;
    bool m_swimPrints;
    bool m_skipHardware; 
    bool m_printOverwrite; 
};

// Global variable defined in .cpp implementation
extern GlobalConfig globalConfig;

#endif // GLOBAL_CONFIG_HPP