/**
 * @file fileReader.cpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief 	Class definition for File Read and Write
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <stdlib.h>

#include <boost/algorithm/string.hpp>

#include <Utils/fileReader.hpp>

/**
 * @brief  Read a 2d matrix stored in a txt file with a delimeter 
 * 
 * @param fileAbsPath 
 * @param delimeter 
 * @return std::vector<std::vector<float>> 
 */
std::vector<std::vector<float>> FileReader::read2DMatrix(const char* fileAbsPath,char delimeter){
    // std::ifstream is RAII, i.e. no need to call close
    std::ifstream in_file(fileAbsPath);
    std::vector<std::vector<float>> vector_2d;
    if (in_file.is_open()){
        std::string line;
        while(getline(in_file, line)){
            line.erase(
                std::remove_if(line.begin(),line.end(),isspace),
                line.end()
            );
            
            if(line[0] == '#' || line.empty()) continue;
            std::istringstream ss_line(line);
            std::string number;
            std::vector<float> numbers;
            while(getline(ss_line, number, delimeter)){
                numbers.push_back(std::stof(number));

            }
            vector_2d.push_back(numbers);
        }
        return vector_2d;
    }
    else {
        std::cerr << "Couldn't open config file for reading.\n";
        return vector_2d;
    }
}

/**
 * @brief Write to a file given filepath, delimeter and data 
 * 
 * @param fileAbsPath 
 * @param delimeter 
 * @param data 
 * @return true 
 * @return false 
 */
bool FileReader::write2DMatrix(const char* fileAbsPath,char delimeter, std::vector<std::vector<double>> data){
    std::ofstream out_file(fileAbsPath);
    for(int i = 0; i < data.size(); i++){
        for(int j = 0; j < data[i].size() -1; j++){
            out_file << data[i][j] << ",";
        }
        out_file << data[i].size() -1 << "\n";
    } 
    out_file.close();
    return true;
}