/**
 * @file timeStamp.cpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief Class definition for AddTimeStamp
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <ostream>
#include <cassert>

#include <sstream>
#include <chrono>
#include <ctime>
#include <iomanip>

#include <fstream>
#include <Utils/utils.hpp>
#include <Utils/timeStamp.hpp>

/**
 * @brief Construct AddTimeStamp object
 * 
 * @param std::basic_ios<char> 
 */
AddTimeStamp:: AddTimeStamp( std::basic_ios< char >& out )
    : out_(out)
    , sink_()
    , newline_(true)
{
    sink_ = out_.rdbuf(this);
    assert( sink_ );
}

/**
 * @brief Destroy the Add Time Stamp:: Add Time Stamp object
 * 
 */
AddTimeStamp::~AddTimeStamp()
{
    out_.rdbuf( sink_ );
}

/**
 * @brief overriding stream buffer overlow function
 * 
 * @param m 
 * @return std::streambuf::int_type 
 */
std::streambuf::int_type AddTimeStamp::overflow( int_type m = traits_type::eof() )
{
    if( traits_type::eq_int_type( m, traits_type::eof() ) )
        return sink_->pubsync() == -1 ? m: traits_type::not_eof(m);
    if( newline_ )
    {
        std::ostream str( sink_ );
        if( !(str <<  get_timestamp_formated(timestamp_format::LOG) ) )
            return traits_type::eof(); // Error
    }
    newline_ = traits_type::to_char_type( m ) == '\n';
    return sink_->sputc( m );
}