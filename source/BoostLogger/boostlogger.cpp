#include <BoostLogger/boostlogger.hpp>
#include <boost/log/core/core.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <fstream>
#include <ostream>

/**
 * @file boostlogger.cpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-10-30
 * 
 * Standalone header and source files
 * 
 * @brief Logger (based on boost) to simply replace cout
 * 
 */
namespace logging = boost::log;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace trivialtype = logging::trivial;

BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", trivialtype::severity_level)

/// @brief Initialize boost global logger
BOOST_LOG_GLOBAL_LOGGER_INIT(boostlogger, src::severity_logger_mt<trivialtype::severity_level>){
    
    src::severity_logger_mt<trivialtype::severity_level> lg;

    // attribute's value
    lg.add_attribute("TimeStamp", attrs::local_clock());

    // sink backend - logfile and console
    typedef sinks::synchronous_sink<sinks::text_ostream_backend> text_sink;
    boost::shared_ptr<text_sink> sink = boost::make_shared<text_sink>();
    sink->locked_backend()->auto_flush(true);
    sink->locked_backend()->add_stream(boost::shared_ptr<std::ostream>(new std::ofstream(LOGFILE)));
    sink->locked_backend()->add_stream(boost::shared_ptr<std::ostream>(&std::clog, boost::null_deleter()));

    // format and filtering 
    logging::formatter formatter = expr::stream
        << expr::format_date_time(timestamp, "%Y-%m-%d, %H:%M:%S.%f") << " "
        << "[" << trivialtype::severity << "]"
        << " - " << expr::smessage;
    sink->set_formatter(formatter);
    sink->set_filter(severity >= SEVERITY_THRESHOLD);

    // add sink to logger 
    logging::core::get()->add_sink(sink);

    return lg;
}