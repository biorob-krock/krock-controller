/**
 * @file main.cpp
 * @author Legacy
 * @brief Krock webot simulation's main function
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#define OPTIMIZATION2
//#include <webots/Supervisor.hpp>
//#include <webots/Servo.hpp>
#include <webots/Robot.hpp>
#include <webots/Gyro.hpp>
#include <webots/Camera.hpp>
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include <iostream>
#include <cmath>
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include <ctime>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include <atomic>
#include <LegacyController/controller.hpp>
#include <LegacyController/robotSim.hpp>
#include <RobotComm/joystick.h>
//#include "MLP.hpp"
#include <RobotConfig/global_config.hpp>
#include <Utils/fileReader.hpp>

#define TIME_STEP   4
#define JOYSTICK_SHARED_MEMORY "ps3shm"
#define JOYSTICK_DEVNAME "/dev/input/js0"

#define ROS_NODE_NAME "krock2"
#define GLOBAL_CONFIG_FNAME "/config/GLOBAL_WEBOTS.cfg"

using namespace std;
using namespace Eigen;
using namespace webots;
using namespace libconfig;

void joystickLoop();
std::atomic<bool> stopJoystickThread(false);

/**
 * @brief Main function: links the controller and robot classes
 * 
 * @param argc 
 * @param argv 
 * @return int 
 * 
 * @note PROJECT_DIRECTORY: global variable, taken from CMakeList.txt 
 *  
 */
int main(int argc, char **argv)
{
    std::cout << "Starting main" << std::endl;
    std::thread joystickThread;

    std::stringstream configFilePath;
    configFilePath << PROJECT_DIRECTORY << GLOBAL_CONFIG_FNAME;

    if (globalConfig.readFromFile(configFilePath.str()))
    {
        std::cout << "Succesfully read config file." << std::endl;
    }
    else
    {
        std::cout << "Unable to read global config. Aborting." << std::endl;
        return EXIT_FAILURE;
    }


    // Start joystick thread if used
    if (globalConfig.useJoystick() && (globalConfig.getJoystickMode() == 0 || globalConfig.getJoystickMode() == 1))
    {   
        joystickThread = std::thread(joystickLoop);
        joystickThread.detach();
    }

    /// @todo: to maintain compatibility we cast a vector to a static array.
    /// However, We might as well directly provide a vector to the functions.
    int enabledMotors[100];
    for (int i = 0; i < globalConfig.getEnabledMotors().size(); i++)
    {
        enabledMotors[i] = globalConfig.getEnabledMotors()[i];
    }

    cout<<"MAIN STARTS"<<endl;
    double Td = TIME_STEP/1000.;
    double table_p[30], table_t[30];



    //======================================================//

    RobotSim robotSim(TIME_STEP);
    cout<<"ROBOTSIM CREATED"<<endl;
    robotSim.InitIMU();

    Controller controller(Td);
    cout<<"CONTROLLER CREATED"<<endl;

    controller.printConfiguration(); 


    if (globalConfig.useROS())
    {
        cout << "using ROS: node name: " << ROS_NODE_NAME << endl;
        controller.startROS(argc, argv, ROS_NODE_NAME);
    }


    double t=0, t_tot=0, freq=0.5;
    int k=1;

    double t0, t1, t2;
    double tt0, tt1, tt2, dt;
    int numsteps=0;
    tt0=get_timestamp();
    tt1=get_timestamp();
    tt2=get_timestamp();

    VectorXd torques(30), position(30), force(12);
    MatrixXd JFL(3,4), JFR(3,4), JHL(3,4), JHR(3,4), tmp(3,3);
    double d_posture[30], d_torques[30], compassData[3], IMUdata[3], gpsDataF[3], gpsDataH[3], ts_data[12+12], forcedataRaw[16], markers[4];
    double fgird_rotMat[9];
    double FL_feet_gpos[3], FR_feet_gpos[3], HL_feet_gpos[3], HR_feet_gpos[3], globalCoM[3];
    int count=0;

    MatrixXd globalPoly(3,4);
    bool robotMoved = false;


    // Recording data from webots    
    // std::vector<std::vector<double>> data_angles;
    // std::stringstream ssDataFileAbsPath;
    // ssDataFileAbsPath << PROJECT_DIRECTORY << "/log/angles_webots_50000.txt";
    // std::string dataFileAbsPath = ssDataFileAbsPath.str();
    // int recod_count = 20000;

//=============================  LOOP  =============================================
    while(robotSim.step(TIME_STEP) != -1) 
    {
        // Update counter for Joint Commands
        // recod_count = recod_count -1;

        dt=get_timestamp()-tt0;
        tt0=get_timestamp();
        dt=Td;
        t=t+dt;

        if(t>1.3 && !robotMoved){
            //double p[3]={-1, -0.1, -0.0657};  // walls
            double p[3]={-1, -0.1, -1.23292};  // pipe test
            //robotSim.setPositionOfRobot(p);

            robotMoved=true;
        }

        controller.setTimeStep(dt);
        //controller.getCoM().transpose();

        robotSim.getPositionTorques(d_posture, d_torques);
        robotSim.ReadIMUAttitude(fgird_rotMat);
        robotSim.ReadTouchSensors(ts_data);
        //robotSim.GetCompass(compassData);
        
        robotSim.GetPosition(gpsDataF, gpsDataH);
        robotSim.GetFeetGPS(FL_feet_gpos, FR_feet_gpos, HL_feet_gpos, HR_feet_gpos);

        controller.getAttitude(fgird_rotMat);
        controller.getGPS(gpsDataF, FL_feet_gpos, FR_feet_gpos, HL_feet_gpos, HR_feet_gpos);
        controller.updateRobotState(d_posture, d_torques);

        controller.getForce(ts_data, forcedataRaw);

        //controller.GetCompass(compassData);
        if(!controller.runStep(robotSim.getTime())){
            break;
        }
        controller.getAngles(table_p);
        // std::vector<double> table_p_vec(std::begin(table_p), std::end(table_p));
        // std::cout << "[Test]: "  << table_p_vec << std::endl;
        // data_angles.push_back(table_p_vec);


        //cout << table_p << endl;
        // cout << table_p.size() << endl;
        controller.getGlobalCoM(globalCoM);
        //globalPoly=controller.getGlobalSupportPoly();

        robotSim.setAngles(table_p, enabledMotors);
        //robotSim.setCoMmarker(globalCoM);
        //robotSim.setSupportPoly(globalPoly, controller.legs_stance, 0.001);

        //controller.sendStatusToMatlab(5, "128.178.148.33");

        //===============================================================================================//
        
        tt1=get_timestamp ();
        //cout<<(tt1 - tt0) <<"s"<<"\t"<<(tt1 - tt0) * 1000<<"ms"<<"\t\t"<<endl;
        if(Td>(tt1-tt0)){
       //     usleep((Td- (tt1-tt0))*1000000 );
        }

        tt2=get_timestamp();

        if(count%3==0){
            //sendUDP(&vel, 2*sizeof(double), "128.178.148.59", 8472);
        }
        
        // Break from recording commands
        // cout << "here1: "  << recod_count << endl;
        // if(recod_count == 0) break;


    }

    stopJoystickThread = true;

    // Wait for end of joystick thread
    if (joystickThread.joinable())
    {
        cout << "Joinable" << endl;
        stopJoystickThread = true;
        joystickThread.join();
    }
    else{
        cout << "not Joinable" << endl;
        stopJoystickThread = true;
    }


    // Write Joint commands
    // cout << "here2" << endl;
    // FileReader::write2DMatrix(dataFileAbsPath.c_str(), ',', data_angles);
    
    
    //controller.mpc_thread.join();
    //controller.fdo_thread.join();

//==============================================================================
  return EXIT_SUCCESS;
}

/**  
 * @brief Thread reading the inputs from the joystick
 */
void joystickLoop()
{
    shared_memory_object::remove(JOYSTICK_SHARED_MEMORY);
    PS3joy ps3joy((const char*)JOYSTICK_SHARED_MEMORY);

    bool joystick_plugged = false;

    while (!stopJoystickThread)
    {
		
		if(!joystick_plugged)
        {
        	joystick_plugged = ps3joy.OpenDevice((char*)JOYSTICK_DEVNAME);

            if (joystick_plugged)
                cout << "Succesfully initialized joystick" << endl;
            else
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
        else
        {
            ps3joy.GetInput();
            ps3joy.WriteSharedMemory();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(5000));
}
