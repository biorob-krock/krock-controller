/**
 * @file global_config.cpp
 * @author Legacy
 * @brief Class definitions for GlobalConfig
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <RobotConfig/global_config.hpp>

#include <iostream>

// Global config variable
GlobalConfig globalConfig;


GlobalConfig::GlobalConfig()
{
/** @brief Class holding robot parameters used throughout the code  
 *  
 */

}

bool GlobalConfig::readFromFile(std::string configPath)
{
/** @brief Import global configuration parameters from .cfg file   
 *  
 * Dependent upon libconfig library for parsing .cfg files
 */
    libconfig::Config cfg;

    // Read the file. If there is an error, report it and exit.
    try
    {
        cfg.readFile((configPath.c_str()));
    }
    catch(const libconfig::FileIOException &fioex)
    {
        std::cerr << "I/O error while reading global config file: " 
                  << configPath << std::endl;

        return false;
    }
    catch(const libconfig::ParseException &pex)
    {
        std::cerr << "Parse error while reading global config file at " 
                  << pex.getFile() << ":" << pex.getLine()
                  << " - " << pex.getError() << std::endl;

        return false;
    }

    // Extract config settings
    try
    {
        m_isSimulation = cfg.lookup("IS_SIMULATION");
        m_isOptimization = cfg.lookup("IS_OPTIMIZATION");

        m_useJoystick = cfg.lookup("USE_JOYSTICK");
        m_joystickMode = cfg.lookup("JOYSTICK_MODE");
        m_isAutoRun = cfg.lookup("AUTO_RUN");
        m_autoRunState = cfg.lookup("AUTO_RUN_STATE");
        m_highLevelControllerEnabled = cfg.lookup("ENABLE_HIGH_LEVEL_CONTROLLER");

        m_useTail = cfg.lookup("USE_TAIL");
        m_useIMU = cfg.lookup("USE_IMU");
        m_useROS = cfg.lookup("USE_ROS");

        m_swim = cfg.lookup("SWIM");
        m_spineCompensationFeedback = cfg.lookup("SPINE_COMPENSATION_FEEDBACK");
        m_useReflexes = cfg.lookup("USE_REFLEXES");
        
        m_nbLegs = cfg.lookup("NB_LEGS");
        m_nbMotorsLegs = cfg.lookup("NB_MOTORS_LEGS");
        m_nbMotorsTrunk = cfg.lookup("NB_MOTORS_TRUNK");
        m_nbMotorsNeck = cfg.lookup("NB_MOTORS_NECK");
        m_nbMotorsTail = cfg.lookup("NB_MOTORS_TAIL");

        m_nbMotors = m_nbLegs*m_nbMotorsLegs + m_nbMotorsTrunk
                   + m_nbMotorsNeck + m_nbMotorsTail;

        m_motorIDs = std::vector<int>(m_nbMotors, -1);
        m_enabledMotors = std::vector<bool>(m_nbMotors, 0);

        // Bit more complicated for an array 
        const libconfig::Setting& root = cfg.getRoot();
        const libconfig::Setting &motorIDsArray = root["MOTOR_IDS"];
        const libconfig::Setting &enabledMotorsArray = root["ENABLED_MOTORS"];

        if (motorIDsArray.getLength() != m_nbMotors)
        {
            std::cout << "Warning: MOTORS_IDS does not contain as many elements as the number of motors" << std::endl;
        }

        for(int i = 0; i < motorIDsArray.getLength() && i < m_nbMotors; i++)
        {
            m_motorIDs[i] = motorIDsArray[i];
        }

        if (enabledMotorsArray.getLength() != m_nbMotors)
        {
            std::cout << "Warning: ENABLED_MOTORS does not contain as many elements as the number of motors" << std::endl;
        }
        
        for(int i = 0; i < enabledMotorsArray.getLength() && i < m_nbMotors; i++)
        {
            m_enabledMotors[i] = enabledMotorsArray[i];
        }

        m_allMotorsDisabled = cfg.lookup("DISABLE_ALL_MOTORS");
        m_maxTorque = cfg.lookup("MAX_TORQUE");
        m_maxSpeed = cfg.lookup("MAX_SPEED");

        m_logData = cfg.lookup("LOG_DATA");
        // Two steps are requiered otherwise a run-time exception is thrown...
        std::string logDescription = cfg.lookup("LOG_DESCRIPTION");
        m_logDescription = logDescription;

        m_statusPrints = cfg.lookup("STATUS_PRINTS");
        m_configPrints = cfg.lookup("CONFIG_PRINTS");
        m_walkPrints = cfg.lookup("WALK_PRINTS");
        m_swimPrints = cfg.lookup("SWIM_PRINTS");
        m_mattPrints = cfg.lookup("MATT_PRINTS");
        m_skipHardware = cfg.lookup("SKIP_HARDWARE");
        m_printOverwrite = cfg.lookup("PRINT_OVERWRITE");
    }
    catch(const libconfig::SettingNotFoundException &nfex)
    {
        std::cerr << "Couldn't find the setting: " << nfex.getPath() 
                  << " in configuration file." << std::endl;

        return false;
    }
    catch(const libconfig::SettingTypeException &nfex)
    {
        std::cerr << "Type error for setting: " << nfex.getPath() 
                  << " in configuration file." << std::endl;

        return false;
    }

    return true;
}

bool GlobalConfig::isSimulation()
{
    return m_isSimulation;
}

bool GlobalConfig::isOptimization()
{
    return m_isOptimization;
}

bool GlobalConfig::useJoystick()
{
    return m_useJoystick;
}

int GlobalConfig::getJoystickMode()
{
    return m_joystickMode;
}

bool GlobalConfig::isAutoRun()
{
    return m_isAutoRun;
}

int GlobalConfig::getAutoRunState()
{
    return m_autoRunState;
}

bool GlobalConfig::highLevelControllerEnabled()
{
    return m_highLevelControllerEnabled;
}

bool GlobalConfig::useTail()
{
    return m_useTail;
}

bool GlobalConfig::useIMU()
{
    return m_useIMU;
}

bool GlobalConfig::useROS()
{
    return m_useROS;
}

bool GlobalConfig::swim()
{
    return m_swim;
}

bool GlobalConfig::spineCompensatedFeedback()
{
    return m_spineCompensationFeedback;
}

bool GlobalConfig::useReflexes()
{
    return m_useReflexes;
}

int GlobalConfig::nbMotors()
{
    return m_nbMotors;
}

int GlobalConfig::nbMotorsTrunk()
{
    return m_nbMotorsTrunk;
}

int GlobalConfig::nbLegs()
{
    return m_nbLegs;
}

int GlobalConfig::nbMotorsLegs()
{
    return m_nbMotorsLegs;
}

int GlobalConfig::nbMotorsTail()
{
    return m_nbMotorsTail;
}

int GlobalConfig::nbMotorsNeck()
{
    return m_nbMotorsNeck;
}

std::vector<int> GlobalConfig::getMotorsIDs()
{
    return m_motorIDs;
}

std::vector<bool> GlobalConfig::getEnabledMotors()
{
    return m_enabledMotors;
}

bool GlobalConfig::allMotorsDisabled()
{
    return m_allMotorsDisabled;
}

int GlobalConfig::maxTorque()
{
    return m_maxTorque;
}

int GlobalConfig::maxSpeed()
{
    return m_maxSpeed;
}

// Logging
bool GlobalConfig::logData()
{
    return m_logData;
}

std::string GlobalConfig::getLogDescription()
{
    return m_logDescription;
}

bool GlobalConfig::statusPrints()
{
    return m_statusPrints;
}

bool GlobalConfig::mattPrints()
{
    return m_mattPrints;
}

bool GlobalConfig::configPrints()
{
    return m_configPrints;
}

bool GlobalConfig::walkPrints()
{
    return m_walkPrints;
}

bool GlobalConfig::swimPrints()
{
    return m_swimPrints;
}

bool GlobalConfig::skipHardware()
{
    return m_skipHardware;
}

bool GlobalConfig::printOverwrite()
{
    return m_printOverwrite;
}