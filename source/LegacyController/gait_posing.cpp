/**
 * @file gait_posing.cpp
 * @author Legacy
 * @brief Function for posing of the robot
 * @version 0.1
 * @date 2021-02-08
 * 
 * @todo doxygen documentation
 * 
 */
#include <LegacyController/controller.hpp>

/* Joystick manipulation of different parameters/variables */
void
Controller :: posingManipulation()
{

    double centerLine = 0.5*(forTraj(2,1) + forTraj(5,1));
    double centerLine2 = 0.5*(forTraj_posing0(2,1) + forTraj_posing0(5,1));

    // rotating body
    double posing_rotational_velocity=0;
    posing_rotational_velocity = (-joy_x2*0.2 + forTraj_posing0(5,1) - forTraj(5,1))*3;

    // front girdle position
    double fgirdArc = (-joy_x1*0.4 + forTraj(5,1) - forTraj(2,1))*2;

    // forward - backward
    Vector3d transVel = MatrixXd::Zero(3,1);
    Vector3d proj0, proj;
    proj0 = AngleAxisd(-forTraj(5,1), Vector3d::UnitZ()) * forTraj_posing0.block<3,1>(3,1);
    proj =  AngleAxisd(-forTraj(5,1), Vector3d::UnitZ()) * forTraj.block<3,1>(3,1);


    transVel(0) = (joy_y1*0.1 + proj0(0) - proj(0))*2;
    transVel = AngleAxisd(forTraj(5,1)-centerLine, Vector3d::UnitZ()) * transVel;

    // front girdle up - down
    static double posingFgirdHeightMod = 0;
    static double normalHeight = GP.midStance(2,0);
    //if(posingFgirdHeightMod == 0){
    //    normalHeight = 0.5*(feetReference(2,0) + feetReference(2,1));
    //}
    posingFgirdHeightMod = -joy_y2*0.1;
    posingFeetHeight = normalHeight + posingFgirdHeightMod;

    // double v, double w, Vector3d V, double W, double fgirdArc, Vector2d spineCPGscaling
    girdleTrajectories(0, 0, transVel, posing_rotational_velocity, fgirdArc, MatrixXd::Zero(2,1));
    girdleVelocities();
}