
/**
 * @file controller_ros.cpp
 * @author Legacy
 * @brief Functions for ROS handling 
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <LegacyController/controller.hpp>
#include <std_msgs/Bool.h> 
#include "std_msgs/Int16.h"
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <sstream>

#ifdef FOR_WEBOTS
#include <stdlib.h> // For putenv()
#endif

#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <tf2_ros/transform_broadcaster.h>

/** 
 * @brief Starts the ROS node.
 * 
 * @param nodeName The name of the ROS node, all published data will be on
 * topics starting with this name
 */
void Controller::startROS(int argc, char **argv, const string &nodeName)
{
    m_nodeName = nodeName;

#ifdef FOR_WEBOTS
    /** 
     * @note Since Webots is running the controller in a sandboxed environment, 
     * the environment variable `ROS_MASTER_URI` will not be set and the controller 
     * will crash on the ros::init() call... After hours of blind debugging this
     * is the patch is came up with...
     * 
     * @todo this address should be specified elsewhere as a parameter in case it
     * ever changes. However, this is the default value and should most likely work.
     */
    putenv("ROS_MASTER_URI=http://localhost:11311");
#endif
    
    ros::init(argc, argv, m_nodeName);

    // All topics will be advertised relative to m_nodeName
    // e.g. "jointCommand" becomes "<m_nodeName>/jointCommand"
    ros::NodeHandle nodeHandle(m_nodeName);

    // Publishers
    m_pubJointCommand = nodeHandle.advertise<sensor_msgs::JointState>("jointCommand", 10);
    m_pubJointMeasured = nodeHandle.advertise<sensor_msgs::JointState>("jointMeasuredData", 10);
    m_pubIMURollPitchYaw = nodeHandle.advertise<geometry_msgs::Vector3>("imuRPY", 10);
    // m_pubLegForceFL = nodeHandle.advertise<geometry_msgs::Vector3>("legForce/fl", 10);
    // m_pubLegForceFR = nodeHandle.advertise<geometry_msgs::Vector3>("legForce/fr", 10);
    // m_pubLegForceBL = nodeHandle.advertise<geometry_msgs::Vector3>("legForce/bl", 10);
    // m_pubLegForceBR = nodeHandle.advertise<geometry_msgs::Vector3>("legForce/br", 10);
    m_pubForceVisualization = nodeHandle.advertise<geometry_msgs::PoseArray>("forceVisu", 10);

    m_pubRobotState = nodeHandle.advertise<std_msgs::Int16>("robotState", 10);

    ros::param::set("/swim_params/A", A_swim);
    ros::param::set("/swim_params/lambda", lambda_swim);
    ros::param::set("/swim_params/omega", omega_swim);
    ros::param::set("/swim_params/beta", beta_swim);
    ros::param::set("/swim_params/offset", offset_swim);

    ros::param::set("/krock_config/is_simulation", globalConfig.isSimulation());
    ros::param::set("/krock_config/high_level_controller", globalConfig.highLevelControllerEnabled());
    ros::param::set("/krock_config/use_joystick", globalConfig.useJoystick());
    ros::param::set("/krock_config/disable_motors", globalConfig.allMotorsDisabled());

    //For V4AR collaboration
    //m_pubLegStances = nodeHandle.advertise<std_msgs::Float64MultiArray>("LegStances", 10);
    //m_pubGlobalVelocity = nodeHandle.advertise<geometry_msgs::Twist>("GlobalVelocity", 10);
    //m_pubPose2d = nodeHandle.advertise<geometry_msgs::Pose2D>("pose2d", 10);
    // For IDSIA pipeline
    m_pubPose = nodeHandle.advertise<geometry_msgs::PoseStamped>("pose", 10);

    // Subscribers
    m_subRequestedState = nodeHandle.subscribe("requestedState",10, &Controller::requestedStateCallback, this);
    m_subGait = nodeHandle.subscribe("gait",10, &Controller::gaitCallback, this);
    m_subPositionTarget = nodeHandle.subscribe("positionTarget",10, &Controller::positionTargetCallback, this);
    m_subControlCommand = nodeHandle.subscribe("controlCommand",10, &Controller::controlCommandCallback, this);
}

/** 
 * @brief Publishes the sensors measurements on several topics.
 * 
 * @details The data that is published is the following:
 * - joint commands on the topic "<name>/jointCommand"
 * - joint measured on the topic "<name>/jointMeasuredData"
 * - joint IMU roll pitch yaw on "<name>/imuRPY"
 * - leg forces on topics "<name>/legForce/[fl | fr | bl | br]"
 * - visualization on topic "<name>/forceVisu"
 */
void Controller::publishData(double timestamp)
{
    if (ros::ok())
    {
        sensor_msgs::JointState cmd;
        sensor_msgs::JointState meas;

        for (int i = 0; i < globalConfig.nbMotors(); i++)
        {
            std::stringstream name;
            name << "M" << i;
            meas.header.stamp = ros::Time(t);
            cmd.name.push_back(name.str());
            cmd.position.push_back(angles[i]);

            meas.header.stamp = ros::Time(t);
            meas.name.push_back(name.str());
            meas.position.push_back(fbck_angles[i]);
            meas.effort.push_back(fbck_torques[i]);
        }

        std_msgs::Int16 robotState; 
        robotState.data = state;

        geometry_msgs::Vector3 rpy;
        rpy.x = forRPY(0);
        rpy.y = forRPY(1);
        rpy.z = forRPY(2);

        geometry_msgs::Vector3 forces[4];
        geometry_msgs::PoseArray forceVisu;
        forceVisu.header.frame_id = "robot_frame";

        for (int i=0; i<4; i++)
        {
            // double fx = nnEstForce.block<3,1>(0,i)(2);
            // double fy = nnEstForce.block<3,1>(0,i)(1);
            // double fz = nnEstForce.block<3,1>(0,i)(0);
            // forces[i].x = fx;
            // forces[i].y = fy;
            // forces[i].z = fz;

            //avoid noise from optosensor
            //This could be done in a better way if we could remove offset 
            // if( sqrt(fx*fx + fy*fy + fz*fz) > 2)
            // {
            //     geometry_msgs::Pose p;
            //     p.position.x = feetLocations.block<3,1>(0,i)(0);
            //     p.position.y = feetLocations.block<3,1>(0,i)(1);
            //     p.position.z = feetLocations.block<3,1>(0,i)(2);
            //     p.orientation.x = fx;
            //     p.orientation.y = fy;
            //     p.orientation.z = fz;

            //     forceVisu.poses.push_back(p);
            // }
        }

        geometry_msgs::PoseStamped poseMessage;
        poseMessage.header.stamp = ros::Time(timestamp);
        poseMessage.header.frame_id = "world";

        // ROS frame of reference is different than Webots'
        poseMessage.pose.position.x = gpsPos(0);
        poseMessage.pose.position.y = -gpsPos(2);
        poseMessage.pose.position.z = gpsPos(1);

        tf2::Quaternion orientation;
        orientation.setRPY(forRPY(0), forRPY(1), forRPY(2));
        poseMessage.pose.orientation.x = orientation.x();
        poseMessage.pose.orientation.y = orientation.y();
        poseMessage.pose.orientation.z = orientation.z();
        poseMessage.pose.orientation.w = orientation.w();

        m_pubJointCommand.publish(cmd);
        m_pubJointMeasured.publish(meas);
        m_pubIMURollPitchYaw.publish(rpy);
        // m_pubLegForceFL.publish(forces[0]);
        // m_pubLegForceFR.publish(forces[1]);
        // m_pubLegForceBL.publish(forces[2]);
        // m_pubLegForceBR.publish(forces[3]);
        m_pubForceVisualization.publish(forceVisu);
        m_pubPose.publish(poseMessage);
        m_pubJointCommand.publish(cmd);

        //publishTransformMessage(timestamp, "world", "krock2_spine_link", gpsPos(0), -gpsPos(2), gpsPos(1), forRPY(0), forRPY(1), forRPY(2));
        // gps is not available outside of Webots, so we will omit it for now
        publishTransformMessage(timestamp, "world", "krock2_spine_link", 0, 0, 0, 0, 0, 0);

        publishTransformMessage(timestamp, "krock2_spine_link", "krock2_base_link", 0, 0, 0, 0, 0, -fbck_angles[16]);

        publishTransformMessage(timestamp, "krock2_base_link", "marker_4", 0.05, 0, 0.1, 0, 0, 0);

        publishTransformMessage(timestamp, "krock2_base_link", "marker_5", -0.047, 0.08, 0.06, 0, 0, 0);

        publishTransformMessage(timestamp, "krock2_base_link", "fl_shoulder", 0, 0.0905, 0, fbck_angles[0], 0, fbck_angles[1]);
        publishTransformMessage(timestamp, "fl_shoulder", "marker_0", 0, 0.1, 0.04, 0, 0, 0);

        publishTransformMessage(timestamp, "krock2_base_link", "fr_shoulder", 0, -0.0905, 0, fbck_angles[4], 0, fbck_angles[5]);
        publishTransformMessage(timestamp, "fr_shoulder", "marker_1", 0, -0.1, 0.04, 0, 0, 0);

        publishTransformMessage(timestamp, "krock2_base_link", "spine_1", -0.05, 0, 0, 0, 0, fbck_angles[16]);
        publishTransformMessage(timestamp, "spine_1", "spine_2", -0.246, 0, 0, 0, 0, fbck_angles[17]);
        publishTransformMessage(timestamp, "spine_2", "marker_6", 0.005, -0.08, 0.08, 0, 0, 0);

        publishTransformMessage(timestamp, "spine_2", "hl_shoulder", -0.05, 0.0905, 0, fbck_angles[8], 0, fbck_angles[9]);
        publishTransformMessage(timestamp, "hl_shoulder", "marker_2", 0, 0.1, 0.04, 0, 0, 0);

        publishTransformMessage(timestamp, "spine_2", "hr_shoulder", -0.05, -0.0905, 0, fbck_angles[12], 0, fbck_angles[13]);
        publishTransformMessage(timestamp, "hr_shoulder", "marker_3", 0, -0.1, 0.04, 0, 0, 0);

        /*
        std::vector<double> leg_stances; 
        for(int i=0; i<4; ++i)
        {
            leg_stances.push_back(legs_stance(i));
        }
        std_msgs::Float64MultiArray leg_stances_message;
        leg_stances_message.data = leg_stances;
        m_pubLegStances.publish(leg_stances_message);

        if(!simle_pose_pub_initialized) {
            simple_x = gpsPos(0);
            simple_y = -gpsPos(2);
            simple_z = gpsPos(1);
            last_roll = forRPY(0);
            last_pitch = forRPY(1);
            last_yaw = forRPY(2);
            simle_pose_pub_initialized = true;
        }

        double roll = forRPY(0);
        double pitch = forRPY(1);
        double yaw = forRPY(2);
        double el_a = GP.ellipse_a(0);
        double el_b = GP.ellipse_b(0);gpsPosDirection)*inputTranslationVelocity;
        double vy_local = maxSpeed*r_ellipse/el_a*sin(inputTranslationDirection)*inputTranslationVelocity;
        double vx_global = vx_local*cos(pitch)*cos(yaw) + vy_local*cos(roll)*-sin(yaw);
        double vy_global = vx_local*cos(pitch)*sin(yaw) + vy_local*cos(roll)*cos(yaw);
        double vz_global = vx_local*-sin(pitch) + vy_local*sin(roll);
        simple_x += vx_global*dt;
        simple_y += vy_global*dt;
        simple_z += vz_global*dt;
        //double simple_theta = yaw;
        publishTransformMessage(timestamp, "world", "krock_self_est_2dpose", simple_x, simple_y, simple_z, roll, pitch, yaw);

        geometry_msgs::Twist global_velocity;
        global_velocity.linear.x = vx_global;
        global_velocity.linear.y = vy_global;
        global_velocity.linear.z = vz_global;
        global_velocity.angular.x = (roll - last_roll)/dt;
        global_velocity.angular.y = (pitch - last_pitch)/dt;
        global_velocity.angular.z = (yaw - last_yaw)/dt;
        last_roll = roll;
        last_pitch = pitch;
        last_yaw = yaw;
        m_pubGlobalVelocity.publish(global_velocity);
        */ 

        spinROS();
    }
}

void Controller::publishTransformMessage(double timestamp, std::string parent_frame, std::string child_frame, double x, double y, double z, double roll, double pitch, double yaw)
{
    static tf2_ros::TransformBroadcaster transform_brodcaster;
    geometry_msgs::TransformStamped transform;
    transform.header.stamp = ros::Time(timestamp);
    transform.header.frame_id = parent_frame;
    transform.child_frame_id = child_frame;

    transform.transform.translation.x = x;
    transform.transform.translation.y = y;
    transform.transform.translation.z = z;

    tf2::Quaternion orientation;
    orientation.setRPY(roll, pitch, yaw);
    transform.transform.rotation.x = orientation.x();
    transform.transform.rotation.y = orientation.y();
    transform.transform.rotation.z = orientation.z();
    transform.transform.rotation.w = orientation.w();

    transform_brodcaster.sendTransform(transform);
}

/** 
 * @brief Calls ros::spinOnce so that the messages are published on the topics
 * and the callbacks for the topics are processed.
 */
void Controller::spinROS()
{
    if (ros::ok())
        ros::spinOnce();
}

/** 
 * @brief Change the requested state.
 * 
 * @details See the state enum in controller.hpp for the values that can be sent.
 */
void Controller::requestedStateCallback(const std_msgs::Int32ConstPtr& msg)
{
    if (!globalConfig.useJoystick())
    {
        std::cout << "Received state request: " << msg->data << std::endl;

        requestedState = msg->data;
    }
}

/** 
 * @brief Change the gait of Krock (low/high stance).
 * 
 * @details msg->data will be clamped between 0 and 1.
 * 1.0 corresponds to the low stance, 0.0 corresponds to the high stance.
 */
void Controller::gaitCallback(const std_msgs::Float64ConstPtr& msg)
{
    if (!globalConfig.useJoystick())
    {
        float gaitMixing =  msg->data;
        std::cout << "Received gait parameter: " << gaitMixing << std::endl;
        
        // Clamp between 0 and 1
        gaitMixing = (gaitMixing > 1.0) ? 1.0 : gaitMixing;
        gaitMixing = (gaitMixing < 0.0) ? 0.0 : gaitMixing;

        inputGaitMixing = gaitMixing;
    }
}

/** 
 * @brief Set a new position target for the high-level controller.
 * 
 * @note msg->y will be ignored, so the position target is only 2D.
 */
void Controller::positionTargetCallback(const geometry_msgs::PointConstPtr& msg)
{
    if (!globalConfig.useJoystick())
    {
        std::cout << "Received position target: x=" << msg->x << " y="
        << msg->y << " z=" << msg->z << std::endl;

        m_highLevelPositionTarget[0] = msg->x;
        m_highLevelPositionTarget[1] = msg->y;
        m_highLevelPositionTarget[2] = msg->z;
    }
}

/** 
 * @brief Send commands to the controller.
 * 
 * @details There are 3 commands that are used by the controller:
 * - inputTranslationDirection: msg->x
 * - inputTranslationVelocity: msg->y
 * - inputTurningCurvature: msg->z
 */
void Controller::controlCommandCallback(const geometry_msgs::PointConstPtr& msg)
{
    if (!globalConfig.useJoystick())
    {
        std::cout << "Received control commands:" << std::endl;
        std::cout << "Translation direction: " << msg->x << std::endl;
        std::cout << "Translation velocity: " << msg->y << std::endl;
        std::cout << "Turning curvature: " << msg->z << std::endl;

        inputTranslationDirection = msg->x;
        inputTranslationVelocity = msg->y;
        inputTurningCurvature = msg->z;
    }
}
