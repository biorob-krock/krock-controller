/**
 * @file controller_high_level.cpp
 * @author Legacy
 * @brief Compute controller commands depending on the set position target.
 * @details Minimalist high-level controller.
 * The user sets m_highLevelPositionTarget (e.g. via the ROS topic) and this 
 * function will compute control commands to drive the robot towards it.
 * To optimize the speed of the robot, the robot will not move laterally 
 * (inputTranslationDirection = 0), but only forward at full speed with rotation.
 * The robot will try to align itself with the target position.
 * inputTurningCurvature is computed using a simple P-controller on the angular
 * error between the direction towards the target and the current orientation 
 * (yaw) of the robot. The yaw measured by the IMU must be filtered, since the
 * robot spine is oscillating during its motion. To do so, a simple moving 
 * window is used.
 * @version 0.1
 * @date 2021-02-08
 * @note Currently the high-level controller ignores the Y component of the
 * position target, so the robot will reach a position along X-Z (horizontal).
 * 
 * 
 */
#include <LegacyController/controller.hpp>

#define STOP_DISTANCE 0.25
#define P_GAIN_ANGLE 0.5
#define YAW_FILTER_WINDOW 150

void Controller::updateHighlevelCommands()
{
    // filtering window size: TIME_STEP * nb_sample
    static double yawReadings[YAW_FILTER_WINDOW] = {0};
    static int currentIndex = 0;

    if (state != INITIAL && (requestedState == STANDING || requestedState == -1))
        requestedState = STANDING;

    yawReadings[currentIndex] = forRPY(2);
    currentIndex++;
    if (currentIndex >= YAW_FILTER_WINDOW)
        currentIndex = 0;

    // Average
    double robotYaw = 0;
    for (int i = 0; i < YAW_FILTER_WINDOW; i++)
    {
        robotYaw += yawReadings[i];
    }
    robotYaw /= YAW_FILTER_WINDOW;

    // delta_x, detla_z, y is ignored
    double positionError[2] = {m_highLevelPositionTarget[0] - gpsPos(0), 
                               m_highLevelPositionTarget[2] - gpsPos(2)};
    
    double distanceToTarget = sqrt(positionError[0] * positionError[0]
                                 + positionError[1] * positionError[1]);

    if (distanceToTarget < STOP_DISTANCE)
    {
        inputTranslationDirection = 0;
        inputTranslationVelocity = 0;
        inputTurningCurvature = 0.0;
    }
    else
    {
        double targetDirectionAngularError = M_PI / 2 - atan2(positionError[0], positionError[1]) + robotYaw;

        // Wrap angular error in the range [-pi;pi]
        if (targetDirectionAngularError > M_PI)
            targetDirectionAngularError -= 2 * M_PI;
        else if (targetDirectionAngularError < -M_PI)
            targetDirectionAngularError += 2 * M_PI;

        inputTurningCurvature = P_GAIN_ANGLE * targetDirectionAngularError;
        inputTurningCurvature = (inputTurningCurvature > 1) ? 1.0 : inputTurningCurvature; 
        inputTurningCurvature = (inputTurningCurvature < -1) ? -1.0 : inputTurningCurvature; 

        // Set these to maximize speed (lateral motion is very slow)
        inputTranslationDirection = 0.0;
        inputTranslationVelocity = 1.0;
    }
}