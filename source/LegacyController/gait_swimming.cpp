/**
 * @file gait_swimming.cpp
 * @author Legacy
 * @brief Functions for swimming
 * @version 0.1
 * @date 2021-02-08
 * 
 * @todo doxygen documentation formatting 
 * 
 */
#include <LegacyController/controller.hpp>
#include <math.h>

using namespace std;
using namespace Eigen;

MatrixXd
Controller :: swimFun()
{
/*! \brief Calculate desired angles for swimming spine and hold legs to the side.
 *
 *  Calculate spine oscillating wave. 
 *  Return MatrixXd of desired angles.
 */
    if(globalConfig.swimPrints()){
        cout << "Swim mode calculations" << endl; 
        cout << "t: \t" << t << endl; 
    }
    
    MatrixXd swimSpineAngles;  // Angle commands to return 
    static int nspine = globalConfig.nbMotorsTail()
                      + globalConfig.nbMotorsTrunk()
                      + globalConfig.nbMotorsNeck();

    swimSpineAngles.resize(nspine,1); 
    swimSpineAngles.setZero(); 

    // Equation: A*e⁽x*B)sin(n/lambda + omega*t)
    for(int i=0; i<nspine; i++){
        // joy_y2 is right-joystick vertical axis 
        int spineAngleSign = 1; 
        //TAIL PATCH 
        // Not sure why this is inverted on the robot but I am sticking this here until we dig into Tomislav's kinematics 
        if( (i == 0) || (i==1) ){
            spineAngleSign = -1; 
        }
        swimSpineAngles(i,0) = spineAngleSign * (A_swim*joy_y2*exp(beta_swim*spineGeo[i])*sin(spineGeo[i]/lambda_swim + omega_swim*M_PI*2*t) - offset_swim*joy_x2);
    }

    if(joy_r1 > 0){
        loadConfig("config/swimming.cfg");
        cout << "Reloading swim parameters [L1 button pressed]" << endl;
    }

    if(globalConfig.swimPrints()){
        cout << "Swim mode calculations" << endl; 
        cout << "Commanded angles: " << endl;
        for(int i = 0; i<nspine; i++){
            cout << "Spine motor: " << i << "\t Angle: " << swimSpineAngles(i,0)<< endl; 
        }
    }
    
    if(globalConfig.swimPrints()){
        cout << "joystick reading (forward speed): " << endl; 
        cout << joy_y2 << endl; 
        cout << "joystick reading (left-right): " << endl; 
        cout << joy_x2 << endl; 
    }

    return swimSpineAngles; 
}

MatrixXd
Controller :: swimPosture(){
/**
 * \brief Return hard-coded positions for the legs.
 *
 */
    
    MatrixXd swimLegAngles(4,4);

    swimLegAngles = swimLegPos; // Reading this in from config file 

    return swimLegAngles; 

}

MatrixXd
Controller::get_q_tail(){
/** 
 * \brief Pass out q_tail to use outside the controller class 
 * 
 */

    return q_tail; 
}
