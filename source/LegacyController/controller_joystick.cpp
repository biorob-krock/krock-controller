/**
 * @file controller_joystick.cpp
 * @author Legacy
 * @brief Functions to read and process joystick inputs
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <LegacyController/controller.hpp>


/* Operators on gaitParameters */
gaitParams
combineGaitParameters(gaitParams *gp1, gaitParams *gp2, double x)
{
    gaitParams gp=*gp1;

    gp.spineCPGscaling =    (1-x)*gp1->spineCPGscaling +      x*gp2->spineCPGscaling;
    gp.Duty =               (1-x)*gp1->Duty +                 x*gp2->Duty;
    gp.phShifts =           (1-x)*gp1->phShifts +             x*gp2->phShifts;
    gp.midStance =          (1-x)*gp1->midStance +            x*gp2->midStance;
    gp.ellipse_a =          (1-x)*gp1->ellipse_a +            x*gp2->ellipse_a;
    gp.ellipse_b =          (1-x)*gp1->ellipse_b +            x*gp2->ellipse_b;
    gp.swing_height =       (1-x)*gp1->swing_height +         x*gp2->swing_height;
    gp.swing_width =        (1-x)*gp1->swing_width +          x*gp2->swing_width;
    gp.nSurf =              (1-x)*gp1->nSurf +                x*gp2->nSurf;
    gp.nLO =                (1-x)*gp1->nLO +                  x*gp2->nLO;
    gp.nTD =                (1-x)*gp1->nTD +                  x*gp2->nTD;
    gp.bezierScaling =      (1-x)*gp1->bezierScaling +        x*gp2->bezierScaling;
    gp.tSclSwing =          (1-x)*gp1->tSclSwing +            x*gp2->tSclSwing;
    gp.qNULL =              (1-x)*gp1->qNULL +                x*gp2->qNULL;

    return gp;
}

/* Reads joystick */
void
Controller :: readJoystick()
{
    //joystick.update();
    joystick.ReadSharedMemory();

    // PS3 CONTROLLER
    // Buttons
    // useful diagram in section 9 of this website: http://wiki.ros.org/ps3joy
    // joy_sel=joystick.joyStruct.buttons[0];
    // joy_l3=joystick.joyStruct.buttons[1];
    // joy_r3=joystick.joyStruct.buttons[2];
    // joy_start=joystick.joyStruct.buttons[3];
    // joy_l2=joystick.joyStruct.buttons[8];
    // joy_r2=joystick.joyStruct.buttons[9];
    // joy_l1=joystick.joyStruct.buttons[10];
    // joy_r1=joystick.joyStruct.buttons[11];
    // joy_bU=joystick.joyStruct.buttons[12];
    // joy_bR=joystick.joyStruct.buttons[13];
    // joy_bD=joystick.joyStruct.buttons[14];
    // joy_bL=joystick.joyStruct.buttons[15];

    // joy_aU=joystick.joyStruct.buttons[4];
    // joy_aR=joystick.joyStruct.buttons[5];
    // joy_aD=joystick.joyStruct.buttons[6];
    // joy_aL=joystick.joyStruct.buttons[7];

    // // Axes
    // joy_x1= joystick.joyStruct.axes[0];
    // joy_y1=-joystick.joyStruct.axes[1];
    // joy_x2= joystick.joyStruct.axes[2];
    // joy_y2=-joystick.joyStruct.axes[3];
    // joy_x3= joystick.joyStruct.axes[4];
    // joy_y3=-joystick.joyStruct.axes[5];


    // DS4 CONTROLLER (PS4)
    // Buttons
    joy_sel=joystick.joyStruct.buttons[8];   // on the controller it's named share
    joy_start=joystick.joyStruct.buttons[9]; // on the controller it's named options
    joy_l3=joystick.joyStruct.buttons[11];
    joy_r3=joystick.joyStruct.buttons[12];
    
    joy_bD=joystick.joyStruct.buttons[0];
    joy_bR=joystick.joyStruct.buttons[1];
    joy_bU=joystick.joyStruct.buttons[2];
    joy_bL=joystick.joyStruct.buttons[3];
    joy_l1=joystick.joyStruct.buttons[4];
    joy_r1=joystick.joyStruct.buttons[5];

    // the followings are not anymore buttons but axis
    joy_l2=joystick.joyStruct.axes[2];
    joy_r2=joystick.joyStruct.axes[5];
    // joy_aU=joystick.joyStruct.axes[7]; //positive when pressed
    // joy_aR=joystick.joyStruct.axes[6]; //negative when pressed
    // joy_aD=joystick.joyStruct.axes[7]; //negative when pressed
    // joy_aL=joystick.joyStruct.axes[6]; //positive when pressed

    // Axes
    joy_x1= joystick.joyStruct.axes[0];
    joy_y1=-joystick.joyStruct.axes[1];
    joy_x2= joystick.joyStruct.axes[3];
    joy_y2=-joystick.joyStruct.axes[4];

    //don't know what are those axes
    joy_x3= joystick.joyStruct.axes[4];
    joy_y3=-joystick.joyStruct.axes[4];

    if(globalConfig.isAutoRun()){
        joystickAuto();
    }
    if(globalConfig.getJoystickMode() == 1){
        joystickRecord();
    }
    else if(globalConfig.getJoystickMode() == 2){
        joystickReplay();
    }

    joy_lsr=sqrt(joy_x1*joy_x1+joy_y1*joy_y1);
    joy_lsr=joy_lsr>1?1:joy_lsr;
    if(joy_lsr<0.1){
        joy_lsr=0;
    }
    if(joy_lsr>0.2){
        joy_lsphi=atan2(-joy_x1, joy_y1);
    }
    else{
        joy_lsphi=0;
    }

    joy_rsr=sqrt(joy_x2*joy_x2+joy_y2*joy_y2);
    joy_rsr=joy_rsr>1?1:joy_rsr;
    if(joy_rsr<0.1){
        joy_rsr=0;
    }
    if(joy_rsr>0.2){
        joy_rsphi=atan2(joy_y2, joy_x2)-my_pi/2;
    }
    else{
        joy_rsphi=0;
    }

}

/* Reads joystick inputs and modifies trajectories */
bool
Controller :: updateState()
{

    if(globalConfig.useJoystick())
    {
        readJoystick();

        // Update controller commands
        inputTranslationVelocity = joy_lsr;
        inputTranslationDirection = joy_lsphi;
        inputTurningCurvature = joy_x2;

        inputGaitMixing = inputGaitMixing + (joy_l2 - joy_r2)*dt/2.;
        inputGaitMixing = (inputGaitMixing < 0) ? 0 : inputGaitMixing;
        inputGaitMixing = (inputGaitMixing > 1) ? 1 : inputGaitMixing;

        requestEmergencyStop = (joy_start == 1);

        // Update requested state based on buttons
        requestedState = state;

        if (joy_sel == 0)
        {
            if (joy_l1 == 1)
                requestedState = POSING;

            if (joy_bR == 1){
              // Normal walking
                GP = WP;
                requestedState = STANDING;
            }
            if (joy_bL == 1){
              //requestedState = AMPHIBIOUS;

              // "Extra High" gait
                requestedState = HIGH_STEP;
            }
            if (joy_bD == 1)
                requestedState = INITIAL;

            /** 
             * @note joy_aR is not updated by the joystick libray.
             * 
             * @todo change button to trigger this state or fix joystick lib.
             */
            if (joy_aR > 0)
                requestedState = SUIT;

            if(joy_bU == 1)
                requestedState = SWIMMING;
        }
        else
        {
            requestedState = -1;
        }

    }
    else if (globalConfig.highLevelControllerEnabled())
    {
        updateHighlevelCommands();
    }

    phases=phases0;
    //============================== EMERGENCY STOP ==============================
    if(requestEmergencyStop)
    {
        return false;
    }

    static double gpx = 0, gpx_old=0;
    if(state == WALKING || state == STANDING || state == POSING)
    {
        gpx = inputGaitMixing;

        if(gpx_old!=gpx)
        {
            GP = combineGaitParameters(&WP, &WPlow, gpx);
            gpx_old = gpx;
        }
    }


    //=============================== POSING ===================================
    if(requestedState == POSING && state != POSING)
    {
        state=POSING;
        forTraj_posing0 = forTraj;
        posing_head=0;
        T_stand = T_trans0/2;
    }
    if(requestedState == STANDING && state==POSING)
    {
        state=STANDING;
        T_stand = T_trans0/2;
    }

    //=============================== CRAWLING ===================================
     // static bool trigger_crawling_continue = false;

    // TODO check behavior of this weird condition, my guess is that we want to run an
    // iteration of the controller before setting crawling continue to true.
    // This crawling continue variable is very obscure and probably this can be done
    // in a much cleaner fashion
    //
    // if(requestedState == CRAWLING && trigger_crawling_continue == false && state == CRAWLING)
    // {
    //     crawling_continue = true;
    // }
    // trigger_crawling_continue = (requestedState == CRAWLING);

    // if(requestedState == CRAWLING && state != CRAWLING)
    // {
    //     state=CRAWLING;
    //     GP=WP;
    //     T_trans=T_trans0;
    //     crawling_continue = false;
    //     crstate = initial;
    // }


    //=============================== HIGH STEP ===================================
    // else if(requestedState == HIGH_STEP && !(state == WALKING || state == STANDING))
    if(requestedState == HIGH_STEP)
      {
        state=STANDING;
        GP=WPhighstep;
        T_trans=T_trans0;
        T_stand = T_trans0/2;
      }

    //=============================== STANDING ===================================
    else if(requestedState == STANDING && !(state == WALKING || state == STANDING))
    {
        state=STANDING;
        GP=WP;
        // GP=combineGaitParameters(&WP, &WPlow, gpx);
        T_trans=T_trans0;
        T_stand = T_trans0/2;
    }

    //=============================== WALKING ===================================
    if (state == STANDING && inputTranslationVelocity > 0.2)
    {
        state=WALKING;
        //GP=WP;
        // GP=combineGaitParameters(&WP, &WPlow, gpx);
        T_stand = T_trans0/2;
    }

    //=============================== STANDING ===================================
    else if(state == WALKING && inputTranslationVelocity <= 0.2)
    {
        state=STANDING;
        //GP=WP;
        GP=combineGaitParameters(&WP, &WPlow, gpx);
        T_stand = T_trans0/2;
    }

    //=============================== INITIAL ===================================
    if(requestedState == INITIAL && !(state==INITIAL))
    {
        state=INITIAL;
        T_trans=T_trans0;
    }

    //=============================== Suit-up ===================================
    if(requestedState == SUIT && !(state==SUIT))
    {
        state=SUIT;
    }


    //=============================== SWIMMING ===================================
    if(requestedState == SWIMMING && state!=SWIMMING)
    {
        state=SWIMMING;
        T_trans=T_trans0;
        freq_swim=0;
        cpg_offset=0;
    }

    //=============================== AMPHIBIOUS ===================================
    if(requestedState == AMPHIBIOUS && state!=AMPHIBIOUS)
    {
        state=AMPHIBIOUS;
        T_trans=T_trans0;
        freq_swim=0;
        cpg_offset=0;
        GP=combineGaitParameters(&WP, &WPlow, gpx);
        T_stand = T_trans0/2;
    }

    //=============================== DECAY TRANSITION FILTERING CONSTANT ===================================
    T_trans=pt1(0, T_trans, 1, dt);
    T_stand=pt1(0, T_stand, 1, dt);

    //=============================== JOYSTICK INTERACTION ===================================
    if(globalConfig.isAutoRun()){
        inputTranslationVelocity=1;
        if(t<0.1){
            state=globalConfig.getAutoRunState();
        }
    }

    // Reset requested state
    requestedState = -1;

    return true;
}


void
Controller :: joystickRecord()
{
	static ofstream joysticRec("./data/joysticRec.txt");
	// joysticRec << joy_x1 <<"\t" << joy_y1 << "\t";
	// joysticRec << joy_x2 <<"\t" << joy_y2 << endl;

     // Buttons
    joysticRec << joy_sel << "\t"; 
    joysticRec << joy_l3 << "\t"; 
    joysticRec << joy_r3 << "\t"; 
    joysticRec << joy_start << "\t"; 
    joysticRec << joy_l2 << "\t"; 
    joysticRec << joy_r2 << "\t"; 
    joysticRec << joy_l1 << "\t"; 
    joysticRec << joy_r1 << "\t"; 
    joysticRec << joy_bU << "\t"; 
    joysticRec << joy_bR << "\t"; 
    joysticRec << joy_bD << "\t"; 
    joysticRec << joy_bL << "\t"; 
    joysticRec << joy_aU << "\t"; 
    joysticRec << joy_aD << "\t"; 
    joysticRec << joy_aL << "\t"; 
    joysticRec << joy_x1 << "\t"; 
    joysticRec << joy_y1 << "\t"; 
    joysticRec << joy_x2 << "\t"; 
    joysticRec << joy_y2 << "\t"; 
    joysticRec << joy_x3 << "\t"; 
    joysticRec << joy_y3 << endl; 

}

void
Controller :: joystickReplay()
{
	static int linecount=0;
	if(linecount>=sizeOfJoystickRecording)
		return;
	else{
		joy_x1=joysticRecordings[4*linecount+0];
		joy_y1=joysticRecordings[4*linecount+1];
		joy_x2=joysticRecordings[4*linecount+2];
		joy_y2=joysticRecordings[4*linecount+3];
		linecount++;
	}
}

void
Controller :: joystickAuto(){

    static bool is_init=false, is_enabled=false, is_oscillating=false;
    static int num_commands;
    static double in_array[10][5];
    static double joy_freq;

    // ------------------- INITIALIZE --------------------------
    if(is_init==false){
        is_init=true;
        stringstream stringstream_file;
        ifstream file_joystickAuto;

        // init reading
        file_joystickAuto.open("config/joystickAuto.config");
        stringstream_file.str(std::string());
        readFileWithLineSkipping(file_joystickAuto, stringstream_file);

        // read stuff
        stringstream_file >> is_enabled;
        stringstream_file >> num_commands;


        for(int i=0; i<num_commands; i++){
            for(int j=0; j<5; j++){
                stringstream_file >> in_array[i][j];
            }
        }

        stringstream_file >> is_oscillating;
        stringstream_file >> joy_freq;

    }

    // ------------------- RUN --------------------------

    if(is_enabled){
        if(t<0.1){
            state=globalConfig.getAutoRunState();
        }
        int seg=0;
        for(int i=0; i<num_commands; i++){
            if(in_array[i][0] <= t){
                seg=i;
            }
        }

        joy_x1=in_array[seg][1];
        joy_y1=in_array[seg][2];
        joy_x2=in_array[seg][3];
        joy_y2=in_array[seg][4];

        if(is_oscillating){
            if( t<= in_array[0][0]){
                joy_x1=0;
                joy_y1=1;
                joy_x2=cos(2*my_pi*joy_freq*t);;
                joy_y2=0;
            }
            else{
                joy_x1=cos(2*my_pi*joy_freq*t);
                joy_y1=sin(2*my_pi*joy_freq*t);
                joy_x2=0;
                joy_y2=0;
            }

        }
    }


}
