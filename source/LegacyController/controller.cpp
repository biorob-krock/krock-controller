/**
 * @file controller.cpp
 * @author Legacy
 * @brief Main controller class file
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <LegacyController/controller.hpp>
#include <iostream>
#include <cmath>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdlib.h>

#include <chrono>
#include <ctime>   

using namespace std;
using namespace Eigen;



// ---------------------------------------------- FUNCTIONS ----------------------------------------------------------
/* Controller constructor */
Controller :: Controller(double time_step) : joystick((const char*)"ps3shm")
{
/** 
 * @brief Controller class constructor brief
 * 
 * 
 */


    // std::chrono::system_clock::time_point controllerClockStart = std::chrono::system_clock::now();
    controllerClockStart = std::chrono::system_clock::now();
    prevPrint = std::chrono::system_clock::now();


    //############################# RESIZE DYNAMIC MATRICES ##########################################
    fbck_torques.resize(globalConfig.nbMotors(), 1);
    fbck_angles.resize(globalConfig.nbMotors(), 1);
    joint_angles.resize(globalConfig.nbMotors(), 1);
    fbck_current.resize(globalConfig.nbMotors(), 1);

    global_joint_pos.resize(3, globalConfig.nbMotors());

    angles.resize(globalConfig.nbMotors(), 1);
    torques.resize(globalConfig.nbMotors(), 1);

    init_angles.resize(globalConfig.nbMotors(), 1);

    q0_trunk_from_spline.resize(globalConfig.nbMotorsTrunk(), 1);
    q_trunk.resize(globalConfig.nbMotorsTrunk(), 1);

    trunk_kin.resize(globalConfig.nbMotorsTrunk()+1, 1);

    if(globalConfig.nbMotorsNeck() > 0){
        neck_kin.resize(globalConfig.nbMotorsNeck(), 1);
    }

    // TAIL PATCH 
    if(globalConfig.nbMotorsTail() > 0)
    { // PREVIOUS ENTRY 
    //if(globalConfig.useTail()){
        tail_kin.resize(globalConfig.nbMotorsTail(), 1);
        q_tail.resize(globalConfig.nbMotorsTail(),1);
        HJt.resize(globalConfig.nbMotorsTail()+1);
        HJt_g.resize(globalConfig.nbMotorsTail()+1);
    }

    HJs.resize(globalConfig.nbMotorsTrunk()+1);
    HJfl.resize(5);
    HJfr.resize(5);
    HJhl.resize(5);
    HJhr.resize(5);
    HJs_g.resize(globalConfig.nbMotorsTrunk()+1);
    HJfl_g.resize(5);
    HJfr_g.resize(5);
    HJhl_g.resize(5);
    HJhr_g.resize(5);


    ikinQpSolver.resize(4);
    legJacob.resize(4);

    //############################ initialize MPC controller ##########################################
    setTimeStep(time_step);
    mpc_com.readParameters("config/MPC/mpc.config");
    if(mpc_com.enabled){
        cout << "setConstraintMatrices"<<endl;
        mpc_com.setConstraintMatrices();
        cout << "constructQP"<<endl;
        mpc_com.constructQP();
        cout << "printMatrices"<<endl;
        mpc_com.printMatrices("MPC_TEST.txt");
        cout << "initQPSolver"<<endl;
        mpc_com.initQPSolver();
    }
    angSignsCorrIkin2Webots.reserve(globalConfig.nbMotors());
    angShiftsCorrIkin2Webots.reserve(globalConfig.nbMotors());
    angSignsCorrIkin2Robot.reserve(globalConfig.nbMotors());
    angShiftsCorrIkin2Robot.reserve(globalConfig.nbMotors());

    // other variables
    supportPolys.resize(mpc_com.N);
    supportPolysIMU.resize(mpc_com.N);

    //############################ PARAMETERS ########################################################
    // Separating these out to avoid mistreadings I was running into
    getParameters();
    loadConfig("config/joystick.cfg");
    loadConfig("config/swimming.cfg");
    loadConfig("config/masses.cfg");
    loadConfig("config/kinematics.cfg");
    getParameters_iKinController();

    GP=WP;

    if(globalConfig.configPrints()){
        cout << "Config files parsed in Controller initialization" << endl;
    }
    

    state=INITIAL;
    requestedState = INITIAL; 
    initializedSpine = 0;
    
    requestEmergencyStop = false;
    inputGaitMixing = 0;
    inputTranslationDirection = 0;
    inputTranslationVelocity = 0;
    inputTurningCurvature = 0;

    T_trans=T_trans0;
    T_stand=T_trans0/2;

    t=0;

    forRPY.setZero();
    is_flipped=false;
    turning_curvature=0;

    //########## STANDING POSITION #################

    force_filt << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    qs << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;

    // initial conditions
    qFL.setZero();
    qFR.setZero();
    qHL.setZero();
    qHR.setZero();
    init_angles.setZero();
    init_angles*=0;

    legs_out_of_reach.setZero();

    feetReference = GP.midStance;

    legs_stance << 1,1,1,1;
    swingPhase << 0, 0, 0, 0;

    joint_angles.setZero();
    feet_force<<0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    feet_force_filt=feet_force;
    fbck_angles.setZero();
    freq_walk=0;

    rotMatFgird=Matrix3d::Identity();

    girdleVelocity_filtered=MatrixXd::Zero(3,2);
    girdleAngularVelocity_filtered=MatrixXd::Zero(1,2);
    forVelocity_filtered=MatrixXd::Zero(3,2);
    forAngularVelocity_filtered=MatrixXd::Zero(1,2);

    feetReference_forceCorrections.setZero();

    // initialize trajectory container
    for(int i=0; i<300; i++){
        forTrajHist(0,i)=-i/250.*IG;
        forTrajHist(1,i)=0;
        forTrajHist(2,i)=0;
    }

    //############################ initialize FDO controller ##########################################
    fdo.initSolver(masses.sum(), mu_wall, fdo_dt);
    legs_contact_control << 0,0,0,0;

    crstate = initial;
    //#############################Joystick dynamics########################################

    /*
    nfl=0; nfr=0; nhl=0; nhr=0;
    fxfl=0; fxfr=0; fxhl=0; fxhr=0;*/

    posing_xFH=0; posing_yF=0; posing_zF=0; posing_head=0;
    crab_rotAng=0;

    //############################# REFLEXES ########################################
    for(int i=0; i<4; i++){
        stu_reflex_active[i]=0;
        ext_reflex_active[i]=0;
    }


    fgird2ForRotMat=MatrixXd::Identity(3,3);


    //############################# LEG PHASES ########################################
    for(int i=0; i<4; i++){
        legPhase(i)=GP.phShifts(i)<0?GP.phShifts(i)+1:GP.phShifts(i);
    }

    legs_contact.setZero();

    girdleTraj << 0,0,0,0,0,0, 0,0,0,0,0,0;
    girdleTraj(3,0)=-IG; girdleTraj(3,1)=-IG;
    //girdleRealOrientation << 0,0, 0,0;
    forTraj << 0,0,0, 0,0,0, 0,0,0, 0,0,0;
    forTraj(3,0)=-IG; forTraj(3,1)=-IG;
    fgird_heading_angle=0;
}

/* Sets time step */
void
Controller :: setTimeStep(double time_step)
{
    
    dt=time_step;
    return;
}

bool
Controller :: runStep(double webots_time)
{
/** @brief Controller's main function, update angles for all joints. 
 *  
 * Both calculates desired angles then send commands to motors
 */

    if(!updateState()){
        return false;
    }


    // forward kinematics
    joint_angles=fbck_angles;
    forwardKinematics();
    realCoM=getCoM();

    // // estimate contact force
    // mlpContactForceEstimation();
  
    if (globalConfig.statusPrints())
        printControlStatus();

    if(state==INITIAL){
        angles=init_angles;

        // Make sure the callbacks for the topics are processed since the 
        // function publishData() is not called in this state.
        if(globalConfig.useROS())
            spinROS();

        return true;
    }

    //############################ TIME #########################################################
        t+=dt;

    //--------------------------- joystick input-----------------------------
    if(state==WALKING || state==AMPHIBIOUS){
        walking_calcSpeedDir(); 
    }

    //-----------------------------------------------------------------------
    getSwingStanceEndPoints(); //Choose appropriate stance starting point to get a maximum possible stance length on the ellipse
                                // modifies somne gait parameter (GP) properties 

    // Run state machines
    switch(state){

        case WALKING:  
        // Quadrupedal walking  
            walkingStateMachine(); 
            break; 

        // case CRAWLING:  
        // // Pipe crawling between two vertical plates 
        // // currently disabled, no controller input will trigger
        // // This was testing in simulation but hardware did not have enough torque 
        //     crawlingStateMachine(); 
        //     break; 

        case POSING:  
        // Posing with stationary feet while reorienting front girdle to move camera   
            posingManipulation();
            break; 

        case AMPHIBIOUS: 
        {
        // Walking with front legs, swimming with back    
            walkingStateMachine();
            MatrixXd q_spine = swimFun(); 
            const int nntail =3; 
            const int nntrunk =2; 
            q_trunk = q_spine.block<nntrunk,1>(0,0);
            if(globalConfig.useTail()){
                q_tail = q_spine.block<nntail,1>(2,0);
            }
        }
            break; 

        case SWIMMING:  
          {
        // Oscillating spine and tail to propel forward in water 
            MatrixXd q_spine = swimFun(); 
            const int nntail =3; 
            const int nntrunk =2; 
            q_trunk = q_spine.block<nntrunk,1>(0,0);
            if(globalConfig.useTail()){
                q_tail = q_spine.block<nntail,1>(2,0);
            }
          }
            break;       

        case STANDING:
          {
          if(!initializedSpine){
            q_trunk.setZero();
            initializedSpine = 1;
            cout << "Spine initialized" << endl;
           }
          }
           break;
    }

    // // reflexes
    // feetReference_reflex = MatrixXd::Zero(3,4);
    // stumbleReflex();
    // legExtension();

    // Calculate leg angles 
    MatrixXd calcLegAngles(4,4);
    // if(state==WALKING || state==CRAWLING || state==POSING){
    // If using legged locomotion, go through inverse kinematic calculations 

    // combine all the factors for feet reference
    for(int i=0; i<4; i++){
        ikinRef.block<3,1>(0,i)=feetReference.block<3,1>(0,i) +
                                1*feetReference_forceCorrections.block<3,1>(0,i);
                                // + 1*feetReference_reflex.block<3,1>(0,i);
    }

    // Inverse kinematics
    ikinRef = standingTransition(ikinRef);
    calcLegAngles = inverseKinematicsController(ikinRef);

    if(state==SWIMMING){
    // If swimming, calculate spine angles and set static leg angles
        if(globalConfig.swimPrints()){
            cout << "Assigning swim leg angles " << endl; 
        }
        calcLegAngles = swimPosture(); 
    }

    if(state==AMPHIBIOUS){
    // If swimming, calculate spine angles and set static leg angles
        if(globalConfig.swimPrints()){
            cout << "Assigning swim leg angles " << endl; 
        }
        MatrixXd tmpLegAngles(4,4);
        tmpLegAngles = swimPosture(); 
        calcLegAngles.block<4,2>(0,2) = tmpLegAngles.block<4,2>(0,2); // only replace back two legs with stationary swim posture 
    }

    if(state==SUIT){
    // If swimming, calculate spine angles and set static leg angles
        if(globalConfig.swimPrints()){
            cout << "Assigning suit-up leg angles " << endl; 
        }
        calcLegAngles = suitLegPos; 
    }

    // Set angles calculated from above 
    // angles - dynamic 
    angles.block<4,1>(0,0) =calcLegAngles.block<4,1>(0,0);
    angles.block<4,1>(4,0) =calcLegAngles.block<4,1>(0,1);
    angles.block<4,1>(8,0) =calcLegAngles.block<4,1>(0,2);
    angles.block<4,1>(12,0)=calcLegAngles.block<4,1>(0,3);
    angles.block<2,1>(16,0)=q_trunk; // Trunk angles 
    if(globalConfig.useTail()){
        for(int i=0; i < globalConfig.nbMotorsTail(); i++){
			angles(18 + i) = q_tail(i);
		}
    }

    joint_angles=angles;

    // Publish / log data 
    if(globalConfig.logData()){
        logData(); 
    }

    if(globalConfig.useROS()){
        publishData(webots_time);
    }

    return true;
}



/**
 * \brief Writes angles calculated by runStep function into a table - interface with Pleurobot class 
 * 
 */
void
Controller :: getAngles(double *table)
{
        static MatrixXd angles_old=init_angles;
        static MatrixXd angles2=transformation_Ikin_Robot(init_angles,1,1);
        static MatrixXd angles2_old=transformation_Ikin_Robot(init_angles,1,1);

        // Sensing if robot is flipped
        is_flipped = abs(forRPY(0))>my_pi/2;
        static bool is_flipped_old = false;
        if(is_flipped!=is_flipped_old){
            T_trans=T_trans0;
        }
        is_flipped_old=is_flipped;

        if(is_flipped){
            angles=flipKinematics(angles);
        }

        //smoothing trajectories
        angles=pt1_vec(angles, angles_old, T_trans, dt);         // pt1_vec is filtering from misc_math.cpp in Libraries/Utils 

        angles_old=angles;

        if(globalConfig.isSimulation()){
            angles2=transformation_Ikin_Webots(angles,1, 1);
        }

        if(!globalConfig.isSimulation()){
            angles2=transformation_Ikin_Robot(angles,1,1);
        }

        //smoothing trajectories
        angles2=pt1_vec(angles2, angles2_old, Tfilt_angles, dt);
        angles2_old=angles2;

        for(int i=0; i < globalConfig.nbMotors(); i++){
            table[i]=angles2(i);
        }


    return;
}

/* Writes torques calculated by runStep function into a table - interface with Pleurobot class */

void
Controller :: getTorques(double *table)
{
        static MatrixXd torques2(globalConfig.nbMotors(), 1);
        //static MatrixXd torques(27,1);

        static MatrixXd torques2_old=MatrixXd::Zero(globalConfig.nbMotors(), 1);
        static MatrixXd torques_old=MatrixXd::Zero(globalConfig.nbMotors(), 1);

        if(globalConfig.isSimulation()){
            torques2=transformation_Ikin_Webots(torques, 1, 0);
        }

        if(!globalConfig.isSimulation()){
            torques2=transformation_Ikin_Robot(torques, 1, 0);
        }


        for(int i=0; i < globalConfig.nbMotors(); i++){
            table[i]=torques2(i);
        }


        // CONSTRAINTS
    if(!globalConfig.isSimulation()){
        for(int i=0; i < globalConfig.nbMotors(); i++){
            table[i]=table[i]>7?7:table[i];
            table[i]=table[i]<-7?-7:table[i];
        }
    }

    return;
}


/* Estimates foot contact for stance and swing (obstacle) phases and for different reflexes*/
void
Controller :: contactDetection()
{
    double limF, limH, forceFilter=0.1;
    Vector3d tmpForce;
    reCon << 0, 0, 0, 0;
    rsCon << 0, 0, 0, 0;
    vmCon << 0, 0, 0, 0;

    // leg extension reflex
    static MatrixXd force_filt1=MatrixXd::Zero(12, 1);
    limF=10;
    limH=10;
    force_filt1=pt1_vec(force, force_filt1, forceFilter, dt);

    if(force_filt1.block<3,1>(0,0).norm()>limF)
        reCon(0)=1;
    if(force_filt1.block<3,1>(3,0).norm()>limF)
        reCon(1)=1;
    if(force_filt1.block<3,1>(6,0).norm()>limH)
        reCon(2)=1;
    if(force_filt1.block<3,1>(9,0).norm()>limH)
        reCon(3)=1;


    // stumble reflex
    static MatrixXd force_filt2=MatrixXd::Zero(12, 1);
    limF=30;
    limH=50;
    force_filt2=pt1_vec(force, force_filt2, forceFilter, dt);

    if(force_filt2.block<3,1>(0,0).norm()>limF)
        rsCon(0)=1;
    if(force_filt2.block<3,1>(3,0).norm()>limF)
        rsCon(1)=1;
    if(force_filt2.block<3,1>(6,0).norm()>limH)
        rsCon(2)=1;
    if(force_filt2.block<3,1>(9,0).norm()>limH)
        rsCon(3)=1;

    // posture control
    static double ffl3=0, ffr3=0, fhl3=0, fhr3=0;

    ffl3=pt1(force.block<3,1>(0,0).norm(), ffl3, forceFilter, dt);
    ffr3=pt1(force.block<3,1>(3,0).norm(), ffr3, forceFilter, dt);
    fhl3=pt1(force.block<3,1>(6,0).norm(), fhl3, forceFilter, dt);
    fhr3=pt1(force.block<3,1>(9,0).norm(), fhr3, forceFilter, dt);
}


/* Update robot positions and fbck_torques */
void
Controller :: updateRobotState(double *d_posture, double *d_current)
{
    for(int i=0; i < globalConfig.nbMotors(); i++){
        fbck_angles(i)=d_posture[i];
        fbck_current(i)=d_current[i];
    }

    if(globalConfig.isSimulation()){
        fbck_angles=transformation_Ikin_Webots(fbck_angles, -1, 1);
        fbck_torques=transformation_Ikin_Webots(fbck_current, -1, 0);
    }
    else{
        fbck_angles=transformation_Ikin_Robot(fbck_angles, -1, 1);
        fbck_torques=transformation_Ikin_Robot(fbck_current, -1, 0);
    }

}

/* Get sensor data */
void
Controller :: getSensors(int acc, double *accData_i, int gyro, double *gyroData_i, int compass, double *compassData_i, int gps, double *gpsData_i)
{

    if(acc){
        accData[0]=accData_i[0];
        accData[1]=accData_i[1];
        accData[2]=accData_i[2];
    }
    if(gyro){
        gyroData[0]=gyroData_i[0];
        gyroData[1]=gyroData_i[1];
        gyroData[2]=gyroData_i[2];
    }
    if(compass){
        compassData[0]=compassData_i[0];
        compassData[1]=compassData_i[1];
        compassData[2]=compassData_i[2];
    }
    if(gps){
        gpsData[0]=gpsData_i[0];
        gpsData[1]=gpsData_i[1];
        gpsData[2]=gpsData_i[2];
    }

    #ifdef OPTIMIZATION
    rolling+=gyroData[0]*gyroData[0]*dt;
    pitching+=gyroData[2]*gyroData[2]*dt;
    yawing+=gyroData[1]*gyroData[1]*dt;
    if(t>0.7){
    for(int i=0; i<27; i++){
        applied_torques+=fbck_torques(i)*fbck_torques(i)*dt;
        if(i<11){
            applied_torques_s+=fbck_torques(i)*fbck_torques(i)*dt;
        }
        else{
            applied_torques_l+=fbck_torques(i)*fbck_torques(i)*dt;
        }
    }
    }
    #endif
}


/* Get acc data */
void
Controller :: getAcceleration(double acc[3])
{
    accData[0]=acc[0];
    accData[1]=acc[1];
    accData[2]=acc[2];
}


/* Get gps */
void
Controller :: getGPS(double *gps, double *gps_feet1, double *gps_feet2, double *gps_feet3, double *gps_feet4){
    for(int i=0; i<3; i++){
        feetGPS(i,0)=gps_feet1[i];
        feetGPS(i,1)=gps_feet2[i];
        feetGPS(i,2)=gps_feet3[i];
        feetGPS(i,3)=gps_feet4[i];
        gpsPos(i)=gps[i];
    }

}


void
Controller :: getGlobalCoM(double pos[3]){
    Vector3d tmp;

    tmp=forRotMat*realCoM;

    pos[0]=tmp(0)+gpsPos(0);
    pos[1]=tmp(2)+gpsPos(1);
    pos[2]=-tmp(1)+gpsPos(2);


}


Matrix<double, 3, 4>
Controller :: getGlobalSupportPoly(){
    Vector3d tmp;
    MatrixXd globalPoly(3,4);
    Vector4d tmp_vector;

    for(int i=0; i<4; i++){
        tmp_vector << feetMod.block<3,1>(0,i), 1;
        if(i<2){
            //tmp_vector.block<3,1>(0,0)=AngleAxisd(forTraj(2,1)-girdleTraj(2,1), Vector3d::UnitZ())*tmp_vector.block<3,1>(0,0); // girdle frame to girdle
            tmp_vector.block<3,1>(0,0)=Fgird.block<3,3>(0,0)*tmp_vector.block<3,1>(0,0);
        }
        else{
            //tmp_vector.block<3,1>(0,0)=AngleAxisd(forTraj(5,1)-girdleTraj(5,1), Vector3d::UnitZ())*tmp_vector.block<3,1>(0,0); // girdle frame to girdle
            tmp_vector.block<3,1>(0,0)=Hgird.block<3,3>(0,0)*tmp_vector.block<3,1>(0,0);
        }

        tmp=forRotMat*(supportPolys[0].block<3,1>(0,i)+tmp_vector.block<3,1>(0,0));
        //tmp=(supportPolys[0].block<3,1>(0,i)+tmp_vector.block<3,1>(0,0));
        //tmp=forRotMat*(supportPolys[0].block<3,1>(0,i)+feetMod.block<3,1>(0,i));

        globalPoly(0,i)=tmp(0)+gpsPos(0);
        globalPoly(1,i)=tmp(2)+gpsPos(1);
        globalPoly(2,i)=-tmp(1)+gpsPos(2);
    }


    return globalPoly;
}

/* Get IMU data */
void
Controller :: getAttitude(double *rotmat)
{
    for(int i=0; i<3; i++){
        fbck_fgirdRotMat(0,i)=rotmat[i];
        fbck_fgirdRotMat(1,i)=rotmat[i+3];
        fbck_fgirdRotMat(2,i)=rotmat[i+6];
    }


    forRotMat = fgird2ForRotMat.inverse()*fbck_fgirdRotMat;

    forRPY = forRotMat.eulerAngles(2,1,0);
    forRPY(0)=    atan2( forRotMat(2,1),forRotMat(2,2));
    forRPY(1)=    atan2( -forRotMat(2,0),sqrt(forRotMat(2,1)*forRotMat(2,1)+forRotMat(2,2)*forRotMat(2,2)));
    forRPY(2)=    atan2( forRotMat(1,0),forRotMat(1,1));

}

void
Controller :: getRPY(double *rpy)
{
    forRPY(0)=  rpy[0];
    forRPY(1)=  rpy[1];
    forRPY(2)=  rpy[2];
}


// void
// Controller :: sendStatusToMatlab(int downsampling, const char *IPadr)
// {
//     static unsigned long int cnt=0;
//     cnt++;
//     if(cnt%downsampling){
//         return;
//     }

//     std::vector<float>data;

//     // angles
//     for(int i=0; i < globalConfig.nbMotors(); i++){
//         data.push_back((float)angles(i));
//     }

//     //feedback angles
//     for(int i=0; i < globalConfig.nbMotors(); i++){
//         data.push_back((float)fbck_angles(i));
//     }

//     // current
//     for(int i=0; i < globalConfig.nbMotors(); i++){
//         data.push_back((float)fbck_current(i));
//     }

//     // raw optoforce
//     for(int i=0; i<12; i++){
//         data.push_back((float)rawForce(i));
//     }

//     // feet force - optoforce
//     for(int i=0; i<12; i++){
//         data.push_back((float)feet_force(i));
//     }

//     // // feet force
//     // for(int i=0; i<12; i++){
//     //     data.push_back((float)nnEstForce(i));
//     // }

//     // IMU
//     for(int i=0; i<3; i++){
//         data.push_back((float)forRPY(i));
//     }
//     // kinematics
//     for(unsigned int i=0; i<HJfl_g.size(); i++){
//         for(unsigned int j=0; j<3; j++)
//             data.push_back((float)HJfl_g[i](j, 3));
//     }
//     for(unsigned int i=0; i<HJfr_g.size(); i++){
//         for(unsigned int j=0; j<3; j++)
//             data.push_back((float)HJfr_g[i](j, 3));
//     }
//     for(unsigned int i=0; i<HJhl_g.size(); i++){
//         for(unsigned int j=0; j<3; j++)
//             data.push_back((float)HJhl_g[i](j, 3));
//     }
//     for(unsigned int i=0; i<HJhr_g.size(); i++){
//         for(unsigned int j=0; j<3; j++)
//             data.push_back((float)HJhr_g[i](j, 3));
//     }
//     for(unsigned int i=0; i<HJs_g.size(); i++){
//         for(unsigned int j=0; j<3; j++)
//             data.push_back((float)HJs_g[i](j, 3));
//     }
//     for(unsigned int i=0; i<HJt_g.size(); i++){
//         for(unsigned int j=0; j<3; j++)
//             data.push_back((float)HJt_g[i](j, 3));
//     }

//     sendUDP(data.data(), data.size()*sizeof(float), IPadr, 8472);
// }

int
Controller :: getRobotState()
{
    return state; 
}
