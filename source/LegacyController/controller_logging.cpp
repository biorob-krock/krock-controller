/**
 * @file controller_logging.cpp
 * @author Legacy
 * @brief Functions for logging (legacy)
 * @version 0.1
 * @date 2021-02-08
 * 
 * @todo format doxygen comments to match the same standard
 * 
 */
#include <LegacyController/controller.hpp>
#include <iostream>
#include <cmath>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>      

#include <iostream>
#include <chrono>
#include <ctime>   

//Logging 
#include <bits/stdc++.h> 
#include <iostream> 
#include <chrono>
#include <sys/stat.h> 
#include <sys/types.h> 
#include <ctime>   
#include <iostream>
#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>
#include <experimental/filesystem>
#include <string>

#include <RobotConfig/global_config.hpp>

namespace fs = std::experimental::filesystem;
using namespace std;


#define NC  "\033[0m"
#define RED  "\033[1;31m"
#define GREEN  "\033[1;32m"

    // RED='\033[0;31m'
    // REDL='\033[1;31m' # "light red"
    // CYANL='\033[1;36m'
    // NC='\033[0m'        # No Color

/* Logging forward kinematics, commanded, and measured angles from controller */ 
// time seems to start/stop with the controller 
void
Controller :: logData()
{
/*! \brief Log system data to text files for later plotting or debugging. 
 *
 *  Open several text files kept in /data/ folder.
 *  Write a tsv files. 
 *  Currently parsed and plotted by several python files.
 */

    // Controller time 
    static ofstream timeLOG("./data/timeLOG.txt");
    timeLOG << t << endl;

    // Commanded angles 
    static ofstream anglesLOG("./data/anglesLOG.txt");
    for(int i=0; i < globalConfig.nbMotors(); i++){
        anglesLOG << angles[i] << "\t";
    }
    anglesLOG << endl; 

    // TODO: WHAT IS CRSTATE
    static ofstream crstateLOG("./data/crstateLOG.txt");
    crstateLOG << crstate << endl;

    // Measured angles  
    static ofstream positionLog("./data/positionLog.txt");
    for(int i=0; i < globalConfig.nbMotors(); i++){
        positionLog << fbck_angles[i] << "\t";
    }
    positionLog << endl; 

    // Measured currents
    static ofstream torqueLog("./data/torqueLog.txt");
    for(int i=0; i < globalConfig.nbMotors(); i++){
        torqueLog << fbck_torques[i] << "\t";
    }
    torqueLog << endl; 

    // Forward kinematics 
    static ofstream fkinLog("./data/fkinLog.txt");
    for(int i=0;i<5;i++){
        for(int j=0;j<3;j++)
            fkinLog<<HJfl_g[i](j, 3)<<"\t";
    }
    for(int i=0;i<5;i++){
        for(int j=0;j<3;j++)
            fkinLog<<HJfr_g[i](j, 3)<<"\t";
    }
    for(int i=0;i<5;i++){
        for(int j=0;j<3;j++)
            fkinLog<<HJhl_g[i](j, 3)<<"\t";
    }
    for(int i=0;i<5;i++){
        for(int j=0;j<3;j++)
            fkinLog<<HJhr_g[i](j, 3)<<"\t";
    }
    for(int j=0;j<3;j++)
            fkinLog<<Fgird(j, 3)<<"\t";
    for(int i=0;i<globalConfig.nbMotorsTrunk();i++)
        for(int j=0;j<3;j++)
            fkinLog<<HJs_g[i](j, 3)<<"\t";
    for(int j=0;j<3;j++)
        fkinLog<<Hgird(j, 3)<<"\t";
    fkinLog<<endl;


    // Optoforce readings 
    static ofstream forceLOG("./data/forceLOG.txt");
    // forceLOG << nnEstForce.block<3,1>(0,0).transpose() << "\t";
    // forceLOG << nnEstForce.block<3,1>(0,1).transpose() << "\t";
    // forceLOG << nnEstForce.block<3,1>(0,2).transpose() << "\t";
    // forceLOG << nnEstForce.block<3,1>(0,3).transpose() << "\t";
    forceLOG << feet_force_reference.block<3,1>(0,0).transpose() << "\t";
    forceLOG << feet_force_reference.block<3,1>(0,1).transpose() << "\t";
    forceLOG << feet_force_reference.block<3,1>(0,2).transpose() << "\t";
    forceLOG << feet_force_reference.block<3,1>(0,3).transpose() << endl;

    // IMU readings 
    static ofstream imuLOG("./data/imuLOG.txt");
    imuLOG << forRPY.transpose() << endl;

    // Feet positions
    static ofstream feetReferenceLog("./data/feetReferenceLog.txt");
    for(int i=0; i<4; i++){
        feetReferenceLog << ikinRef.block<3,1>(0,i).transpose() << "\t";
    }
    for(int i=0; i<4; i++){
        feetReferenceLog << feetLocationsLocal.block<3,1>(0,i).transpose() << "\t";
    }
    for(int i=0; i<4; i++){
        feetReferenceLog << feetReference.block<3,1>(0,i).transpose() << "\t";
    }
    for(int i=0; i<4; i++){
        feetReferenceLog << feetReference_forceCorrections.block<3,1>(0,i).transpose() << "\t";
    }
    feetReferenceLog << endl;

}


void 
Controller :: printControlStatus()
{
/**! 
 * \brief Print out a reasonable number of motor positions to help in debugging  
 *  
 * Toggle globalConfig.printOverwrite() (set in GLOBAL.cfg) allows printouts to overwrite each other (instead of scrolling) 
 * but this will inhibit reading errors and other printouts thrown in the rest of the code  
 */
    double PRINT_PERIOD = 0.5;  // seconds 
    int PRINTOUT_PRECISION = 4; // sig fig on motor readouts 
    int PRINTLENGTH = 24;       // Number of carriage returns when overwriting terminal 

    auto now = std::chrono::system_clock::now();

    std::chrono::duration<double> system_running = now-controllerClockStart;
    std::chrono::duration<double> elapsed_seconds = now-prevPrint;

    if( elapsed_seconds.count() >= PRINT_PERIOD){
        if(globalConfig.printOverwrite())
        {
            cout << "\033[" << PRINTLENGTH << "A";
        } 
        else{
            cout << "\n" << endl; 
        }

        // State 
        cout << "State: \t";

        switch(state){
            case INITIAL:   cout <<GREEN << "INITIAL\t"<<NC; break; 
            case STANDING:  cout << "STANDING\t"; break; 
            case WALKING:   cout << "WALKING\t"; break; 
            case HIGH_STEP:  cout << "HIGH STEP\t"; break; 
            case SWIMMING:  cout << "SWIMMING\t"; break; 

        }
        cout << endl;
        
        cout << "System time: " << setprecision(PRINTOUT_PRECISION) << system_running.count() << endl; 

        // Commanded angles 
        cout << "\t\t COMMANDED \t FEEDBACK" << endl; 
        for(int i = 0; i < globalConfig.nbMotors(); i++){
            cout <<"Motor " << i << ":\t" 
                << setprecision(PRINTOUT_PRECISION) << angles(i) << "\t\t" 
                << fbck_angles(i) << endl; 
        }
        prevPrint = now; 
    }
}


void 
Controller :: saveOutputLog()
{
    //Format string for 
    time_t now = time(0);  
    tm *ltm = localtime(&now);
    string datetime = to_string(1900 + ltm->tm_year) + "_" + 
        to_string(1 + ltm->tm_mon) + "_" +
        to_string(1 + ltm->tm_mday) + "_" +
        to_string(ltm->tm_hour) + "_" + // Daylight savings 
        to_string(1 + ltm->tm_min) + "_" + 
        to_string(1 + ltm->tm_sec); 
    string cpyDir = "data/" + datetime + "_" +  globalConfig.getLogDescription();
    const char *c = cpyDir.c_str(); 

  if (mkdir(c, 0777) == 0)
  {
    cout <<"Directory created: "<< c << '\n';

        // Copy data files 
        for(auto& p: fs::directory_iterator("data/"))
        {
            fs::path thisPath = p.path(); 
            string thisPath2;
            thisPath2 = string(thisPath);
            size_t isTxt = thisPath2.find(".txt");	

            if(isTxt != string::npos)
            {
                int omitParent = 5; // ommitting original parent directory [data/] in path  
                string filename = string(c)+ "/" + thisPath2.substr(omitParent);
                fs::copy(thisPath2, filename);
                cout <<"Copying: " << filename << endl;
            }
        }

        // Configuration files for reference
        // TODO: Replace this with a single config.txt file with the important parameters
        string configDir = cpyDir + "/config";
        const char *c = configDir.c_str();
        mkdir(c , 0777);

        for(auto& p: fs::directory_iterator("config/"))
        {
            fs::path thisPath = p.path(); 
            string thisPath2;
            thisPath2 = string(thisPath);

            size_t isTxt = thisPath2.find(".config");	
            if(isTxt != string::npos)
            {
                int omitParent = 7; // ommitting original parent directory [config/] in path  
                string filename = string(c)+ "/" + thisPath2.substr(omitParent);
                fs::copy(thisPath2, filename);
                cout <<"Copying: " << filename << endl;
            }

            isTxt = thisPath2.find(".cfg");	

            if(isTxt != string::npos)
            {
                int omitParent = 7; // ommitting original parent directory [config/] in path  
                string filename = string(c)+ "/" + thisPath2.substr(omitParent);
                fs::copy(thisPath2, filename);
                cout <<"Copying: " << filename << endl;
            }
        }
  }
  else{
    cout << "WARNING: Directory already exists" <<endl; 
  }  
}

void printBool(bool globalToggle){
/**! 
 * \brief Color printout to be used by Controller::printConfiguration() method during robot startup
 * 
 */

    #define NC  "\033[0m"
    #define RED  "\033[1;31m"
    #define GREEN  "\033[1;32m"

    if(globalToggle){
        cout << GREEN << "TRUE" << NC << endl;
    }
    else{
        cout << RED << "FALSE" << NC << endl;
    }
}

void 
Controller :: printConfiguration()
{
/**! 
 * \brief Print out high level configuration of robot to help in debugging
 *  
 * 
 */

    cout<< "\nConfiguration Toggles: " << endl;

    cout<< "Use ROS: \t\t" ;  
    printBool(globalConfig.useROS());    

    cout<< "Simulation: \t\t" ;
    printBool(globalConfig.isSimulation());    

    cout<< "High Level Controller: \t" ;
    printBool(globalConfig.highLevelControllerEnabled());    

    cout<< "Enable joystick: \t";
    printBool(globalConfig.useJoystick());    

    cout<< "Skip Hardware: \t";
    printBool(globalConfig.skipHardware());    
    cout << "\n" << endl;

    // In progress
    // if(globalConfig.useROS()){
    //     char* pPath;
    //     pPath = getenv ("ROS_MASTER_URI");
    //     if (pPath!=NULL){
    //         printf ("ROS_MASTER_URI: %s",pPath);
    //         cout << pPath << endl;
    //         cout << "Path is not null" << endl; 
    //     }
    // }

}

