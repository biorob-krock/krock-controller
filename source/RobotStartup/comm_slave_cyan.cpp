/**
 * @file comm_slave_cyan.cpp
 * @author Legacy
 * @brief comm_slave_cyan executable source code
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>

// cpp libraries 
#include <unistd.h>
#include <libserial/SerialPort.h>
#include <libserial/SerialStream.h>
#include <boost/array.hpp>
#include <boost/asio.hpp>

//  "lib" libraries headers
#include "vectornav.h"

// krock-controller headers
#include <RobotComm/ps3joy.hpp>
#include <RobotComm/robotStatus.hpp>
#include <Utils/utils.hpp>
#include <Utils/timeStamp.hpp>
#include <BoostLogger/boostlogger.hpp>

using namespace std;
using boost::asio::ip::udp;
using namespace boost::interprocess;

// using namespace LibSerial;
// namespace ls = LibSerial;

#define NC  "\033[0m"
#define RED  "\033[1;31m"
#define GREEN  "\033[1;32m"

bool isRunning(const char* name)  
{  
	char command[32];  
	sprintf(command, "pgrep %s > /dev/null", name);  
	return 0 == system(command);  
} 


/** 
 * @brief Runs on the Cyan, important for 1. Joystick communication 2. Toggle motors 3. start or stop main process
 * 
 * @details Connect to joystick and use some of the commands to toggle mortors (ON and OFF) and start or stop 
 * 			the main process (robot_main) from executing
 * 		
 * 
 * @note libserial version 1.0.4 used here, original krock-controller (ubuntu18 and before)
 * 		were on a older version (before 1.0.0 probably), and 
 * 
 * @todo rename the script and its executable
 * @todo remove the commented code for optoforce
 * @todo find the right place for this 
 * @todo rename namespace and use those 
 * @todo Use Boost Logger to log all the information
 * @todo Program a button for rosbag launch?
 * 		- check if ros is running (and a pc is connected ?)
 * 		- launch and stop rosbag (use a detached thread maybe?)
 * @todo Use global config to use the same name for shared objects ?
 */
int main(){
	AddTimeStamp ats_cout(std::cout);
    AddTimeStamp ats_cerr(std::cerr);
    AddTimeStamp ats_clog(std::clog);

	cout << GREEN << "[START-MAIN]" <<  NC << endl;
	bool joystick_plugged;
	bool start_pressed, start_pressed_old=false;
	bool select_pressed, select_pressed_old=false;
	bool joy_l2, joy_l1, joy_r1, joy_r2;
	bool imu_plugged;
	
	// ================= INITIALIZE JOYSTICK =====================================
	shared_memory_object::remove("ps3shm");
	PS3joy ps3joy((const char*)"ps3shm");
	joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");
	// ================= INITIALIZE SENSORS =====================================
	Vn100 vnIMU;
	imu_plugged = InitIMU(&vnIMU, "/dev/vectornav_vn100", 115200);
	// ================= INITIALIZE ROBOT STATUS =====================================
	///@todo check why shared memory should be removed before 
    shared_memory_object::remove("sensorsStruct_shm");
	shared_memory_object::remove("rstatus2unity_shm");
	RobotStatus rStatus((const char*)"rstatus2unity_shm", (const char*)"sensorsStruct_shm", false );
	///@todo why is this required ???????????????
	rStatus.WriteToRobotSharedMemory();
	// ================= INITIALIZE TTL SWITCH =====================================
	LibSerial::SerialPort ttlSwitch("/dev/ttl_switch");
	ttlSwitch.SetBaudRate(LibSerial::BaudRate::BAUD_57600);
	ttlSwitch.SetCharacterSize(LibSerial::CharacterSize::CHAR_SIZE_8);
	ttlSwitch.SetFlowControl(LibSerial::FlowControl::FLOW_CONTROL_HARDWARE);
	ttlSwitch.SetParity(LibSerial::Parity::PARITY_NONE);
	ttlSwitch.SetStopBits(LibSerial::StopBits::STOP_BITS_1);

	// ================= INITIALIZE THE LOOP =====================================
	const double Td=10/1000.;
	double t=0, dt, t0, t1, t2;
	t0=get_timestamp();
	int k=0; 
	bool motorsOn=false;
	// ================= THE LOOP =====================================
	while(1){
		dt=get_timestamp()-t0;
		t0=get_timestamp();
		t=t+dt;
		// ============================= READ JOYSTICK ===================================
		if(!joystick_plugged){
        	joystick_plugged = ps3joy.OpenDevice((char*)"/dev/input/js0");
        }
        else{
            ps3joy.GetInput();
            ps3joy.WriteSharedMemory();
        }
        // ============================= WRITE SENSORS TO SHARED MEMORY ===================================
		if(imu_plugged){
            rStatus.sensor_status_struct.imu_plugged=1;
            ReadIMUrpy(&vnIMU, rStatus.sensor_status_struct.imu);
		}
		else{
			imu_plugged = InitIMU(&vnIMU, "/dev/vectornav_vn100", 115200);
          	rStatus.sensor_status_struct.imu_plugged=imu_plugged;
		}
		rStatus.WriteToSensorSharedMemory();
		/// @todo: log the information correctly
		cout << "imu \t";
		for(int i=0; i<3; i++){
			LOG_INFO << rStatus.sensor_status_struct.imu[i] << "\t";
		}
		cout << endl;

        //========= POWER MOTORS ================
		if(ps3joy.joyStruct.select_pressed && !ps3joy.joyStruct.select_pressed_old && !ps3joy.joyStruct.LR_pressed){
			/// @todo: log the information correctly
			motorsOn=!motorsOn;
		}
		/// @todo check why are we using !, and setting status multiple times ?
		ttlSwitch.SetRTS(!motorsOn);

		cout << ((motorsOn)? GREEN: RED) 		<< "[MOTOR-VALUES] TTLSwitch Status:   " << std::boolalpha <<  motorsOn 		<< NC << endl;
		cout << ((imu_plugged)? GREEN: RED) 	 	<< "[SENSOR-IMU] initialized:          " << std::boolalpha << imu_plugged 		<< NC << endl;
		cout << ((joystick_plugged)? GREEN: RED) << "[JOYSTICK] initialized:            " << std::boolalpha << joystick_plugged  << NC << endl; 
		//========= RUN or STOP robotController PROGRAM IF START IS PRESSED ================
		if(ps3joy.joyStruct.start_pressed && !ps3joy.joyStruct.start_pressed_old  && !ps3joy.joyStruct.LR_pressed){
			if(isRunning("robot_main")) {
				printf("MAIN IS RUNNING. STOPING IT NOW!\n");
				 //A process having name robot_main is running.
			}
			else{
				printf("MAIN IS NOT RUNNING. STARTING IT NOW!\n");
				system("cd /home/biorob/krock-controller; ./robot_main & ");
			  //A process having name robot_main is NOT running.

			}
		}

        //=========================== TIME STEP ===============================
		t1=get_timestamp();
		cout.flush();
		if(Td>(t1-t0)){
		    sleepBoost(Td- (t1-t0));
		}
		t2=get_timestamp();

    }

	return 0;
}
