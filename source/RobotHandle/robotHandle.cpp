/**
 * @file robotHandle.cpp
 * @author Legacy
 * @brief RobotHandle class definitions
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <iostream>
#include <fstream>
#include <cmath>

// helpers
#include <RobotConfig/global_config.hpp>
#include <BoostLogger/boostlogger.hpp>
#include <RobotComm/robotStatus.hpp>


#include <RobotHandle/robotHandle.hpp>


using namespace std;


extern double get_timestamp();

/// @note dx_write is mutex for writing angles and dx_read is for reading feedback values
/// @note These are global mutex and thus are shared by the robotComm thread as well 
std::mutex dx_write, dx_read;

/// @todo create a robotComm as a static function of the robotHandle class and use that for thread 
/// @todo use set and get Position and Torque methods (even inside the robotComm method itself)
/// @todo any data exchange should always happen using these getter and setter methods
/// @todo Refactor such that setter and getter for dx_motor are different from the handle 

/**
 * @brief A thread that performs reads and writes to RobotHandle(robot) object
 *          by aquiring and releasing lock, at 100 Hz
 *  
 * @details robotComm thread is initialised and detached from RobotHandle class 
 *          It has RobotHandle object and used_ids passed as a pointers
 *          This allows the thread to operate on the passed objects and share resources 
 *          with the main thread. This thread will exit only when `robot->stopThread` 
 *          is set to true. This was the author's way for having responsibility of 
 *          data update (hardware data) separate from the controller logic. 
 * @param robot 
 * @param numMotors 
 * @param used_ids 
 *  
 */
void robotComm(RobotHandle *robot, int numMotors, int *used_ids){
    double t1, t2, t3, dt;
    double fbck_angles[MAX_NUM_SERVOS];
    double fbck_current[MAX_NUM_SERVOS];
    double angles[MAX_NUM_SERVOS];
    bool positionTorqueToggle=false;
    
    vector<int> position_ctrl_servos(robot->globalConfigRH.nbMotors());
    for(int i=0; i < numMotors; i++){
        position_ctrl_servos[i]=robot->globalConfigRH.getMotorsIDs()[i];
    }

    //static ofstream mpcTimeLog("./data/mpcTimeLog.txt");
    while(!robot->stopThread){
        // cout << "Robot thread " << robot->stopThread <<endl;
        t1=get_timestamp();

        // READ FEEDBACK
        if(robot->readPosition && robot->readCurrent){
            positionTorqueToggle=!positionTorqueToggle;     // each cycle read only position or torque data
            if(positionTorqueToggle){
                robot->getAllPositions(fbck_angles);
            }
            else{
                robot->getAllCurrents(fbck_current);
            }
        }
        else if(robot->readPosition){
            robot->getAllPositions(fbck_angles);
        }
        else if(robot->readCurrent){
            robot->getAllCurrents(fbck_current);
        }

        // write the feedback from temp variables to RobotHandle object (robot)
        dx_read.lock();
            for(int i=0; i<numMotors; i++){
                robot->fbck_angles[i] = fbck_angles[i];
                robot->fbck_current[i] = fbck_current[i];
            }
        dx_read.unlock();

        //usleep(500);

        // write the angles from robot handle to temp variables
        dx_write.lock();
            for(int i=0; i<numMotors; i++){
                angles[i] = robot->angles[i];
            }
        dx_write.unlock();

        // set the angles in temp variables to robots
        if(robot->writeAngles){
            //cout << "Writing angles" << endl;
            robot->setAllPositions(angles, robot->used_IDs);
        }

        t2=get_timestamp();

        // SLEEP
        if(0.01 > (t2-t1)){
            usleep((0.01 - (t2-t1))*1000000);
        }

        t3=get_timestamp();
        //for(int i=0; i<10; i++){
        //    cout << angles[i] << endl;
        //}
        //
    }
    cout << "Robot thread ending " <<endl;
}


/**
 * @brief Construct a new Robot Handle:: Robot Handle object
 * 
 * @param motorIDs 
 * @param enabledMotors 
 * @param nbMotors 
 * @todo check if pass by reference possbile
 * @todo check if vectors can be directly used
 */
// RobotStatus(const char *robotStatusShmName, const char *sensorStatusShmName, bool skipHardware);
// RobotHandle::RobotHandle(std::vector<int>  motorIDsConfig, std::vector<bool> enabledMotorsConfig,  int  nbMotorsConfig){
RobotHandle::RobotHandle(GlobalConfig &inGlobalConfig){

    globalConfigRH = inGlobalConfig;
    std::vector<int>  motorIDs = globalConfigRH.getMotorsIDs();
    std::vector<bool> enabledMotors = globalConfigRH.getEnabledMotors();
    int  nbMotors = globalConfigRH.nbMotors();

    readPosition = false;
    readCurrent = false;
    writeAngles = false;
    stopThread = false;

    int motor_IDs[100];
    for (int i = 0; i <  motorIDs.size(); i++)
    {
        motor_IDs[i] = motorIDs[i];
    }

    int enabled_motors[100];
    for (int i = 0; i < enabledMotors.size(); i++)
    {
        enabled_motors[i] = enabledMotors[i];
    }
    if(!globalConfigRH.skipHardware()){
        /// @todo remove these parts and take only from config
	    initMotorSystem(init_max_speed, init_max_torque, nbMotors, motor_IDs, enabled_motors, "/dev/usb2rs485");
    }
    // cout<< GREEN << "ROBOT INITIALIZED" << NC <<endl;
    /// @todo: Take the max values from config 
    /// @todo: Keeping this piece of code for now, to make preliminary checks
    if(!globalConfigRH.skipHardware())
    {
        usleep(50000);
        cout << "max torque: " << globalConfigRH.maxTorque() <<"\t" <<"max speed: "<< globalConfigRH.maxSpeed() << endl;
        setMaxSpeed(globalConfigRH.maxSpeed());
        usleep(10000);
        setMaxTorque(1023);
        usleep(10000);
        setTorqueLimit(globalConfigRH.maxTorque());
        usleep(1000);
    }

    /// @note: moved to robot_handle 
    /// @todo: maybe  shift the robot status cpp and hpp files to robot handle directory ?
    /// @todo: remove hard coding
    bool skip_hardware = globalConfigRH.skipHardware();
    rStatus = RobotStatus((const char*)"rstatus2unity_shm", (const char*)"sensorsStruct_shm", skip_hardware);
    initSharedMemory();
    /// @todo: replace with boost log 
    // if(globalConfigRH.configPrints()){
    //     printf("written to a shm\n");
    // }
}

/**
 * @brief Initalize shared memory
 * 
 */
void RobotHandle::initSharedMemory(){
    /// @note why a write for robot status is need ?
    rStatus.WriteToRobotSharedMemory();
    LOG_INFO << "written to a shm" << endl;

    ///@todo delete old code
    // rStatus.WriteSharedMemory();

    // if(globalConfigRH.configPrints())
    // {
    //     printf("written to a shm\n");
    // }
    // rStatus.sensor_status_struct_shm = ssshm.find_or_construct<rStatus.sensor_status_struct>("rStatus.sensor_status_struct")();
}

/**
 * @brief Initialize Motors
 * 
 * @param maxSpeed 
 * @param maxTorqueIn 
 * @param numMotorsConfig 
 * @param usedIDs 
 * @param enabledMotorIDs 
 * @param portName 
 * @return true 
 * @return false 
 */
bool RobotHandle::initMotorSystem(
    int maxSpeed,
    int maxTorqueIn,
    int numMotorsConfig,
    int usedIDs[30],
    int enabledMotorIDs[30],
    const char *portName){
    dx_motors_sys = *dxsystemSetup(1, portName);
    // LOG_INFO << "Creating dynamixel system"<<endl;
    numMotors = dx_motors_sys.getNumMotors();
    // // dx_motors_sys.allID(); 
    // LOG_INFO << "\n" << "Found Motors = "<<numMotors << endl;
    // for(int i=0; i<numMotors; i++){
    //     LOG_INFO << "ID: "<< dx_motors_sys.motorNum2id(i) << "\t" << "model: " << dx_motors_sys.getModel(i) << endl;
    // }
    // LOG_INFO << "\n" << endl;


    if(numMotors!=numMotorsConfig){
        LOG_ERROR << "WRONG NUMBER OF MOTORS IN THE CONFIG FILE." << endl;
        LOG_ERROR << "NumMotors: " << numMotors << ",  NumMotorsConfig: " << numMotorsConfig << endl;

        numMotors = numMotorsConfig;
        throw std::invalid_argument("Wrong number of motors connected");
        return false;
    }
    else{
        LOG_INFO << "Correct number of motors were found: " << globalConfigRH.nbMotors() << endl;
    }


    ServosOn(enabledMotorIDs);

    /* USED MOTORS */
    for(int i = 0; i < numMotors; i++){

        ids_arr[i] = enabledMotorIDs[i];
        // LOG_INFO << i << " ENABLED IDS " << ids_arr[i] << endl;
        if(enabledMotorIDs[i]){
            used_IDs.push_back(usedIDs[i]);
        }

    }

    /* Initialize center values of servos */
    ///@note these are also the values set by InitialPosture
    for (int i = 0; i < numMotors; i++)
    {
        d_center[i] = 0;
    }

    //d_center[numMotors-16+2] = -90 * M_PI / 180.0;
    //d_center[numMotors-16+2+4] = 90 * M_PI / 180.0;
    //d_center[numMotors-16+2+8] = -90 * M_PI / 180.0;
    //d_center[numMotors-16+2+12] = 90 * M_PI / 180.0;

    setAllDelay(0);

    usleep(10000);
    // set servos max speed
    setMaxSpeed(maxSpeed);
    usleep(400000);
    // set servos max torque
    setTorqueLimit(maxTorqueIn);

    usleep(400000);
    InitialPosture(ids_arr);
    usleep(400000);

    int used_ids_thread[30];
    for(int i=0; i<30; i++){
        used_ids_thread[i] = used_IDs[i];
    }
    robot_comm_thread=std::thread(robotComm, this, numMotors, used_ids_thread);  //robotComm(Robot *robot, int numMotors, int *used_ids)
    robot_comm_thread.detach();

    return true;
}

/**
 * @brief Destroy the Robot Handle:: Robot Handle object
 * 
 */
RobotHandle::~RobotHandle(){
    cout << "Starting RobotHandle out"<< endl;
    if(!globalConfigRH.skipHardware())
    {
        ///@note It is very impotant ti call thread stop while using functions that directly use motor functions 
        this->stopThread = true;
        setMaxSpeed(100);
        usleep(10000);
        InitialPosture(ids_arr);
        cout << "Set the initial posture back" << endl;
        usleep(2000000);
        cout << "SHUTING DOWN Robot Servo motors" << endl;
        ServosOff(ids_arr);
        cout << "Completed SHUTING DOWN Robot Servo motors" << endl;
        usleep(1000000);
    }
} 

/**
 * @brief 
 * 
 * @param imudata 
 */
void RobotHandle::readIMU(double *imudata){
    // memcpy(&rStatus.sensor_status_struct, rStatus.sensor_status_struct_shm, sizeof(rStatus.sensor_status_struct));
    rStatus.ReadFromSensorSharedMemory();
    imudata[0]=rStatus.sensor_status_struct.imu[0]*M_PI/180.;
    imudata[1]=rStatus.sensor_status_struct.imu[1]*M_PI/180.;
    imudata[2]=rStatus.sensor_status_struct.imu[2]*M_PI/180.;
}

/**
 * @brief readForceSensors
 * 
 * @param forcedata 
 * @param forcedataRaw 
 */
void RobotHandle::readForceSensors(double *forcedata, double *forcedataRaw){
    rStatus.ReadFromSensorSharedMemory();
    // memcpy(&rStatus.sensor_status_struct, rStatus.sensor_status_struct_shm, sizeof(rStatus.sensor_status_struct));
    for(int i = 0; i < 12; i++){
        forcedata[i]=rStatus.sensor_status_struct.optoforce[i];
    }
    for(int i=0; i<16; i++){
        forcedataRaw[i]=rStatus.sensor_status_struct.optoforceRaw[i];
    }
}

/**
 * @brief writeRobotStatus
 * 
 * @param joint_angles 
 * @param fbck_torques 
 * @param forRPY 
 * @param force 
 */
void RobotHandle::writeRobotStatus(MatrixXd joint_angles, MatrixXd fbck_torques, MatrixXd forRPY, MatrixXd force){
    for(int i=0; i < globalConfigRH.nbMotors(); i++){
        rStatus.robot_status_struct.motor_positions[i]=joint_angles(i);
        rStatus.robot_status_struct.motor_torques[i]=fbck_torques(i);
    }
    for(int i=0; i<2; i++){
        rStatus.robot_status_struct.imu[i]=forRPY(i);
    }
    for(int i=0; i<12; i++){
        rStatus.robot_status_struct.forces[i]=force(i);
    }
    rStatus.WriteToRobotSharedMemory();
}

/**
 * @brief Sets the position of one servo to an angle in radians
 * 
 * @param id 
 * @param angle 
 */
void RobotHandle::setSinglePosition(int id, double angle){
    dx_motors_sys.setPosition(id, angle);
}


/**
 * @brief Sets the torque of one servo
 * 
 * @param id 
 * @param torque 
 */
void RobotHandle::setSingleTorque(int id, int torque){
    //d_cm730->WriteWord(id, TORQUE_LIMIT_L, torque,0);
}


/**
 * @brief read individual servo position
 * 
 * @param id 
 * @return int 
 */
int RobotHandle::getSinglePosition(int id){
     return(dx_motors_sys.getPosition(id));
}

/**
 * @brief Sets the position of all servos to a table of angles in radians
 * 
 * @param angles 
 * @param ids 
 */
void RobotHandle::setAllPositions(double *angles, const vector<int>& ids){
    vector<int> goalPositions;
    for(unsigned int i=0; i<ids.size(); i++){
        if(ids_arr[i]){
            // cout << "writing to enabled id: " << i << "\t with ids_arr = " <<ids_arr[i] <<  endl; 
            goalPositions.push_back(angle2Position(angles[i], dx_motors_sys.getModel(i), IS_RADIAN));
            // cout << "Motor: " << i << "\t Desired angle: " << angles[i] << "\t Goal position: " << goalPositions[i] << endl; 
        }
        //cout << i<<"\t" <<ids[i]<<"\t" <<ids_arr[i]<< "\t" <<  goalPositions[i] <<"\t" << angles[ids[i]]<< endl;
    }

    // cout << "GOAL POSITIONS" << endl; 
    // for(int i = 0; i < globalConfigRH.nbMotors(); i++){
    //     cout << "Motor: " << i << "\t Goal position: " << goalPositions[i] << endl; 
    // }

    if(!globalConfigRH.allMotorsDisabled()){
        dx_motors_sys.setIDsPosition(ids, goalPositions);
    }

}

/**
 * @brief Sets the position of all servos to a table of angles in radians
 * 
 * @param torques 
 * @param ids 
 */
void RobotHandle::setAllTorques(double *torques, const vector<int>& ids){
    vector<int> goalTorques;
    for(unsigned int i=0; i<ids.size(); i++){
        if(ids_arr[i])
            goalTorques.push_back(translateTorque(torques[i]));
    }
    dx_motors_sys.setIDsTorques(ids, goalTorques);
}

/**
 * @brief Gets the position of all servos to a table of angles in radians
 * 
 * @param angles 
 */
void RobotHandle::getAllPositions(double *angles){
    vector<int> positions = dx_motors_sys.getAllPosition();
    for(int i=0; i<numMotors; i++){
        if(ids_arr[i])
            angles[i]=position2Angle(positions[i], dx_motors_sys.getModel(i));
    }

}


/**
 * @brief Gets the position of all servos to a table of angles in radians
 * 
 * @param angles 
 * @param used_IDs 
 */
void RobotHandle::getIDsPositions(double *angles, const vector<int>& used_IDs){
    vector<int> positions = dx_motors_sys.getIDsPosition(used_IDs);
    for(int i=0; i<numMotors; i++){
        if(ids_arr[i])
            angles[i]=position2Angle(positions[i], dx_motors_sys.getModel(i));
    }

}

/**
 * @brief Sets the position of all servos to center 
 * 
 * @param ids 
 */
void RobotHandle::InitialPosture(int *ids){
    //MovingSpeed(50, ids);
    //setMaxTorque(300, ids);
    ///@note d_center is == 0
    cout << "Init posture" << endl;
    /* Initialize center values of servos */
    ///@note these are also the values set by InitialPosture
    for (int i = 0; i < numMotors; i++)
    {
        d_center[i] = 0;
    }
    setAllPositions(d_center, used_IDs);
    usleep(2 * MILLION);
}

/**
 * @brief Sets the maximum torque of all servos to a specific value
 * 
 * @param torque 
 */
void RobotHandle::setMaxTorque(int torque){
    vector<int> torque_vec (numMotors,torque);
    dx_motors_sys.setAllTorqueMax(torque_vec);
}

/**
 * @brief Sets the maximum torque of all servos to a specific value
 * 
 * @param torque 
 */
void
RobotHandle::setTorqueLimit(int torque){
    vector<int> torque_vec (numMotors,torque);
    dx_motors_sys.setAllTorqueLimit(torque_vec);
}

/**
 * @brief Sets the moving speed of all servos to a specific value 
 * 
 * @param speed
 * @todo change name from setMaxSpeed to setMotorSpeed or something, Max speed is misleading
 */
void RobotHandle::setMaxSpeed(int speed){
    vector<int> goalSpeeds (numMotors,speed);
    dx_motors_sys.setAllSpeed(goalSpeeds);
}

/**
 * @brief Sets up torque mode
 * 
 * @param table 
 * @param ids 
 */
void RobotHandle::TorqueMode(int *table, int *ids){
//  SyncWriteByteTable(TORQUE_MODE_ENABLE, table, ids);
}


/**
 * @brief Turn servo power on
 * 
 * @param ids 
 */
void RobotHandle::ServosOn(int *ids){
    for(int i=0; i<numMotors; i++){
        if(ids[i]){
            dx_motors_sys.queueByte(i, P_TORQUE_ENABLE, 1);
        }
        else{
            dx_motors_sys.queueByte(i, P_TORQUE_ENABLE, 0);
        }
    }
    dx_motors_sys.executeQueue();
}


/**
 * @brief Turn servo power off
 * 
 * @param ids 
 */
void RobotHandle::ServosOff(int *ids){
    for(int i=0; i<numMotors; i++){
        if(ids[i]){
            dx_motors_sys.queueByte(i, P_TORQUE_ENABLE, 0);
        }
    }
    dx_motors_sys.executeQueue();
}


/**
 * @brief read individual servo torque enable
 * 
 */
void RobotHandle::getTorqueEnable(){
    for(int i=0; i<numMotors; i++){
        cout << "torque_en_" <<i<<": "<<dx_motors_sys.getTorqueEn(i) << "\t";
    }
    cout << endl;
}


/**
 * @brief Set retrun delay time of motors
 * 
 * @param val 
 */
void RobotHandle::setAllDelay(int val){
    dx_motors_sys.setAllDelay(val);
}

/**
 * @brief  Sets the P gain of all servos
 * 
 * @param Pgain 
 */
void RobotHandle::setAllGainP(int Pgain){
    vector<int> Pgains_vec;
    for(int i=0; i<numMotors; i++){
        if(ids_arr[i])
            Pgains_vec.push_back(Pgain);
    }
    dx_motors_sys.setAllGainP(Pgains_vec);
}


/**
 * @brief 
 * 
 * @param torque_ctrl_on 
 * @param ids 
 */
void RobotHandle::setTorqueControl(int *torque_ctrl_on, const vector<int>& ids){
    vector<int> trq_ctrl;
    for(unsigned int i=0; i<ids.size(); i++){
        if(ids_arr[i])
            trq_ctrl.push_back(torque_ctrl_on[i]);
    }
    dx_motors_sys.setIDsTorqueControl(ids, trq_ctrl);
}


/**
 * @brief Translate torque into range (-1023 to 1023)
 * 
 * @param torque 
 * @return int 
 */
int RobotHandle::translateTorque(double torque){
    int tornum=0;
    if(torque>1)
        torque=1;
    if(torque<-1){
        torque=-1;
    }
    if(torque>=0){
        tornum=(int)(torque*1023);
    }
    else{
        tornum=(int)(1023 + 1023*(-torque));
    }
    return tornum;
}

/**
 * @brief Gets the position of all servos to a table of angles in radians
 * 
 * @param currents 
 * @todo understand difference between threads and simple function (this)
 */
void
RobotHandle::getAllCurrents(double *currents){
    vector<int> current_vec = dx_motors_sys.getAllCurrent();
    for(int i=0; i<numMotors; i++){

        if(ids_arr[i]){
            currents[i]=(current_vec[i]-2048)*9.2115/2048;
            //cout << current_vec[i] << endl;
        }
    }
}



/**
 * @brief 
 * 
 * @param new_angles 
 */
void RobotHandle::setAllPositionsThreadV(vector<double> new_angles){
    dx_write.lock();
        for(int i=0; i<numMotors; i++){
            angles[i] = new_angles[i];
        }
        writeAngles=true;
    dx_write.unlock();
}

/**
 * @brief 
 * 
 * @param new_angles 
 */
void RobotHandle::setAllPositionsThread(double* new_angles){
    dx_write.lock();
        for(int i=0; i<numMotors; i++){
            angles[i] = new_angles[i];
        }
        writeAngles=true;
    dx_write.unlock();
}

/**
 * @brief Get all the feedback motor angles (Positions)
 * 
 * @param angles 
 */
void RobotHandle::getAllPositionsThread(double *angles){
    dx_read.lock();
        for(int i=0; i<numMotors; i++){
            angles[i] = fbck_angles[i];
        }
        readPosition=true;
    dx_read.unlock();
}

/**
 * @brief Get all the feedback motor currents
 * 
 * @param currents 
 */
void RobotHandle::getAllCurrentsThread(double *currents){
    dx_read.lock();
        for(int i=0; i<numMotors; i++){
            currents[i] = fbck_current[i];
        }
        readCurrent=true;
    dx_read.unlock();
}
