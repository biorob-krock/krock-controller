/**
 * @file pybind11_robotHandle.cpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief Pybind11 wrapper for using GlobalConfig and RobotHandle class
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <RobotHandle/robotHandle.hpp>
#include <RobotConfig/global_config.hpp>

namespace py = pybind11;

/**
 * @brief Construct a new pybind11 module object for robothandle 
 * 
 * @details RobotHandle class needs both GlobalConfig and RobotHandle class to be wrapped. 
 * 
 * @note : RobotHandle not all functions are exposed to python 
 * @todo : Expose all the functions to python
 * @todo : Instead of pointers to arrays add functions that can take vectors 
 * 
 */
PYBIND11_MODULE(robothandle, m) {    
    py::class_<GlobalConfig>(m, "GlobalConfig")
        .def(py::init<>())
        .def("readFromFile", &GlobalConfig::readFromFile)
        .def("isSimulation", &GlobalConfig::isSimulation)
        .def("isOptimization", &GlobalConfig::isOptimization)
        .def("useJoystick", &GlobalConfig::useJoystick)
        .def("getJoystickMode", &GlobalConfig::getJoystickMode)
        .def("isAutoRun", &GlobalConfig::isAutoRun)
        .def("getAutoRunState", &GlobalConfig::getAutoRunState)
        .def("highLevelControllerEnabled", &GlobalConfig::highLevelControllerEnabled)
        .def("useTail", &GlobalConfig::useTail)
        .def("useIMU", &GlobalConfig::useIMU)
        .def("useROS", &GlobalConfig::useROS)
        .def("swim", &GlobalConfig::swim)
        .def("spineCompensatedFeedback", &GlobalConfig::spineCompensatedFeedback)
        .def("useReflexes", &GlobalConfig::useReflexes)
        .def("nbMotors", &GlobalConfig::nbMotors)
        .def("nbMotorsTrunk", &GlobalConfig::nbMotorsTrunk)
        .def("nbLegs", &GlobalConfig::nbLegs)
        .def("nbMotorsLegs", &GlobalConfig::nbMotorsLegs)
        .def("nbMotorsTail", &GlobalConfig::nbMotorsTail)
        .def("nbMotorsNeck", &GlobalConfig::nbMotorsNeck)
        .def("getMotorsIDs", &GlobalConfig::getMotorsIDs) // IDs for Dynamixel motors
        .def("getEnabledMotors", &GlobalConfig::getEnabledMotors)
        .def("allMotorsDisabled", &GlobalConfig::allMotorsDisabled)
        .def("maxTorque", &GlobalConfig::maxTorque)
        .def("maxSpeed", &GlobalConfig::maxSpeed)
        .def("logData", &GlobalConfig::logData)
        .def("getLogDescription", &GlobalConfig::getLogDescription)
        .def("statusPrints", &GlobalConfig::statusPrints)
        .def("mattPrints", &GlobalConfig::mattPrints)
        .def("configPrints", &GlobalConfig::configPrints)
        .def("walkPrints", &GlobalConfig::walkPrints)
        .def("swimPrints", &GlobalConfig::swimPrints)
        .def("skipHardware", &GlobalConfig::skipHardware)
        .def("printOverwrite", &GlobalConfig::printOverwrite);
    
    py::class_<RobotHandle>(m, "RobotHandle")
        .def(py::init<GlobalConfig&>())
        .def("readIMU", &RobotHandle::readIMU)
        .def("readForceSensors", &RobotHandle::readForceSensors)
        .def("getAllCurrents", &RobotHandle::getAllCurrents)
        .def("getAllCurrentsThread", &RobotHandle::getAllCurrentsThread)
        .def("setAllPositionsThread", &RobotHandle::setAllPositionsThread)
        .def("setAllPositionsThreadV",&RobotHandle::setAllPositionsThreadV)
        .def("setMaxSpeed", &RobotHandle::setMaxSpeed)
        .def("InitialPosture", &RobotHandle::InitialPosture)
        .def("ServosOff", &RobotHandle::ServosOff);
}