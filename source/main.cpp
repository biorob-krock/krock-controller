/**
 * @file main_webots.cpp
 * @author Legacy
 * @brief Krock robot's main function
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <iostream>
#include <iomanip>
#include <chrono>
#include <sched.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>
#include <array>
#include <ctime>
#include <cstdlib>

#include <LegacyController/controller.hpp>

#include <RobotComm/joystick.h>
#include <RobotComm/robotStatus.hpp>

#include <RobotConfig/global_config.hpp>
#include <Utils/utils.hpp>
#include <Utils/fileReader.hpp>
#include <Utils/timeStamp.hpp>
#include <BoostLogger/boostlogger.hpp>

#include <RobotHandle/robotHandle.hpp>


#define MAX_NUM_SERVOS 30

#define TIME_STEP           10    //ms

#define NC  "\033[0m"
#define RED  "\033[1;31m"
#define GREEN  "\033[1;32m"

using namespace std;
using namespace libconfig;

#define GLOBAL_CONFIG_FNAME "/config/GLOBAL.cfg"
/**
 * @note: immediate
 * @todo Check controller working on the robot 
 */

/**
 * @note: long term todo list
 * @todo RobotSim create a separate package for robot sim 
 * @todo global config parameter => pass by reference 
 * @todo try to not use global config inside the handler or other class 
 *       as this decreases the understanding of the class 
 * @todo pybind11 additions
 * @todo remove misleading function setMaxSpeed etc 
 * @todo read the max torques etc form the config file
 * 
 */

// bool testloader(){
//     std::stringstream configFilePath;
//     configFilePath << PROJECT_DIRECTORY << "/data/input/testNumArrayListRead.txt";
//     std::vector<std::vector<float>> test = FileReader::read2DMatrix(configFilePath.str().c_str(), ',');
//     LOG_INFO << "test complete" <<  std::endl;
//     LOG_INFO << test <<  std::endl;
//     return true;
// }

// int main(int argc, char* argv[]){
//     LOG_INFO << "test complete 1" <<  std::endl;
//     bool out = testloader();
// }

bool loadConfigFile(){
    std::stringstream configFilePath;
    configFilePath << PROJECT_DIRECTORY << GLOBAL_CONFIG_FNAME;

    if (!globalConfig.readFromFile(configFilePath.str()))
    {
        LOG_WARNING << "Unable to read global config. Aborting." << std::endl;
        return false;
    }
    else{
        for(int i=0; i < globalConfig.nbMotors(); i++){
            LOG_INFO << "motor " << i <<": " << globalConfig.getMotorsIDs()[i] 
                << "\t" << globalConfig.getEnabledMotors()[i] << std::endl;
        }
    }
    return true;
}

int main(int argc, char* argv[])
{
    LOG_INFO << GREEN << "STARTING MAIN\n" << NC;
    
    // Add timestep by default
    AddTimeStamp ats_cout(std::cout);
    AddTimeStamp ats_cerr(std::cerr);
    AddTimeStamp ats_clog(std::clog);

    auto krockClockStart = std::chrono::system_clock::now();
    if(!loadConfigFile()) return EXIT_FAILURE;
    
    //========== Initialize variables =================
    double Td=TIME_STEP/1000.;
    double t=0, t_tot=0;
    double angles_out[MAX_NUM_SERVOS], fbck_angles[MAX_NUM_SERVOS], fbck_current[MAX_NUM_SERVOS];
    double t0=get_real_time(), t1=get_real_time(), t2=get_real_time(), dt;
    int numsteps=0;
    // double senddata[155];
    double cnt_time=0;

    // ================= INITIALIZE ROBOT STATUS =====================================
    // RobotStatus rStatus(
    //     (const char*)"rstatus2unity_shm",
    //     (const char*)"sensorsStruct_shm")
    //     globalConfig.skipHardware());
    
    /// @todo Figure out why is it needed here (before initalising anything)
    /// @note WriteToRobotSharedMemory done in robot_handle.initSharedMemory as well
    // rStatus.WriteToRobotSharedMemory(); 
    
    // if(globalConfig.configPrints()){
    //     printf("written to a shm\n");
    // }
    
    Controller controller(Td);
    controller.printConfiguration(); 

    /// @todo Check how ros is being used
    /// @todo add pybind to have ros running with python ?
    if(globalConfig.useROS()){
        cout << "using ROS" << endl;
        controller.startROS(argc, argv, "krock2");
    }
    cout << "1" << endl;

    //========= Initialize robot ==================
    // pass by value used for function return values 
    // Maybe try to use globalConfig class directly ?
    /// @todo max_torque and max_speed using configs
    /// @todo instead of using glabal config intenally in robot handle, pass them as arguments for more visibility
    RobotHandle robot_handle(globalConfig);

    /// @note: moved to robot_handle 
    /// @todo: maybe  shift the robot status to robot handle directory ?
    // bool skip_hardware = globalConfig.skipHardware();
    // robot_handle.rStatus = RobotStatus((const char*)"rstatus2unity_shm", (const char*)"sensorsStruct_shm", skip_hardware);
    // robot_handle.initSharedMemory();

    usleep(3000000);

    double imudata[3], forcedata[12], forcedataRaw[16];

    cout << "2" << endl;

    //#################### LOOP ############################################################
    //######################################################################################
    //robot_handle.getTorqueEnable();
    while(1)
    {
        //=========================== Time flow ===============================
        dt=get_timestamp()-t0;
        t0=get_timestamp();
        t=t+dt;

        // if dt is > 2 ms then dt=1ms else dt 
        dt=dt>(TIME_STEP/1000.*2)?(TIME_STEP/1000.):dt;

        //=========================== Read sensors ===============================
        robot_handle.readIMU(imudata);
        robot_handle.readForceSensors(forcedata, forcedataRaw);

        //======================= Robot feedback =====================================
        robot_handle.getAllPositionsThread(fbck_angles);
        robot_handle.getAllCurrentsThread(fbck_current);        
        //=========================== Controller calls ===============================
        
        controller.setTimeStep(dt);
        controller.updateRobotState(fbck_angles, fbck_current);
        controller.getRPY(imudata);
        controller.getForce(forcedata, forcedataRaw);

        if(!controller.runStep(t)){
            ///@note should not be required as handled by the destructor of robot_handle
            robot_handle.stopThread=true;
            usleep(300000);
            
            cout << "\n" << "----------" << endl; 
            cout << "Leaving Loop" << endl; 
            cout << "----------" << "\n" << endl; 

            // if(globalConfig.logData()){            
            //     cout << "\n" << "Saving output log" << "\n" << endl; 
            //     controller.saveOutputLog();
            // }
            
            ///@note: this is a **important** break statement to quit the loop 
            break;
        }

        // MatrixXd q_tail_extern = controller.get_q_tail();

        controller.getAngles(angles_out);

        /// @todo: ask what is happening here
        // for(int i=0; i<3; i++){
        //     angles_out[18+i] = (double)q_tail_extern(i);
        // }
        
        controller.printControlStatus();

        robot_handle.setAllPositionsThread(angles_out);

        //============================= SEND ROBOT STATUS =========================================
        for(int i=0; i < globalConfig.nbMotors(); i++){
            robot_handle.rStatus.robot_status_struct.motor_positions[i]=controller.joint_angles(i);
            robot_handle.rStatus.robot_status_struct.motor_torques[i]=controller.fbck_torques(i);
        }
        for(int i=0; i<2; i++){
            robot_handle.rStatus.robot_status_struct.imu[i]=controller.forRPY(i);
        }
        // for(int i=0; i<12; i++){
        //     robot_handle.rStatus.robot_status_struct.forces[i]=controller.nnEstForce(i);
        // }
        robot_handle.rStatus.WriteToRobotSharedMemory();

        
        //============================= SEND UDP ======================================================

        // controller.sendStatusToMatlab(10, "128.179.164.226");
        //=========================== Sleep until time step ===============================
        t1=get_timestamp();
        if(Td>(t1-t0)){
            usleep((Td- (t1-t0))*1000000 );
        }
        t2=get_timestamp();

        //=========================== CHECK IF TIME IS OK =====================
        if((t1 - t0)*1000 > 28){
            cnt_time++;
        }
        else{
            cnt_time=0;
        }
        if(cnt_time>50){
            cout<<"SOMETHING IS WRONG!!!!!!!!!!!!!!!!!!!\nCHECK CABLES, SOMETHING IS PROBABLY DISCONNECTED!!!!!!!!"<<endl;
            break;

        }

        numsteps++;
    }

    //#################### LOOP ENDS #######################################################
    //######################################################################################
    /// @todo this is also done when constructing robotHandle, do this once and save it in congifs maybe 
    /// @todo or somehow make these conversions easy? 
    /// @todo possibly its better to put these values in robothandle destructor ?????
    // std::vector<bool> enabledMotors = globalConfig.getEnabledMotors();
    // int enabled_motors[100];
    // for (int i = 0; i < enabledMotors.size(); i++)
    // {
    //     enabled_motors[i] = enabledMotors[i];
    // }
    // robot_handle.setMaxSpeed(100);
    // usleep(100000);
    // robot_handle.InitialPosture(enabled_motors);
    // usleep(2000000);
    // cout << "SHUTING DOWN" << endl;
    // robot_handle.ServosOff(enabled_motors);
    // //controller.mpc_thread.join();
    // usleep(1000000);
    // cout << "SHUTING DOWN i am even printing ?" << endl;
    return 0;
}
