/**
 * @file pybind11_robotComm.cpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief Pybind11 wrapper for using PS3joy class
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <pybind11/pybind11.h>
#include <pybind11/pytypes.h>
#include <pybind11/numpy.h>
#include <RobotComm/ps3joy.hpp>

namespace py = pybind11;

/**
 * @brief Construct a new pybind11 module object for PS3joy class
 * 
 * @details Pybind11 module definition for PS3joy. Since arrays(cpp) are not supported 
 * for implicit conversion between python and cpp, the definition below implements a custom definition.
 * 
 * @todo testing for all the functionality. Some functions may not be working as expected due to
 * type conversions
 * @todo remove undesired code
 * 
 */
PYBIND11_MODULE(ps3joy, m) {

    py::class_<PS3joy::JoyStruct>(m, "JoyStruct", py::buffer_protocol())
        .def(py::init<>())
        .def_property("axes", [](PS3joy::JoyStruct &p) -> py::array {
            auto dtype = py::dtype(py::format_descriptor<float>::format());
            auto base = py::array(dtype, {6}, {sizeof(float)});
            return py::array(
                dtype, {6}, {sizeof(float)}, p.axes, base);
        }, [](PS3joy::JoyStruct& p) {})
        .def_property("buttons", [](PS3joy::JoyStruct &p) -> py::array {
            auto dtype = py::dtype(py::format_descriptor<int>::format());
            auto base = py::array(dtype, {16}, {sizeof(int)});
            return py::array(dtype, {16}, {sizeof(int)}, p.buttons, base);
        }, [](PS3joy::JoyStruct& p) {})

        .def_readwrite("start_pressed", &PS3joy::JoyStruct::start_pressed)
        .def_readwrite("start_pressed_old", &PS3joy::JoyStruct::start_pressed_old)
        .def_readwrite("select_pressed", &PS3joy::JoyStruct::select_pressed)
        .def_readwrite("select_pressed_old", &PS3joy::JoyStruct::select_pressed_old)
        .def_readwrite("LR_pressed", &PS3joy::JoyStruct::LR_pressed);

    py::class_<PS3joy>(m, "PS3joy")
        .def_readwrite("joyStruct", &PS3joy::joyStruct)
        .def(py::init<const char *>())
        .def("IsReady", &PS3joy::IsReady)
        .def("GetInput", &PS3joy::GetInput)
        .def("PrintInput", &PS3joy::PrintInput)
        .def("SetInput", &PS3joy::SetInput)
        .def("OpenDevice", &PS3joy::OpenDevice)
        // .def("OpenSharedMemory", &PS3joy::OpenSharedMemory)
        // .def("CreateSharedMemory", &PS3joy::CreateSharedMemory)
        .def("WriteSharedMemory", &PS3joy::WriteSharedMemory)
        .def("ReadSharedMemory", &PS3joy::ReadSharedMemory);

}