
/**
 * @file robotStatus.cpp
 * @author Astha Gupta (astha736@gmail.com)
 * @brief class definitions for RobotStatus
 * @version 0.1
 * @date 2021-02-08
 * 
 * 
 */
#include <boost/interprocess/managed_shared_memory.hpp>
#include <RobotComm/robotStatus.hpp>


RobotStatus::RobotStatus(const char *robotStatusShmName, const char *sensorStatusShmName, bool skipHardware){
    robot_status_struct.motorsOn = false;
    skip_hardware = skipHardware;

    robot_shm_manager  = managed_shared_memory(open_or_create, robotStatusShmName, 1024);
    robot_status_struct_shm = robot_shm_manager.find_or_construct<RobotStatusStruct>("RobotStatusStruct")();
    
    if(!skip_hardware){
        sensor_shm_manager = managed_shared_memory(open_or_create, sensorStatusShmName, 1024);
        sensor_status_struct_shm = sensor_shm_manager.find_or_construct<SensorStatusStruct>("SensorStatusStruct")();
    }

}

RobotStatus::RobotStatus(){
    
}

void RobotStatus::WriteToRobotSharedMemory(){
    memcpy(robot_status_struct_shm, &robot_status_struct, sizeof(RobotStatusStruct));
}

void RobotStatus::ReadFromRobotSharedMemory(){
    memcpy(&robot_status_struct, robot_status_struct_shm, sizeof(RobotStatusStruct));
}

void RobotStatus::WriteToSensorSharedMemory(){
    if(!skip_hardware){
        memcpy(sensor_status_struct_shm, &sensor_status_struct, sizeof(SensorStatusStruct));
    }
}
void RobotStatus::ReadFromSensorSharedMemory(){
    if(!skip_hardware){
        memcpy(&sensor_status_struct, sensor_status_struct_shm, sizeof(SensorStatusStruct));
    }
}
