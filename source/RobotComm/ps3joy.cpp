/**
 * @file ps3joy.cpp
 * @author Legacy
 * @brief PS3joy class definitions to manage input from dualshock controller
 * @version 0.1
 * @date 2021-02-08
 * 
 * @todo doxygen documentation 
 * 
 */
#include <RobotComm/ps3joy.hpp>


/* PS3joy constructor*/
PS3joy :: PS3joy(const char *sharedMemoryName) : shm(open_or_create, sharedMemoryName, 1024)
{
  for(int i=0; i<16; i++){
    joyStruct.buttons[i]=0;
  }
  for(int i=0; i<6; i++){
    joyStruct.axes[i]=0;
  }

  joyStruct.start_pressed=false;
  joyStruct.start_pressed_old=false;
  joyStruct.select_pressed=false;
  joyStruct.select_pressed_old=false;
  joyStruct.LR_pressed=false;

  joyStruct_shm = shm.find_or_construct<PS3joy::JoyStruct>("JoyStruct")();
}


/* Opens a joystick device */
bool PS3joy :: OpenDevice(char * device) 
{
  return js.load(device);
}

/* Check if joystick is ready */
bool PS3joy :: IsReady()
{
  return js.is_ready();
}

/* Check if joystick is ready */
// PS3joy::JoyStruct PS3joy :: GetInput()
// JoyStruct PS3joy :: GetInput()
/// @note: GetInput() converted to void for pybind11 
void PS3joy :: GetInput()
{
  js.update();
  // buttons
  for(int i=0; i<16; i++){
    joyStruct.buttons[i] =js.buttons[i];        
  }

  //axes
  for(int i=0; i<6; i++){
    joyStruct.axes[i]=(float)(js.axes[i]);
  }

  // special buttons
  joyStruct.start_pressed_old=joyStruct.start_pressed;
  joyStruct.select_pressed_old=joyStruct.select_pressed;

  //PS4
  joyStruct.start_pressed=(bool)joyStruct.buttons[9];
  joyStruct.select_pressed=(bool)joyStruct.buttons[8];
  joyStruct.LR_pressed=joyStruct.buttons[4]&&joyStruct.buttons[5];
    // joy_l1=joystick.joyStruct.buttons[4];
    //  joy_r1=joystick.joyStruct.buttons[5];

  return; 
  // return joyStruct;
}

/* Prints all the buttons and axes */
void PS3joy :: PrintInput()
{

  for(int i=0; i<6; i++){
    printf("A%d: %f \t", i , joyStruct.axes[i]);
  }
  cout << endl;
  for(int i=0; i<16; i++){
    printf("B%d: %d \t", i , joyStruct.buttons[i]);
  }
  printf("\n\n");
}

/* Manually sets the input */
void PS3joy :: SetInput(PS3joy::JoyStruct joyData)
{
  joyStruct=joyData;
}

/* Write to a shared memory */
void PS3joy :: WriteSharedMemory()
{
  memcpy(joyStruct_shm, &joyStruct, sizeof(PS3joy::JoyStruct));
}

/* Write to a shared memory */
void PS3joy :: ReadSharedMemory()
{
  memcpy(&joyStruct, joyStruct_shm, sizeof(PS3joy::JoyStruct));
}
