#!/usr/bin/env python3
"""[PS3 controller button test code]
"""
import os
import sys
import time
import curses

import threading
from collections import namedtuple

"""@note: Appending the project krock-controller's source dir to sys path 
    - Assuming all the pybind object files are kept in the source folder 
    - This is not the ideal way of doing things. 
    - It might be beneficial to store the object under a proper folder 
    - Maybe try installing this the object files in site-packages 
        - Installing virtual env will not work 
    
"""
import ps3joy as ps3joy

JOYSTICK_SHARED_MEMORY = "ps3shm"
JOYSTICK_DEVNAME = "/dev/input/js0"

def ps3joy_thread(status_dict):
    """[ps3joy_thread: reading commands from ps3joy in shared memory]

    Args:
        status_dict ([dict]): ['stopJoystickThread': False, 'joystick_plugged': False, 'forward': 0.0, 'turning': 0.0]
    
    @notes: dictionary and list are mutable objects that can be shared between threads. Hence using a dictionary to avoid 
    using global variables for thread management.
    """
    ps3joy_obj = ps3joy.PS3joy(JOYSTICK_SHARED_MEMORY)
    while not status_dict['stopJoystickThread']:
        try:
            if not status_dict['joystick_plugged']:
                status_dict['joystick_plugged ']= ps3joy_obj.OpenDevice(JOYSTICK_DEVNAME)
            else:
                ps3joy_obj.GetInput()
                ps3joy_obj.WriteSharedMemory()
                status_dict['forward'] = -ps3joy_obj.joyStruct.axes[4]
                status_dict['turning'] = ps3joy_obj.joyStruct.axes[0]
                if status_dict['forward']  < 0.0:
                    status_dict['forward']  = 0.0
            time.sleep(0.5)
        except Exception as e:
            print(e)
            break 
    return 


def main():
    # dictionary as shared object 
    status_dict = {'stopJoystickThread': False, 'joystick_plugged': False, 'forward': 0.0, 'turning': 0.0}
    x = threading.Thread(target=ps3joy_thread,  args=(status_dict, ))
    x.start()
    i = 0

    while True:
        try:
            i = i+1
            print("drive_forward: " + str(status_dict['forward']))
            print("drive_turning: " + str(status_dict['turning']))
            print(status_dict['joystick_plugged'])
        except Exception as e:
            print(e)
            break 
        except KeyboardInterrupt:
            # just for debugging purpose for now 
            status_dict['stopJoystickThread'] = True
            x.join()
            return 

    status_dict['stopJoystickThread'] = True
    x.join()

    return
    



if __name__ == '__main__':
    TIC = time.time()
    main()