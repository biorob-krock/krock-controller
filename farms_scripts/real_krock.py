
#!/usr/bin/env python3
"""[Run CPG controller on krock]

with robothandle and joystick
    - currently joystick is only used to exit the loop 

Future Work: 
    - implement joystick to vary the drive 
    - maybe joy(0,1) -> d(1,3) variation
    - need to also implement a standing mode 
    - allow trasition from walking to standing mode

"""

import os
import time
import sys
import farms_pylog as pylog
import numpy as np
import threading

from farms_models.utils import get_sdf_path
from farms_amphibious.experiment.options import (
    # get_krock_options,
    get_krock_kwargs_options,
    get_animat_options,
    set_no_swimming_options,
    get_simulation_options,

)

from farms_amphibious.model.animat import Amphibious
from farms_amphibious.control.controller import AmphibiousController
from farms_data.amphibious.data import AmphibiousData

sys.path.append('/home/biorob/farms-integration/integration-scripts')
from extra_farms.krock_options import (
    get_krock_options,
    amphibious_options,
    get_krock_joint_names_ordered,
)
sys.path.append("..") 
from extra_krock.ps3joy_cmd import ps3joy_thread

from extra_farms.krock_utils import get_data_krock

import robothandle
import ps3joy


GLOBAL_CONFIG_FNAME="config/GLOBAL.cfg"
KROCK_CONTROLLER_DIR=os.path.dirname(os.path.abspath(__file__)).split('/farms_scripts')[0]
SDF_PATH = '/home/biorob/farms-integration/krock-sdf-model/krock.sdf'
JOYSTICK_SHARED_MEMORY="ps3shm"

def run_krock_cpg(timestep, n_iterations, motior_activate, slow_down):

    global GLOBAL_CONFIG_FNAME
    global SDF_PATH
    
    # load robot config files 
    config_path = os.path.join(KROCK_CONTROLLER_DIR, GLOBAL_CONFIG_FNAME)
    assert os.path.isfile(config_path)
    krock_global_config = robothandle.GlobalConfig()
    config_real_success = krock_global_config.readFromFile(config_path)
    
    # make result dir
    # currentDirPath  = os.path.dirname(os.path.abspath(__file__))
    # log_path = os.path.join(
    #     currentDirPath,
    #     'krock_results',
    #     )
    # if log_path and not os.path.isdir(log_path):
    #     os.makedirs(log_path)

    # Options
    assert os.path.isfile(SDF_PATH)
    sdf, animat_options = get_krock_options(sdf_path=SDF_PATH)
    (
        simulation_options,
        arena,
    ) = amphibious_options(animat_options, use_water_arena=False)
    simulation_options.timestep = timestep
    simulation_options.n_iterations = n_iterations


    # Animat data
    animat_data = AmphibiousData.from_options(
        animat_options.control,
        simulation_options.n_iterations,
        simulation_options.timestep,
    )

    # set the controller type in animat_options
    animat_controller = AmphibiousController(
        joints=animat_options.morphology.joints_names(),
        animat_options=animat_options,
        animat_data=animat_data,
    )


    try:
        # Important: These two should always be together 
        robot_handle = robothandle.RobotHandle(krock_global_config)
        robot_handle.setMaxSpeed(100)
        animat_controller.network.data.network.drives.array[:, 0] = 1
        krock_joint_names_ordered = get_krock_joint_names_ordered()
        ps3joy_obj = ps3joy.PS3joy(JOYSTICK_SHARED_MEMORY)
        

        ls = time.time()
        for iteration in range(simulation_options.n_iterations -1):
            # Add code here to change drive from ps3joy_obj input 
            # animat_controller.network.data.network.drives.array[iteration, 0] = forward
            # animat_controller.network.data.network.drives.array[iteration, 1] = turning  
            start_time = time.time()
            ps3joy_obj.ReadSharedMemory()

            if ps3joy_obj.joyStruct.start_pressed and not ps3joy_obj.joyStruct.start_pressed_old:
                print("Start button pressed, leaving the loop .........")
                break
            animat_controller.step(
                iteration=iteration,
                time=iteration*simulation_options.timestep,
                timestep=simulation_options.timestep
            )

            pos = animat_controller.positions(
                iteration=iteration,
                time=iteration*simulation_options.timestep,
                timestep=simulation_options.timestep
            )
            # very important to correctly format the data
            pos_update = get_data_krock(
                pybullet_data=pos,
                krock_joint_names_ordered=krock_joint_names_ordered)
            pos_update = np.multiply(pos_update, motior_activate)
            #print(pos_update)
            robot_handle.setAllPositionsThreadV(pos_update)
            end_time = time.time()
            dt_time = end_time - start_time
            step_time = slow_down*simulation_options.timestep
            if dt_time < step_time:
                time.sleep(step_time - dt_time)

        lapse = time.time() - ls
    except KeyboardInterrupt:
        return
    
    print("Time: ", lapse)
    print("Iterations: ", simulation_options.n_iterations)
    print("Command frequency: ", float(simulation_options.n_iterations)/lapse)
    print('Switching off the robot..')
    return 
    

def main():
    activate = np.ones(21, dtype=float)
    #activate[0:4] = 1
    run_krock_cpg(
        timestep=0.00100,
        n_iterations=1000*10,
        motior_activate=activate, 
        slow_down=1)

if __name__ == '__main__':
    TIC = time.time()
    main()
    pylog.info('Total simulation time: {} [s]'.format(time.time() - TIC))