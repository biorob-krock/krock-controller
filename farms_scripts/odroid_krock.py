#!/usr/bin/env python3
"""[CPG controller script for odroid]

without robothadle or ps3joy 
usecase 
    - check generated cpg commands and save them (can be used py tests/robotHandle_pybind11_test.py)
    - time the performance of cpg commands
"""

import os
import time
import sys
import farms_pylog as pylog
import numpy as np
import threading

from farms_models.utils import get_sdf_path
from farms_amphibious.experiment.options import (
    # get_krock_options,
    get_krock_kwargs_options,
    get_animat_options,
    set_no_swimming_options,
    get_simulation_options,

)

from farms_amphibious.model.animat import Amphibious
from farms_amphibious.control.controller import AmphibiousController
from farms_data.amphibious.data import AmphibiousData

sys.path.append('/home/biorob/farms-integration/integration-scripts')
from extra_farms.krock_options import (
    get_krock_options,
    amphibious_options,
    get_krock_joint_names_ordered,
)
sys.path.append("..") 
from extra_krock.ps3joy_cmd import ps3joy_thread
from extra_farms.krock_utils import get_data_krock



GLOBAL_CONFIG_FNAME="config/GLOBAL.cfg"
KROCK_CONTROLLER_DIR=os.path.dirname(os.path.abspath(__file__)).split('/farms_scripts')[0]
SDF_PATH = '/home/biorob/farms-integration/krock-sdf-model/krock.sdf'
#JOYSTICK_SHARED_MEMORY="ps3shm"

def main():

    global GLOBAL_CONFIG_FNAME
    global SDF_PATH
    krock_global_config = robothandle.GlobalConfig()
    config_path = os.path.join(KROCK_CONTROLLER_DIR, GLOBAL_CONFIG_FNAME)
    config_real_success = krock_global_config.readFromFile(config_path)
    assert os.path.isfile(config_path)
    
    currentDirPath  = os.path.dirname(os.path.abspath(__file__))

    # log_path = os.path.join(
    #     currentDirPath,
    #     'krock_results',
    #     )
    # if log_path and not os.path.isdir(log_path):
    #     os.makedirs(log_path)

    assert os.path.isfile(SDF_PATH)
    # Options
    sdf, animat_options = get_krock_options(sdf_path=SDF_PATH)
    (
        simulation_options,
        arena,
    ) = amphibious_options(animat_options, use_water_arena=False)
    simulation_options.timestep = 0.01
    simulation_options.n_iterations = 100*10


    # Animat data
    animat_data = AmphibiousData.from_options(
        animat_options.control,
        simulation_options.n_iterations,
        simulation_options.timestep,
    )

    # set the controller type in animat_options
    animat_controller = AmphibiousController(
        joints=animat_options.morphology.joints_names(),
        animat_options=animat_options,
        animat_data=animat_data,
    )

    krock_joint_names_ordered = get_krock_joint_names_ordered()
    # use ps3joy_obj when actively controlling the drive
    #ps3joy_obj = ps3joy.PS3joy(JOYSTICK_SHARED_MEMORY)

    # activate: add only 0.0 or 1.0., gets multiplied with the commands before providing
    activate = np.zeros(21, dtype=float)
    activate[16:] = 1

    animat_controller.network.data.network.drives.array[:, 0] = 1

    for iteration in range(simulation_options.n_iterations -1):
        # Add code here to change drive from ps3joy_obj input 
        # animat_controller.network.data.network.drives.array[iteration, 0] = forward
        # animat_controller.network.data.network.drives.array[iteration, 1] = turning  
        animat_controller.step(
            iteration=iteration,
            time=iteration*simulation_options.timestep,
            timestep=simulation_options.timestep
        )

        pos = animat_controller.positions(
            iteration=iteration,
            time=iteration*simulation_options.timestep,
            timestep=simulation_options.timestep,
        )
        # very important to correctly format the data
        pos_update = get_data_krock(
            pybullet_data=pos,
            krock_joint_names_ordered=krock_joint_names_ordered,
        )
        # multiply activation of joint commands
        pos_update = np.multiply(pos_update, activate)
        out.append(pos_update)
        print(pos_update)
    
    out = np.array(out)
    np.savetxt(fname='cpg_out',X=out)
    print(out.shape)

    # pr.disable()
    # s = io.StringIO()
    # sortby = SortKey.CUMULATIVE
    # ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    # ps.print_stats()
    # print(s.getvalue())


if __name__ == '__main__':
    TIC = time.time()
    import cProfile, pstats, io
    from pstats import SortKey
    # pr = cProfile.Profile()
    # pr.enable()
    main()
    # pr.disable()
    # s = io.StringIO()
    # sortby = SortKey.CUMULATIVE
    # ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    # ps.print_stats()
    # print(s.getvalue())
    pylog.info('Total simulation time: {} [s]'.format(time.time() - TIC))

