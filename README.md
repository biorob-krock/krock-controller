# Krock Controller
Krock2 Controller from the [BioRobotics Lab](https://biorob.epfl.ch/) at EPFL. Initial development by Tomislav Horvat, now maintained by [Astha Gupta](https://people.epfl.ch/astha.gupta?lang=en)

Contributors: 
- [Tomislav Horvat](https://www.epfl.ch/labs/biorob/people/past-members/horvat/)
- [Astha Gupta](https://people.epfl.ch/astha.gupta?lang=en)
- [Mathew Estrada](https://www.epfl.ch/labs/biorob/people/past-members/estrada/)
- [Laura Páez](https://www.epfl.ch/labs/biorob/people/paez/) 
- [Kamilo Melo](https://www.epfl.ch/labs/biorob/people/melo/)

Student contributors
- Thomas Havy
- Adrien Chassignet
- Mohammadreza Ebrahimi

Installation/CMake compilation, Pybind wrappings, and 2021 Legacy controller refactoring by Astha Gupta.
Original development of legacy controller by Tomislav Horvat.

Verified code that runs on the robot is in `master` branch while continued development is in the `develop` branch, also cloned into the robot.
Instructions for installation/compilation of the code are below. More documetnation is in the [doc/](doc/) folder of the repo.

Use `doxygen doxygenconfig`to generate documentation. Use `google-chrome doxygen_html/index.html `to open the documentation with google-chrome or open index.html with any other browser.  

# Controller Codebase

The codebase contains two controllers able to run onboard hardware: 

- "Legacy" C++ controller based on parameterized foot trajectories in task
  space.
- FARMS CPG controller written in python to run central pattern generators
  on the robot.

Interaction with motors and sensors are handled by a common `RobotHandle` class. 


## Dependencies

Dependencies are a mix of system installations along with several codebases located under the `lib/` folder to avoid incompatibilities with updates.
Instructions for installation and compilation are described in the next section.

### Third party libraries

Install these onto your system prior to compiling the controller code.

- [dlib 19.0](http://dlib.net/) - C++ toolkit containing machine learning algorithms
- [eigen3](http://eigen.tuxfamily.org/) - C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms
- [CasADi](https://web.casadi.org/) -  free and open source symbolic framework for automatic differentiation and optimal control
- [qpOASES 3.2.0](https://projects.coin-or.org/qpOASES) - a parametric active-set algorithm for quadratic programming
- [ROS Noetic](http://wiki.ros.org/noetic) - for high-level communication with a master node on an offboard PC
- [pybind11](https://pybind11.readthedocs.io/en/stable/index.html) - to compile python wrappers to use krock with FARMS/Pybullet/Python code   

> Note: eigen3.2 is used in this project because later versions causes seg fault on odroid. Currently, the seg faults have been fixed by using `set(EIGEN_MAX_ALIGN_BYTES 16)`flag in CMakeLists.txt and using the eigen library cloned inside the lib folder (eigen 3.2.0). 
### Custom libraries

- Several libraries written for our specific hardware and located in [lib](lib/) folder.
	- DXLite-master - modified version of [DXLite](https://github.com/KM-RoBoTa/DXLite) package for controlling Dynamixel motors 
	- vectornav - reading VectorNav IMU


## Install

- `git submodule update --init --remote` to get the pybind11 code : get the stable, tag: v2.6.1


## Compiling

- In the main folder script called `install_controller.sh` has been provided. This helps user to 
	- Install of all the dependencies
	- Build the project each time cpp code is changed (files in `build/`)

Run the script with source command:

```bash
# On pc
# When compiling for webots (legacy controller)
./install_controller.sh --FOR_WEBOTS=OFF --WITH_ROS=ON --JOB_THREADS=8 --VAR_USER --INSTALL_ROS --INSTALL_WEBOTS --ASSUME_YES --DEBUG_MODE=ON
# When compiling for krock (legacy controller)
./install_controller.sh --FOR_WEBOTS=ON --WITH_ROS=ON --JOB_THREADS=8 --VAR_USER --INSTALL_ROS --INSTALL_WEBOTS --ASSUME_YES --DEBUG_MODE=ON

# When compiling for krock on odroid
# --JOB_THREADS=2 , odroid has less cores 
# --DEBUG_MODE=OFF as it takes a lot of time 
./install_controller.sh --FOR_WEBOTS=ON --WITH_ROS=ON --JOB_THREADS=2 --VAR_USER --INSTALL_ROS --INSTALL_WEBOTS --ASSUME_YES --DEBUG_MODE=OFF

## Docker 
# Make sure to run with --ASSUME_YES in docker build configurations. Otherwise this would result in an error
# When compiling for webots (legacy controller)
./install_controller.sh --FOR_WEBOTS=OFF --WITH_ROS=ON --JOB_THREADS=8 --INSTALL_ROS --INSTALL_WEBOTS --ASSUME_YES --DEBUG_MODE=ON
# When compiling for krock (legacy controller)
./install_controller.sh --FOR_WEBOTS=ON --WITH_ROS=ON --JOB_THREADS=8 --INSTALL_ROS --INSTALL_WEBOTS --ASSUME_YES --DEBUG_MODE=ON
```
3. Make sure to check that the following lines can be found in the 
	bashrc file after the script is successful 
	1. source /opt/ros/noetic/setup.bash
	2. export LD_LIBRARY_PATH=.../casadi/..:../vectornav/..
	3. export PYTHONPATH=../krock-controller/build

> **Note**:  `--INSTALL_ROS --INSTALL_WEBOTS` tags can be omitted with ros or webots are not installed. Although adding these options will not affect anything if ros and webots are already installed 
> **Note**:  `--VAR_USER` runs the installation scripts with `sudo` user rights. Remove this tag when building docker files
> **Note**:  `--ASSUME_YES` will assume yes to all package installation mentioned in the install_scripts 
> **Note**:  `--DEBUG_MODE` switching on debug mode builds executables such that they can be run with the gnu debugger (gdb command line)

> Note: When the robot is started, it follows the following [instructions](https://gitlab.com/biorob-krock/krock-controller/-/blob/astha-refactor/doc/startup.md). If things are not working as expected. Please check that the that `/usr/local/bin/robot_startup.sh` is available and correct path to comm_slave is provided in it. 

- Recompile: to recompile the code simply run the install_controller.sh script again 


## Understanding the compilation process 
- **install_controller.sh**:  Compilation starting point 
	- Looks for required libraries 
	- Install webots and ros if asked 
	- initiates `cmake` & `make` commands 
	- exports the required paths to `.bashrc`
- **CMakeLists.txt**: Main configuration file for compiling 
	- Set of rules for making makefiles 
	- rules for different executables 
		- robot_main: Legacy controller in CPP for the robot  
		- krock-controller: Legacy controller in CPP for webot robot 
		- comm_slave_cyan: Required by startup services and scripts 
		- [pybind11] ps3joy : Used by farms_scripts files, Python
		- [pybind11] robothandle : Used by farms_scripts files, Python
	- the CPP based executable are then copied to main folder for visibility

## FARMS install for Python CPG controller

Python-based CPG controllers require the Biorob FARMS framework installed.

- Scripts in farms_scripts folder require farms installation. Ask matt for the code on farms installation.
- Krock-controller repository can be build and run independently of farms 
- Only while running the farms dependent python scripts requires FARMS install 
- FARMS can be installed in a separate folder, the only condition is that python3 interpreter should have access to the FARMS libraries 
- To install farms use [farms_integration_setup_no_krock.sh](https://gitlab.com/farms-integration/farms-integration/-/blob/master/farms_integration_setup_no_krock.sh)
```bash
# comment these lines if don't want a virtual env
python3 -m venv venv_farms
source venv_farms/bin/activate
```
> **Note**: Do not use a virtual env if ros is required with farms_scripts (future)

## Generate Documentation with Doxygen 

Documentation for the codebase is generated into a `doxygen_html` folder. Make sure doxygen is [installed](http://www.doxygen.nl/manual/install.html) onto your system. To generate, run the command on the configuration file from the `krock-controller` folder: 

```bash 
doxygen doxygenconfig 
```

Commenting previous code is still underway but this gives an overview of the structure. 

View the documentation by openning `index.html` file under `doxygen_html` or by running the following command `google-chrome doxygen_html/index.html` (`firefox doxygen_html/index.html`). 

# Hardware & Running Controller

## Hardware Resources onboard Krock 
- Two [Odroid XU4](https://www.hardkernel.com/shop/odroid-xu4-special-price/) single board computers in communication with one another. 
	- Cyan (Active)
	- Magenta (Not in use) 
- 17 [Dynamixel MX-64](http://www.robotis.us/dynamixel-mx-64ar/) motors, along with 4 [MX-106](http://emanual.robotis.com/docs/en/dxl/mx/mx-106/)
-[Vecnav VN100](https://www.vectornav.com/products/vn-100) IMU
- [mvBlueFOX](https://www.matrix-vision.com/USB2.0-single-board-camera-with-housing-mvbluefox-igc.html) camera
-[Seek Compact](https://www.thermal.com/compact-series.html) thermal camera
- Optoforce three axis force sensors (now discontinued)

### External components required 
- Host computer with ros installed 
	- to run roscore on this host computer and export the ROS_MASTER_URI on odroid (before running robot_main)
- [Dualshock controller](./doc/joystick.md): used for enabling ttl switch (which allows powering on motors) and controlling the robot

#### Networking 

Networking with ROS, while on campus, is done via EPFL dynamic dyn addresses. Each device is given one with its MAC address. Defined in the `.bashrc` of each machine: 

> **Note**: Need to check what has been exported for ununtu20 
```bash
# Biorob Laptop 
export PC_BIOROB_IP='biorobpc25.epfl.ch'
# Biorob Laptop 
export LAPTOP_BIOROB_IP='M4074E014F0E6.dyn.epfl.ch'
# Cyan 
export CYAN_IP='M7cdd90e03d59.dyn.epfl.ch'
# Magenta 
export MAGENTA_IP='M7cdd90adb51f.dyn.epfl.ch'
```

### Logging into to ODROID

Connect to the ODROID computer through ssh over the EPFL wifi. When the robot is turned on, it automatically posts its IP to the Biorob [dynip page](https://biorob2.epfl.ch/pages/internal/dynip.php). 

`ssh biorob@<IP>`

A [bash script](https://gitlab.com/biorob-krock/utils/-/blob/master/ssh/krock_ssh.sh) is convenient to streamline logging in. 

### Run the Legacy controller 
- check if `comm_slave_cyan` is automatically running (simply use `top` command)
- check the status of joystick and ttlswitch. Using the [watch](TESTCMD.md) commands. 
	- `watch -c tail -n 15 path/to/krock-controller/log/comm_slave/<latest_log_cout_logs>`
	- **Note**: Please delete the old log files, as these new log files are generated at each login 
- Next the ROS_MASTER_URI from host machine should be exported. Otherwise start roscore on odroid in the background (`roscore &`)
	- use background roscore only when data is not being recorded 
	- otherwise rosbag might not fit on the odroid
- The controller is located in `/home/biorob/krock-controller` as a `robot_main` executable. To start the controller, simply call: 
	./robot_main

## Run the CPG controller 
- check if `comm_slave_cyan` is automatically running (simply use `top` command)
- check the status of joystick and ttlswitch. Using the [watch](TESTCMD.md) commands
	- `watch -c tail -n 15 path/to/krock-controller/log/comm_slave/<latest_log_cout_logs>`
	- **Note**: Please delete the old log files, as these new log files are generated at each login 
- Activate the correct python virtual env if necessary (venv_farms using `source <path/to>/venv_farms/bin/activate`)
- `python3 farms_scripts/real_krock.py`


## Debugging 
- Complains about casadi, vectornav, or pybind executables 
	- make sure the environment variables are added to the .bashrc or .zshrc file 
	- source the .bashrc file if the exported commands are not visible in the terminal 
- Remove the build folder and run the install script again in case some python interpreter was previously active 
	- cmake will complain about ros-noetic or python3-empy 
