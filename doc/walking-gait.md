# Krock's walking gait

Krock's legged locomotion is implemented by commanding leg trajectories defined in the robot's reference frame. These trajectories are defined in the operational space with an optimization to then perform the inverse kinematics for the joint angles.  

The code is obfuscated and spread out across multiple files but several variables can be modified to easily change the gait. No checks are performed to ensure that the specified trajectories will result in meaningful joint angles, or that collision will not occur. 

## Modifying gait geometry

The configuration file in `config/ikinController.config` defines the variables encoding the gait geometry. The default gait, loaded upon booting on hardware or Webots, is in the `=== walking related HIGH ===` section of the file. Meaningful variables are: 

- `swing_height`: How high robot is stepping in swing phase 
- `swing_width`: increased width of each swing phase
- `elipse_a`: stride length
- `elipse_b`: no noticeable effect (do we use this value in code?)
- `midStance` : Midpoint position of the gait (defined with respect to the robot's mid torso?). Two rows for front/rear definitions. Three numbers in each row define the midstance of the foot's location in the longitudinal, traverse, vertical axes, respectively. Adding [0 +1 -1] to these values would make the robot's gait wider, and higher. Adding [+1 0 0] would bring the front feet further forward, and rear feet further back. 

### "Gait Mixing"

These values are stored within a `gaitParams` object defined in `structDefs.hpp`. This is loaded in two instances, for `WP` (named for "walking parameters") and `WPlow`. 

A second `gaitParams` object is defined in the config file, allowing the use to scale between two sets of values. If a "lower" gait is defined as the second set of values, holding down `L2` on the controller lowers the gait while holding down `R2` raises it back up. Source file `controller_joystick.pp` encodes this: 


``` cpp

        inputGaitMixing = inputGaitMixing + (joy_l2 - joy_r2)*dt/2.;
        inputGaitMixing = (inputGaitMixing < 0) ? 0 : inputGaitMixing;
        inputGaitMixing = (inputGaitMixing > 1) ? 1 : inputGaitMixing;
```

These two parameters are combined, using variable `inputGaitMixing`, (domain of [0, 1]) is initialized to 0 in `controller.cpp`. 


## Implementation 

This is read in by the `readGaitParameters()` method called by the `Controller::getParameters_iKinController()` method, both defined in `controller_getParameters.cpp`. 