# Pybind11 & Python Development

## Next steps for Pybind11

**Warning**: pybind does not support wrapping of cpp array based attributed or function parameters. There may not be any arrays during compilation of pybind wrappers, but during runtime, when wrappers functionality are called and tested from python3, they result in errors. 

Thus keeping in mind the limitations of pybind11 and the state of the wrappers for controlling krock robot, the next steps for pybind11 include 


- Refactoring or Developing functions to use vectors instead of arrays in RobotHandle, GlobalConfig and PS3Joy 
    - `setAllPositionsThread()` when called in python raises a runtime error
    - As a quick fix `setAllPositionsThreadV()` was defined in RobotHandle(CPP) class 
    - It was then added to pybind11_robotHandle.cpp with `.def("setAllPositionsThreadV",&RobotHandle::setAllPositionsThreadV)`
    - and in the final python code `farms_scripts/real_krock.py`, `setAllPositionsThreadV()` function was called to correctly set the joint angles 
    - Similarly `JoyStruct` attributes like `axes` required explicit definition on how to wrapper axes arrays (Snippet below)
    - Therefore, any use of arrays in python, either as class attributes or function parameters would be required to explicit definitions 
    - Thus, a easier way out is to use vectors, which have implicit conversion definitions in pybind11  
    - **Note** : overloading of cpp functions and then expositing the function with pynind11 gives compilation errors. It would be more useful to either refactor the codes to use vectors or write addition function that take vectors and internally call other functions
```cpp
    .def_property("axes", [](PS3joy::JoyStruct &p) -> py::array {
        auto dtype = py::dtype(py::format_descriptor<float>::format());
        auto base = py::array(dtype, {6}, {sizeof(float)});
        return py::array(
            dtype, {6}, {sizeof(float)}, p.axes, base);
    }, [](PS3joy::JoyStruct& p) {})
``` 

- **Not all the class attributes or function parameters needs to be exposed to python**
    - expose only functions required by python code
    - encapsulate or do not expose sensitive code that should not be changed 
- **Not all the functions currently exposed with pybind11 works well**
    - especially wrappers of the funtions that take array as input 
    - remove these codes, and replace them with functions with vectors 

- Maybe separate GlobalConfig and RobotHandle in different pybind11 shared object files 
    - by treating them separate pybind11 modules 
- Adding wrappers for other components, such as legacy controller 
    - wrap the controller class and only its top level functions to enable calling from python 
- Adding/Removing extra wrapper for functions for RobotHandle, GlobalConfig and PS3Joy 
        - As this was more of a proof of concept, the current pybind wrapper have limited capability 
        - This needs to be extended depending on requirements from future projects 

## Instructions for implementing Legacy controller for FARMS/Pybullet 

Implementing Legacy controller in FARMS or pybullet in general requires two things:
1. Pybind11 wrappers for Legacy controller:
    - This requires expositing Controller class and its definitions to python with help of pybind11. 
    - Expose the high level functions such as `runStep` and other functions called in `source/main.cpp` by writing definition in the pybind module 
    - It is recommended to have a separate file called  `source/LegacyController/pybind11_legacyController.cpp` where all the required functionality are mentioned
        - For instance check `krock-controller/source/RobotComm/pybind11_robotComm.cpp` to see how to write the wrappers 
    - Next edit the highest level `krock-controller/CMakeLists.txt` file to create an compiled library file for `pybind11_legacyController.cpp` 
        - Check how Compiled library file (`.so`) for `robothandle`&  `ps3joy` are created with `krock-controller/CMakeLists.txt`

2. Implementing a custom controller class in FARMS
    - In FARMS the simulations are updated by calling functions of the controller class (and its derived classes) 
    - Thus, in order to FARMS as a interface for pybullet simulation, a derived controller class needs to be implemented, lets call it `KrockLegacyController`
    - The derived class, `KrockLegacyController`, should have a function such as `position()` which spits out the joint positions for a given iteration 
    - Thus this class should initialize a LegacyController and its `position()` function should internally call LegacyController's 
    `controller.getAngles(table_p)` function and return the computed angle (by the controller) 
    - This code will be very similar to FRAMS's `KinematicsController`. 
    - Instead of reading a list of joint commands in case of  `KinematicsController` the   `KrockLegacyController` generates the required joint angles command on the fly 
    - Also ensure that the angles given by `controller.getAngles(table_p)` are multiplied with the right sign convention. 
    - These sign conventions can be found in [integration-data/rosbag/data_manu.py](https://gitlab.com/farms-integration/integration-data/-/blob/master/rosbag/data_manu.py) project. 

> **Note**: pybind11 does not support implicit conversions from cpp arrays to python lists or numpy arrays. Thus it is advised to refactor the code before such that vector is being used instead of arrays. Especially, when those attributes or functions needs to be exposed in python. The internal function can remain the same. 

> **Note**: The sign conventions have been documented in [markdown-docs/farms-integration](https://gitlab.com/biorob-krock/markdown-docs/-/tree/master/farms-integration). Note: That there are three different convention to be aware of webots, legacyController, pybullet, krock. Check [integration-data](https://gitlab.com/farms-integration/integration-data) for more information. 


## Instructions for developing new python-based controllers

#### For developing new python-based controller on **Krock Simulation Pybullet**
- As stated above the best way to use pybullet is with FARMS framework 
- A new controller can be created by extending the controller class written in FARMS 
- Take a look at FARMS's `KinematicsController` or `AmphibiousController` classes on who to extend the base controller 
    - cython can be used to speed up the controller computation speed 
    - Ask Matt and Jon for more info


### For developing new python-based controller on **Krock hardware**
- Add python code under `farms-scripts`, or make another folder 
- Make sure that the `build` folder of the krock-controller is exported to `LD_LIBRRAY_PATH` 
    - This allows to dynamically link the compiled library files (`.so`) for pybind wrappers to be available to python interpreter 
- Writers can take inspiration form `farms-scripts/real_krock.py` on how to write the main loop 
- Make sure that the controller is first run slowly say at 10Hz to check that robot is not doing something rash 
    - Set the robot's Max speed to 50 or 100 before running the designed controller 


