# Linux frequently used commands

## Finding packages 

1. Finding if package is installed and its details: `dpkg -s libeigen3-dev`

```bash
asgupta@biorobpclab4:/usr/local/include$ dpkg -s libeigen3-dev
Package: libeigen3-dev
Status: install ok installed
Priority: optional
Section: libdevel
Installed-Size: 6993
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Architecture: all
Multi-Arch: foreign
Source: eigen3
Version: 3.3.7-2
Depends: pkg-config
Suggests: libeigen3-doc, libmpfrc++-dev
Description: lightweight C++ template library for linear algebra
 Eigen 3 is a lightweight C++ template library for vector and matrix math,
 a.k.a. linear algebra.
 .
 Unlike most other linear algebra libraries, Eigen 3 focuses on the simple
 mathematical needs of applications: games and other OpenGL apps, spreadsheets
 and other office apps, etc. Eigen 3 is dedicated to providing optimal speed
 with GCC. A lot of improvements since 2-nd version of Eigen.
Original-Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Homepage: http://eigen.tuxfamily.org
```
2. Finding files of the installed package: `dpkg-query -L libeigen3-dev`

```bash
asgupta@biorobpclab4:/usr/local/include$ dpkg-query -L libeigen3-dev
/.
/usr
/usr/include
/usr/include/eigen3
/usr/include/eigen3/Eigen
/usr/include/eigen3/Eigen/Cholesky
/usr/include/eigen3/Eigen/CholmodSupport
/usr/include/eigen3/Eigen/Core
/usr/include/eigen3/Eigen/Dense
...
/usr/share/doc/libeigen3-dev/copyright
/usr/share/pkgconfig
/usr/share/pkgconfig/eigen3.pc
```

## Editing files on remote computers  

- Use sshfs: `sshfs [user@]host:[remote_directory] mountpoint [options]`

## Fining directory and files 
[Find command blog](https://www.plesk.com/blog/various/find-files-in-linux-via-command-line/)

* `-O1` – (Default) filter based on file name first
* `-O2` – File name first, then file-type
* `-O3` – Allow find to automatically re-order the search based on efficient use of resources and likelihood of success
* `-maxdepth X` – Search this directory along with all sub-directories to a level of X
* `-iname` – Search while ignoring text case.
* `-not` – Only produce results that don’t match the test case
* `-type f` – Look for files
* `-type d` – Look for directories

### Find a file 
`find <path/to/directory> -type f -name <filename>`

### Find a directory 
`find <path/to/directory> -type d -name <dirname>`

## Find words in files 
[Grep command blog](https://www.cyberciti.biz/faq/howto-use-grep-command-in-linux-unix/)
> **Note**: grep options can be combined for instance `-r -w` can be replaced with `-rw`

### Search a single file

Different examples for grep
```bash
grep 'word' filename
fgrep 'word-to-search' file.txt
grep 'word' file1 file2 file3
grep 'string1 string2'  filename
cat otherfile | grep 'something'
command | grep 'something'
command option1 | grep 'data'
grep --color 'data' fileName
grep [-options] pattern filename 
```

Different options of grep

| Options | Description |
|----------------|--|
| -i        |	Ignore case distinctions on Linux and Unix              |
| -w        |	Force PATTERN to match only whole words                 |
| -v        |	Select non-matching lines                               |
| -n        |	Print line number with output lines                     |
| -h        |	Suppress the Unix file name prefix on output            |   
| -r        |	Search directories recursivly on Linux                  |
| -R        |	Just like -r but follow all symlinks                    |
| -l        |	Print only names of FILEs with selected lines           |   
| -c        |	Print only a count of selected lines per FILE           |   
| --color   |	Display matched pattern in colors                       |


## Editing files on krock-odroid

sshfs [user@]host:[remote_directory] mountpoint [options]

```bash
asgupta@biorobpclab4:~$ sshfs biorob@128.179.184.68:/home/biorob/krock-controller-farms /data/asgupta/krock-U20/
biorob@128.179.184.68's password: 
asgupta@biorobpclab4:~$ 
```