## Old README documentation
KRock 2 Controller from the [BioRobotics Lab](https://biorob.epfl.ch/) at EPFL. Initial development by Tomislav Horvat, now maintained by [Matt Estrada](https://people.epfl.ch/matthew.estrada?lang=en).
Verified code that runs on the robot is in `master` branch while continued development is in the `develop` branch, also cloned into the robot.

Instructions for installation/compilation of the code are below. More documetnation is in the [doc/](doc/) folder of the repo.

## Dependencies

Dependencies are a mix of system installations along with several codebases located under the `lib/` folder to avoid incompatibilities with updates.
Instructions for installation and compilation are described in the next section.

### Third party libraries

Install these onto your system prior to compiling the controller code.

- [dlib 19.0](http://dlib.net/) - C++ toolkit containing machine learning algorithms
- [eigen3](http://eigen.tuxfamily.org/) - C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms
- [CasADi](https://web.casadi.org/) -  free and open source symbolic framework for automatic differentiation and optimal control
- [qpOASES 3.2.0](https://projects.coin-or.org/qpOASES) - a parametric active-set algorithm for quadratic programming
- [ROS Melodic](http://wiki.ros.org/melodic) - for high-level communication with a master node on an offboard PC

### Custom libraries


- Several libraries written for our specific hardware and located in [lib](lib/) folder.
	- Comm - managing communication between 1. Dualshock controller and Linux 2. Contains comm_slave script, which is run when the robot is switched on. This also helps in writing and reading in share memoery space (between main and comm_slave)
	- DXLite-master - modified version of [DXLite](https://github.com/KM-RoBoTa/DXLite) package for controlling Dynamixel motors 
	- vectornav - reading VectorNav IMU
	- Utils - random helpful functions

- discontinued libraries: work in progress for refactoring 
	- Optoforce (discontinued) - reading force sensors at ankles
	- nnTools (discontinued) - neural network tools for training force approximations
	- wiringPi (discontinued)- library to contro GPIO, originally intended for Raspberry Pi
	- XBEE_joy_comm (discontinued)- seems to have been used to communicate with Unity and the previous peripheral, but still has dependencies for communication with the Dualshock4 controller


## Compiling


- In the main folder script called `install_controller.sh` has been provided. This helps user to 
	- Install of all the dependencies
	- Build the project 

- This script has been written to improve upon the old compilation method(below)
	- Used CMake for building 
	- Does not require installationn of libraries in lib folder 
	- all the build files kept in one build folder 

- Assumption:
	1. Git would already be installed 
	2. the controller code has been cloned

- Steps:
	1. `cd <path/to/krock-controller>` 
	2. Run the script with source comand 
	`./install_controller.sh --FOR_WEBOTS=ON --WITH_ROS=ON --JOB_THREADS=8 --VAR_USER --INSTALL_ROS --INSTALL_WEBOTS --ASSUME_YES`
	3. Make sure to check that the following lines can be found in the 
		bashrc file after the script is successful 
		1. source /opt/ros/melodic/setup.bash
		2. export LD_LIBRARY_PATH=.../casadi/..

> Note: On the robot, if this project was a clean install, please make sure to run the [Makefile for comm_slave](https://gitlab.com/biorob-krock/krock-controller/-/blob/astha-refactor/lib/Comm/Comm_slave/Makefile). As the object file of comm_slave is called by the krock's startup script when the robot is switched on. After this the robot might require a reboot. 

> Note: When the robot is started, it follows the following [instructions](https://gitlab.com/biorob-krock/krock-controller/-/blob/astha-refactor/doc/startup.md). If things are not working as expected. Please check that the that `/usr/local/bin/robot_startup.sh` is available and correct path to comm_slave is provided in it. 



## Hardware

- Two [Odroid XU4](https://www.hardkernel.com/shop/odroid-xu4-special-price/) single board computers in communication with one another. 
- Twenty or so [Dynamixel MX-64](http://www.robotis.us/dynamixel-mx-64ar/) motors, along with a few [MX-106](http://emanual.robotis.com/docs/en/dxl/mx/mx-106/)
-[Vecnav VN100](https://www.vectornav.com/products/vn-100) IMU
- [mvBlueFOX](https://www.matrix-vision.com/USB2.0-single-board-camera-with-housing-mvbluefox-igc.html) camera
-[Seek Compact](https://www.thermal.com/compact-series.html) thermal camera
- Optoforce three axis force sensors (now discontinued)

## Logging into to ODROID

Connect to the ODROID computer through ssh over the EPFL wifi. When the robot is turned on, it automatically posts its IP to the Biorob [dynip page](https://biorob2.epfl.ch/pages/internal/dynip.php). 

	ssh biorob@<IP> 

A [bash script](https://gitlab.com/biorob-krock/utils/-/blob/master/ssh/krock_ssh.sh) is convenient to streamline logging in. 

The controller is located in `/home/biorob/krock-controller` as a `robot_main` executable. To start the controller, simply call: 

	./robot_main

To compile on the robot, follow the same procedure instread using `make -j3` since the ODROID can handle fewer job slots. 

### Old compilation method
For compatibility with ROS nodes, follow Ros Melodic [install instructions](http://wiki.ros.org/melodic/Installation/Ubuntu) for `ROS-Base` (not the full-fledged Desktop install). There is also the option to disable ROS within the codebase.

Install one more ROS dependency

```bash
sudo apt install ros-melodic-tf2-eigen ros-melodic-tf2 ros-melodic-tf2-ros
```

Make sure that you correctly source the ros installation files by inserting this command in the `.bashrc` file
``` bash
source /opt/ros/melodic/setup.bash
source ./bashrc
```

source `.bashrc` so the variables are correctly defined (you should be in the directory that contains the `.bashrc` file)
``` bash
source ./bashrc
```

Install boost dependency and libserial for `Comm` library

```bash
sudo apt-get install libboost-all-dev
sudo apt install libserial-dev
```

Install dlib

```bash
sudo apt install libdlib-dev
```

Install eigen3

```bash
sudo apt install libeigen3-dev
```

Install [Casadi](https://github.com/casadi/casadi) dependencies. A local copy of version @6f122ca2 is saved into the git repo.

```bash
sudo apt-get install coinor-libipopt-dev
sudo apt-get install gcc g++ gfortran git cmake liblapack-dev pkg-config --install-recommends
```

Change your directory to the cloned directory and compile Casadi binaries

```bash
cd krock-controller/lib/casadi
mkdir build
cd build
cmake -DWITH_IPOPT=ON ..
make -j8
sudo make install
cd ../..
```

Compile qpOASES-3.2.0. Version 3.2.1 already has some incompatibiilites with the code so work with the local copy of this version.

```bash
cd qpOASES-3.2.0
mkdir build
cd build
cmake ..
make -j8
sudo make install 
cd ..
```

Install libconfig

```bash
cd libconfig-1.7.2
./configure
make
make check
sudo make install
cd ..
```

- Issues 
	- 'aclocal-1.15' is missing on your system
```bash
krock-controller/lib/libconfig-1.7.2/aux-build/missing: line 81: aclocal-1.15: command not found
WARNING: 'aclocal-1.15' is missing on your system.
		You should only need it if you modified 'acinclude.m4' or
		'configure.ac' or m4 files included by 'configure.ac'.
		The 'aclocal' program is part of the GNU Automake package:
		<http://www.gnu.org/software/automake>
		It also requires GNU Autoconf, GNU m4 and Perl in order to run:
		<http://www.gnu.org/software/autoconf>
		<http://www.gnu.org/software/m4/>
		<http://www.perl.org/>
Makefile:408: recipe for target 'aclocal.m4' failed
make: *** [aclocal.m4] Error 127
```
	- Check if the dependencies mentioned - GNU Autoconf, GNU m4 and Perl are installed 
	- Command for Autoconf, `sudo apt install autoconf`
	- Check if 'automake' is installed, otherwise install using `sudo apt-get install automake` 
	- Once all the dependencies and automake are installed, re-run all the previous commands for libconfig.

	- Testsuite XFAIL
	- Example 4 for cpp Testsuite fails, this is a known issue. This issue can be ignored, as it does not affect the main code base. 
```bash
lib/libconfig-1.7.2/examples/c++'
PASS: example1
PASS: example2
PASS: example3
XFAIL: example4
============================================================================
Testsuite summary for libconfig 1.7.2
============================================================================
# TOTAL: 4
# PASS:  3
# SKIP:  0
# XFAIL: 1
# FAIL:  0
# XPASS: 0
# ERROR: 0
```


Compile Krock libraries. Several `.so` should be generated in the `lib/` folder.

```bash
cd lib
make -j8
cd ..
```

Compile Krock controller. You can compile it either for the real robot or for webots:

```bash
# You should be in folder 'krock-controller'
mkdir build && cd build
# For the real robot, use -DFOR_WEBOTS=OFF
cmake .. -DFOR_WEBOTS=ON -DWITH_ROS=ON
make -j8
```
