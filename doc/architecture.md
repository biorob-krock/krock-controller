## Controller Architecture 

## Controller Codebase Architecture

Anytime the codebase is changed, the architecture can be replotted with `doc/directory_diagram.py`
![krock2 codebase architecture](img/krock-dir-structure.gv.png)


## Controller Code Communication Architecture

|krock2 comm architecture real_krock.py|krock2 comm architecture main.cpp (robot_main)|
|-----------------------------------|-----------------------------------|
|The main loop runs in python|The main loop runs in cpp|
|![krock2 comm architecture real_krock](img/Krock-controller-component-diagram-py.png)|![krock2 comm architecture robot_main](img/Krock-controller-component-diagram-cpp.png)|


## Original Robot Architecture

> **Note**: Since only one of the odroid is running currently, all the peripherals are attached to odroid 1 (cyan). currently camera and force sensors are not being used. 


![krock2 control architecture](img/krock2-control-architecture.png)



## Errata 
- Discontinued libraries:
	- Optoforce (discontinued) - reading force sensors at ankles
	- nnTools (discontinued) - neural network tools for training force approximations
	- wiringPi (discontinued)- library to contro GPIO, originally intended for Raspberry Pi
	- XBEE_joy_comm (discontinued)- seems to have been used to communicate with Unity and the previous peripheral, but still has dependencies for communication with the Dualshock4 controller
> **Note**: The Discontinued libraries have been removed, but still some non-functional snippet of code can be found in the codebase. Future work will include remove such dead codes.
