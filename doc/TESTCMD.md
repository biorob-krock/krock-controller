# Useful commands and checklist for running the robot 

# Watch the logs from comm_slave 

- Use the `cout.log` files for checking the status  
- `Watch` 
    - `-c`: to see the colors on display 
    - `-n`: try the -n option to control the interval between each update.
- `tail`
    - display lines from the log file 
    - `-n`: no of lines

```bash
# watch -c (color ANSI) tail (command to be watched) -n (no of line) <log file name>
watch -c tail -n 15 <name_of_the_File>

# OR 
watch -c -n 2 tail -n 15 <name_of_the_File>
```

# kill comm_slave
- `pkill comm_slave`: to kill comm_slave 
- comm_slave is restarted by systemd after about a minute 

# Delete logs 
- `cd log/comm_slave/`
- `rm 2020*` : remove only files that are not being written to  

## Errors 

Issues:

1. After sometime the terminal hangs (odroid) and current drawn 0.0
    - Unable to even ping the odroid in this state 
2. comm_slave

```bash
[01/06/21 12:01:16.-175][START-MAIN]
Error encountered when trying to connect to the sensor.
Segmentation fault (core dumped)
```


3. mac address of PS4 
4. setup the udev rules  


```
biorob@odroid:~$ lsusb
Bus 006 Device 002: ID 0bda:8153 Realtek Semiconductor Corp. RTL8153 Gigabit Ethernet Adapter
Bus 006 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 005 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 004 Device 003: ID 2109:8110 VIA Labs, Inc. Hub
Bus 004 Device 002: ID 05e3:0616 Genesys Logic, Inc. hub
Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 003 Device 003: ID 148f:5370 Ralink Technology, Corp. RT5370 Wireless Adapter
Bus 003 Device 007: ID 0403:6001 Future Technology Devices International, Ltd FT232 Serial (UART) IC
Bus 003 Device 006: ID 0a12:0001 Cambridge Silicon Radio, Ltd Bluetooth Dongle (HCI mode)
Bus 003 Device 005: ID 0403:6001 Future Technology Devices International, Ltd FT232 Serial (UART) IC
Bus 003 Device 004: ID 2109:2811 VIA Labs, Inc. Hub
Bus 003 Device 002: ID 05e3:0610 Genesys Logic, Inc. 4-port hub
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 002: ID 0403:6001 Future Technology Devices International, Ltd FT232 Serial (UART) IC
Bus 002 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

```