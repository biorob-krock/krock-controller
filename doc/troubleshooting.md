# Troubleshooting 


## Debugging the Legacy controller without Webots 

This can be done with following steps 
- set `skipHardware = true` in GLOBAL.cfg 
- run `build/comm_slave_cyan_pc` 
- connect the dualshock to pc via usb cable 
- run `robot_main`

## Debugging the Legacy controller with help of Webots

When the controller crashes in Webots, the error messages will not be broadcasted
inside the Webots console. This makes the debugging process tedious as it is then
necessary to add a lot of prints to find out where the crash is occurring and how
to solve the issue.

It is possible to run the gdb debugger with the Webots controller as follows:
1. In webots, change the robot `controller` to `controller "<extern>"` in the webots wbt file 
2. Start the simulation. 
    - run `roscore` 
    - cd into `path/to/webots-simulation`
    - ` webots worlds/krock-2-obstacles.wbt`
3. Remove the `path/to/webots-simulation/controllers/krock-controller/build` folder 
4. Compile the controller with the debug symbols:
`/install_controller.sh --FOR_WEBOTS=ON --WITH_ROS=ON --JOB_THREADS=8 --VAR_USER --DEBUG_MODE=ON`
5. Source `source ~/.bashrc`
6. Check if the `echo $LD_LIBRARY_PATH` has casadi path:
`..webots-simulation/controllers/krock-controller/build/lib/casadi/lib`

7. You need to export the path to Webots libraries to your environment:
(For Linux) `export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/webots/lib/controller`
8. Run the debugger: `gdb krock-controller`.
    - `gdb$ run` use run command on gdb to start running the controller  
9. Use gdb as you please ([Quick guide](https://www.tutorialspoint.com/gnu_debugger/gdb_quick_guide.htm))

<img src="img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-01.png" width="200"/>

![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-1.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-2.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-3.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-4.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-5.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-6.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-7.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-8.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-9.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-10.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-11.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-12.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-13.png)
![Legacy controller debug with Webots](img/Krock-Legacy-controller-webots-debug/Krock-Legacy-controller-webots-debug-14.png)
## Known Issues (Before install_controller.sh scripts)

Before `install_controller.sh` was written, may of the libraries inside libs folder required manual installation. Many of the libraries were using Makefile and these files required to be invoked one by one. The issues listed below are the ones that were encountered before automated installation using CMakeLists and install_controller script.  

#### qpOASES version

While the controller compiles and, an error is thrown when:
- Using qpOASES newer thanversion  3.2.0 
- Including `-O3` option within CXXFLAGS throws the following error.
- Compiling for the arm processor

```
error: double free or corruption (out)
```

Otherwise it compiles and runs fine on Matt's PC  

#### libconfig files


libconfig .cfg files throw a syntax error if last line ends in a comment. SImply hit enter and leave a blank line at the end. 

```
Parse error at config/swimming.cfg:25 - syntax error
```


## General Troubleshooting Tips

Update linkers after you've installed new packages 
```
sudo ldconfig
```

Double check linking to dynamic libraries for executable (e.g. `robot_main`)

```
ldd robot_main
```