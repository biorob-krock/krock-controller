# Networking
## On EPFL Network


### IP Addresses and ROS Environmental variables
Networking is achieved through DNS with utilizing MAC addresses. For Odroid Magenta can be pinged at `M7cdd90e03d59.dyn.epfl.ch`. Alternatively you can use the [Biorob Dynamic IP page](https://biorob2.epfl.ch/pages/internal/dynip.php). 

Each device is given one with its MAC address. Defined in the `.bashrc` of each machine. Note that `ROS_IP` and `ROS_MASTER_URI` will changed based on the machine and which PC is hosting experiments. For `~/.bashrc` on `cyan`: 

```bash
# ROS variables
source /opt/ros/melodic/setup.bash
source ~/catkin_ws/devel/setup.bash

# Machine addresses
export BIOROB_MOBILE=false

if $BIOROB_MOBILE; then
	export LAPTOP_BIOROB_IP='biorobB207457'
	export CYAN_IP='odroid-cyan'
	export MAGENTA_IP='odroid-magenta'
	export ROS_IP=$LAPTOP_BIOROB_IP
	export ROS_MASTER_URI='http://'$LAPTOP_BIOROB_IP':11311'
else
	export PC_BIOROB_IP='biorobpc25.epfl.ch'
	export LAPTOP_BIOROB_IP='M4074E014F0E6.dyn.epfl.ch'
	export CYAN_IP='M7cdd90adb51f.dyn.epfl.ch'
	export MAGENTA_IP='M7cdd90e03d59.dyn.epfl.ch'
	export PC_REZA_IP='biorobpc7.epfl.ch'
	export ROS_IP=$CYAN_IP
	export ROS_MASTER_URI='http://'$LAPTOP_BIOROB_IP':11311'
fi
```

**Note**: EPFL aliases based on MAC addresses will not register if the machine is on the wired biorob network (e.g. the NCCR lenovo laptop plugged into ethernet), so be aware of this if you are having trouble connecting.

## Off EPFL Network 

### biorob-mobile router 
    
ping directly with hostname (find with `cat /etc/hostname`)

    network: biorob-mobile
    pass: <find on the [website](http://go.epfl.ch/biorob-wifi) indicated on the box>
    
Enter into `/etc/wpa_supplicant/wpa_supplicant.conf`. [Documentation](https://www.freebsd.org/cgi/man.cgi?query=wpa_supplicant.conf&sektion=5&n=1) says that a higher number for priority is more desirable to connect to. 

    network={
        priority=9
        ssid="biorob-mobile"
        proto=WPA2
        key_mgmt=WPA-PSK
        psk="..."
    }

The router has a convenient DNS that allows you to ping and ssh from hostnames of devices on the network. 
    
*Hostnames* (as of 2020-07-20)
 - odroid-magenta
 - odroid-cyan
 - biorobB207457


[How to Mount and Access Ext4 Partition in Mac]( https://www.maketecheasier.com/mount-access-ext4-partition-mac/ ): `sudo ext4fuse /dev/disk2s2 ~/tmp/orobot_sd -o allow_other`
    
### Dlink DWR 730 dlink_portable_robohub (legacy from Tomislav/Kamilo)

Legacy [Dlink DWR 730](https://www.manualslib.com/products/D-Link-Dwr-730-3001589.html) that is still around the lab. 
    
    network: dlink_portable_robohub
    pass: biorob_salamander

    network={
        priority=9
        ssid="dlink_portable_robohub"
        proto=WPA2
        pairwise=CCMP
        group=CCMP
        key_mgmt=WPA-PSK
        psk="biorob_salamander"
    }

Router password is the [default](https://setuprouter.com/router/dlink/dwr-730/login.htm): `admin / [blank]`
