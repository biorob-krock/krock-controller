# Odroid-XU4 Clean Setup Ubuntu 20.04 (Minimal)

Below are notes on how to set up a clean installation of Ubuntu 20.04 on an ODROID computer. 

## Download the ubuntu image using 


[Ubunutu minimal 20.04.1 image](http://de.eu.odroid.in/ubuntu_20.04lts/XU3_XU4_MC1_HC1_HC2/) (EU  germany download : `ubuntu-20.04.1-5.4-minimal-odroid-xu4-20200818.img.xz)`) 

```
wget -r -np <image url>
wget -r -np http://de.eu.odroid.in/ubuntu_20.04lts/XU3_XU4_MC1_HC1_HC2/ubuntu-20.04.1-5.4-minimal-odroid-xu4-20200818.img.xz
```

## Flash the image on a SD card 

Required:
- SD card reader or USB with SD card reader (micro sd reader + micro sd )

1. Make sure the micro sd reader is in the read write mode  [sliding switch](https://www.quora.com/What-is-the-sliding-thing-on-the-side-of-the-SD-cards)
2. Downlaod  [Etcher](https://www.balena.io/etcher/)
3. Use Etcher to Flash the above image on the odroid

After the image has been flashed two partitions will appear `boot` and `rootfs`

## Networking Changes peculiar to Ubuntu20

In Ubuntu20 `NetworkManager` (`nmcli` is the network manager client) is used by default instead of `ifupdown` (ubutnu18). To have the below configuration we would need:

1. Monitor 
2. Keyboard 
3. Ethernet connection (Ask ale, need to give him mac address of eth0, use `ip link` to find it)
4. Check if ethernet working by  `ping 1.1.1.1`, this should show some return msgs in the command line

Log on to odroid:
Using username: `root` and password: `odroid`
Essential things 
    1. Change password for root using `passwd`
    2. Make a user: `biorob` (give the general passowrd, ask Matt)
    3. Add this user to sudo user using `vigr` or anything else

[Blog: create a sudo user](https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart)


Install the following 
- ifconfig 
- net-tools
- iptables 
- vim
- update the system 
- wpa_supplicant (should be installed by default)

Purge the following:
- Networkmanager: `apt-get purge network-manager`

Update the system 
- `apt dist-upgrade`
- Matt's comment: Remove network manager (had a problem getting hung up on boot waiting for eth0 connection

Make sure that the networking service is enable `systemctrl status networking`

List of useful commands before starting 
- `systemctrl status networking`
- `systemctrl enable service` 

Notes from Matt:
  * Start with ethernet to download first needed packages :
    * ip link set eth0 up
    * dhclient -v eth0
    * (Check if it works : ping 1.1.1.1)

## Steps for configuring odroid 

In the `rootfs` partitions make  the following changes 

1. cd into the folder `cd /media/asgupta/rootfs`
2. Be careful not to edit the system file of your own computer(if using ubuntu)
3. setup the interface `sudo gedit ./etc/network/interfaces`
```bash
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet dhcp

auto wlan0
iface wlan0 inet dhcp
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
    post-up /usr/local/bin/updateip.sh
    pre-down /usr/local/bin/updateip.sh release
```
> Changes made: allow-hotplug wlan0 (Ubuntu18) to  auto wlan0 (Ubuntu20)

4. Add wifi connection to wpa_supplicant
    - `sudo gedit ./etc/wpa_supplicant/wpa_supplicant.conf` (note: the password for the WiFi network is not supposed to be published online, please ask for it.)

```bash
ctrl_interface=DIR=/var/run/wpa_supplicant

update_config=1
country=CH
network={
    key_mgmt=WPA-EAP
    ssid="epfl"
    proto=WPA2
    eap=PEAP
    identity="biorob-robots@epfl.ch"
    password="................."
    anonymous_identity="anonymous@epfl.ch"
    phase2="auth=MSCHAPV2"
    ca_cert="/etc/ssl/certs/QuoVadis_Root_CA.pem"
    subject_match="CN=radius.epfl.ch"
    priority=8
}
```
> Changes made: thawte_Primary_Root_CA.pem (Ubuntu18) to QuoVadis_Root_CA.pem (Ubuntu20)

5. Added updateip.sh script to update the ip at the [dynamic ip](https://biorob2.epfl.ch/pages/internal/dynip.php) page 
    - `sudo gedit ./usr/local/bin/updateip.sh` (the hostname can be modified to be more meaningful and mail acquisition of the IP can be added)


```bash

#!/bin/bash
# \file   updateip.sh
# \brief  BIOROB dynamic IP notification script
# \author alessandro.crespi@epfl.ch
# \date   July 2019

# host name of the machine (mandatory to distinguish it in the list)
# please use something meaningful, generic meaningless names might be blocked
hostname="Krockbot-XU4-Ubuntu20"

# options for wget. Use -4 to force using IPv4 connectivity (if your host has
# IPv6 but you want the IPv4 address recorded), or -6 to force IPv6
wgetopt=

# set name of desired network interface (e.g. eth0, wlan1, etc.) if needed;
# by default the first active interface with an IP address and broadcast
# capability (i.e., not the loopback) is taken
interface=wlan0

# send a mail with the info to the listed addresses (separated by comma)
mail=

# extracts the local address of the network interface (or of the first one)
lip="/sbin/ifconfig $interface | grep -Eo "inet.+cast" | grep -Eo '([0-9]{1,3}[\.]){3}[0-9]{1,3}' | head -1"

# extracts the MAC address of the network interface (useful e.g. for debugging)
mac="/sbin/ifconfig $interface | grep -E "HWaddr|ether" | grep -Eo '([0-9a-f]{2}[\:]){5}[0-9a-f]{2}' | head -1"

# adapts request parameters depending if we are updating or releasing the IP
if [ "$1" == "release" ]; then
urladd='&release_ip'
textop="Releasing";
else
urladd=''
textop="Updating";
fi

# adds mail field if needed
if [ ! -z $mail ]; then
urladd="${urladd}&mail=${mail}";
fi

# makes the actual request to update the address
echo -n "$textop IP address on server... "
wget ${wgetopt} -O - -q "http://biorob2.epfl.ch/utils/dynupd.php?hostname=${hostname}&internal_ip=${lip}&mac=${mac}${urladd}"
echo
```


5. give executable right to updateip.sh: `chmod +x ./usr/local/bin/updateip.sh` 
6. Make sure networking service is enabled 
7. shutdown and remove the ethernet cable 
8. Boot again(without ethernet cable, with wifi module)
9. Make sure ping 1.1.1.1 retuns something (this means wifi module is good to go)
10. Check the [dynamic ip](https://biorob2.epfl.ch/pages/internal/dynip.php), it should show the odroid computer(check the hostname)
    - if not, then that means some error happened while running a updateip.sh (command in the interface file)
11. Login via ssh using `ssh biorob@<odroid_ip>` and password (set before).
12. Hopefully by the end you would have spend only half a day (instead of spending 2 days like me!)



## Debugging
  * Check syslog to find the issue: `tail /var/log/syslog`
  * Make the script executable : `chmod +x /usr/local/bin/updateip.sh`
  * Network maanger is removed apt-get purge network-manager
  * CA cert can be a source of error 
  sudo apt-get purge network-manager
  * Start the wifi : ifup wlan0
  * check `date`, it should be today's date

## Other installations 
- git 
- htop 

### ROS 
ROS installation (http://wiki.ros.org/melodic/Installation/Ubuntu) -> ROS-base bare bones 

Create workspace :
    * mkdir -p ~/catkin_ws/src
    * cd ~/catkin_ws/
    * catkin_make
    * To avoid sourcing every new tab:
      * %%echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc%% 
      * %%source ~/.bashrc%%


## Cloning the ODROID 

Notes on cloning 

``` bash
#!/bin/bash

# THIS SCRIPT IS NOT MEANT TO BE RUN 

# Adapting it for Mac ports 
# pv is the Pipe Viewer package, showing the rate of data exchange 
# dd is terminal reading/writing 

TAR_DISK=/dev/disk2
TAR_IMG=~/Desktop/krock1_properbackup_2019_14_2019.img

# diskutil list will show you the ports 
diskutil list 

# Read disk 
sudo dd if=$TAR_DISK | pv | sudo dd of=$TAR_IMG bs=1024

# Common error is "resource busy" 
# Found solution here: 
# 	https://raspberrypi.stackexchange.com/questions/9217/resource-busy-error-when-using-dd-to-copy-disk-img-to-sd-cardcloneODROID.sh

# Write image
sudo dd if=$TAR_IMG | pv | sudo dd of=$TAR_DISK bs=1024

####################
# Notes from Ale on files to be changed after closing
####################
# Be sure to change these files 
# /etc/hostname (change)
# /etc/hosts (replace host -> hostname must resolve to 127.0.0.1, remove unrelated old hostnames to prevent conflicts)
# /etc/udev/rules.d/70-persistent-net.rules (EMPTY IT NO DELETE)
# /etc/network/interfaces
```
