# Step by Step guide to krock 

Assumption: 
    - krock odroid is already setup 
    - krock-controller has been cloned 
    - In the below case `krock-controller-farms` is the name of the folder 
        - Instead of `krock-controller` if the code was cloned without renaming 
    - `comm_slave_cyan` is already setup to start executing on boot 
    - *All of the commands on odroid are being run in the same terminal session*

## Logging in Krock Odroid computer 
```bash
asgupta@biorobpclab4:~$ ssh biorob@128.179.190.239
The authenticity of host '128.179.190.239 (128.179.190.239)' can't be established.
ECDSA key fingerprint is SHA256:sonW9De9vhKXeZmR7umJ0KNqSaF87qNlJgIr2uDsHHg.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes 
Warning: Permanently added '128.179.190.239' (ECDSA) to the list of known hosts.
biorob@128.179.190.239's password: 
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.65-216 armv7l)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
Last login: Fri Feb  5 12:59:38 2021 from 128.178.148.57
biorob@odroid:~$ ls
Desktop    Music     Software   farms-integration
Documents  Pictures  Templates  krock-controller-farms
Downloads  Public    Videos
biorob@odroid:~$ cd krock-controller-farms/
biorob@odroid:~/krock-controller-farms$ ls
CMakeLists.txt   doc                    main.cpp
README.md        doxygenconfig          main_webots.cpp
TESTCMD.md       farms_scripts          robot_main
backup           include                source
build            install_controller.sh  tests
comm_slave_cyan  lib                    ws_krock_controller.code-workspace
config           log
biorob@odroid:~/krock-controller-farms$ git status 
On branch 5-farms-integration
Your branch is up to date with 'origin/5-farms-integration'.

nothing to commit, working tree clean
biorob@odroid:~/krock-controller-farms$ git pull 
remote: Enumerating objects: 36, done.
remote: Counting objects: 100% (36/36), done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 213 (delta 31), reused 31 (delta 31), pack-reused 177
Receiving objects: 100% (213/213), 853.50 KiB | 1.21 MiB/s, done.
Resolving deltas: 100% (108/108), completed with 21 local objects.
From gitlab.com:biorob-krock/krock-controller
   ba1c74d..985d29e  5-farms-integration -> origin/5-farms-integration
Updating ba1c74d..985d29e
Fast-forward
 CMakeLists.txt                                     |  29 +-
 README.md                                          | 322 ++++++--------
 ..
 77 files changed, 2362 insertions(+), 2038 deletions(-)
 delete mode 100644 backup/main.cpp
 create mode 100644 doc/README_OLD.md
 rename TESTCMD.md => doc/TESTCMD.md (100%)
 ..
 delete mode 100644 tests/real_krock_pybind_test.py
 ```


## Building the  krock-controller 

```
./install_controller.sh --FOR_WEBOTS=OFF --WITH_ROS=ON --JOB_THREADS=2 --VAR_USER
 FOR_WEBOTS=  OFF  Build for webots
 WITH_ROS=  ON  Build with ros
 JOB_THREADS=  2  No of jobs threads for make
 VAR_USER=  sudo  '' if no sudo user rights else 'sudo'
 ROS_INSTALL_FLAG=  n  'Y' if webots ros be installed else 'n'
 WEBOTS_INSTALL_FLAG=  n  'Y' if webots should be installed else 'n'
 DEBUG_MODE=  OFF  Cmake debug mode
 ROS is not being installed 
 Webots is not being installed 
 # Installing ... 

 libboost-all-dev  already installed 

 libserial-dev  already installed 

 libdlib-dev  already installed 

 libeigen3-dev  already installed 

 coinor-libipopt-dev  already installed 

 gcc  already installed 

 g++  already installed 

 gfortran  already installed 

 git  already installed 

 cmake  already installed 

 liblapack-dev  already installed 

 pkg-config  already installed 

# Status
 libboost-all-dev : Installed 
 libserial-dev : Installed 
 libdlib-dev : Installed 
 libeigen3-dev : Installed 
 coinor-libipopt-dev : Installed 
 gcc : Installed 
 g++ : Installed 
 gfortran : Installed 
 git : Installed 
 cmake : Installed 
 liblapack-dev : Installed 
 pkg-config : Installed 
-- Using CATKIN_DEVEL_PREFIX: /home/biorob/krock-controller-farms/build/devel
-- Using CMAKE_PREFIX_PATH: /opt/ros/noetic
-- This workspace overlays: /opt/ros/noetic
-- Found PythonInterp: /usr/bin/python3 (found suitable version "3.8.5", minimum required is "3.8.5") 
-- Using PYTHON_EXECUTABLE: /usr/bin/python3
-- Using Debian Python package layout
-- Using empy: /usr/lib/python3/dist-packages/em.py
-- Using CATKIN_ENABLE_TESTING: ON
-- Call enable_testing()
-- Using CATKIN_TEST_RESULTS_DIR: /home/biorob/krock-controller-farms/build/test_results
-- Forcing gtest/gmock from source, though one was otherwise available.
-- Found gtest sources under '/usr/src/googletest': gtests will be built
-- Found gmock sources under '/usr/src/googletest': gmock will be built
-- Found PythonInterp: /usr/bin/python3 (found version "3.8.5") 
-- Using Python nosetests: /usr/bin/nosetests3
-- catkin 0.8.9
-- BUILD_SHARED_LIBS is on
-- Using these message generators: gencpp;geneus;genlisp;gennodejs;genpy
CMake Deprecation Warning at lib/casadi/CMakeLists.txt:31 (cmake_policy):
  The OLD behavior for policy CMP0011 will be removed from a future version
  of CMake.

  The cmake-policies(7) manual explains that the OLD behaviors of all
  policies are deprecated and that a policy should be set to OLD only under
  specific short-term circumstances.  Projects should be ported to the NEW
  behavior and not rely on setting a policy to OLD.


CMake Deprecation Warning at lib/casadi/CMakeLists.txt:35 (cmake_policy):
  The OLD behavior for policy CMP0005 will be removed from a future version
  of CMake.

  The cmake-policies(7) manual explains that the OLD behaviors of all
  policies are deprecated and that a policy should be set to OLD only under
  specific short-term circumstances.  Projects should be ported to the NEW
  behavior and not rely on setting a policy to OLD.


-- No flag needed for enabling C++11 features.
-- Could NOT find Ipopt (missing: Ipopt_DIR)
-- The following features have been enabled:

 * dynamic-loading, Compile with support for dynamic loading of generated functions (needed for ExternalFunction)
 * sundials-interface, Interface to the ODE/DAE integrator suite SUNDIALS.
 * csparse-interface, Interface to the sparse direct linear solver CSparse.
 * tinyxml-interface, Interface to the XML parser TinyXML.
 * ipopt-interface, Interface to the NLP solver Ipopt.

-- The following OPTIONAL packages have been found:

 * Ipopt
 * PkgConfig

-- The following REQUIRED packages have been found:

 * cpp_common
 * rostime
 * roscpp_traits
 * roscpp_serialization
 * message_runtime
 * rosconsole
 * std_msgs
 * rosgraph_msgs
 * xmlrpcpp
 * roscpp
 * PythonInterp
 * catkin
 * gencpp
 * geneus
 * genlisp
 * gennodejs
 * genpy
 * genmsg
 * actionlib_msgs
 * actionlib
 * geometry_msgs
 * message_filters
 * rosgraph
 * message_generation
 * tf2_msgs
 * tf2
 * rospy
 * tf2_py
 * tf2_ros
 * Threads
 * boost_system (required version == 1.71.0)
 * boost_atomic (required version == 1.71.0)
 * boost_chrono (required version == 1.71.0)
 * boost_date_time (required version == 1.71.0)
 * boost_filesystem (required version == 1.71.0)
 * boost_regex (required version == 1.71.0)
 * boost_thread (required version == 1.71.0)
 * boost_headers (required version == 1.71.0)
 * boost_log (required version == 1.71.0)
 * Boost
 * IPOPT

-- The following features have been disabled:

 * opencl-support, Enable just-in-time compiliation to CPUs and GPUs with OpenCL.
 * blasfeo-interface, Interface to blasfeo.
 * hpmpc-interface, Interface to hpmpc.
 * superscs-interface, Interface to Conic solver SuperSCS.
 * osqp-interface, Interface to QP solver OSQP.
 * dsdp-interface, Interface to the interior point SDP solver DSDP.
 * clang-interface, Interface to the Clang JIT compiler.
 * lapack-interface, Interface to LAPACK.
 * qpoases-interface, Interface to the active-set QP solver qpOASES.
 * blocksqp-interface, Interface to the NLP solver blockSQP.
 * bonmin-interface, Interface to the MINLP framework Bonmin.
 * knitro-interface, Interface to the NLP solver KNITRO.
 * cplex-interface, Interface to the QP solver CPLEX.
 * clp-interface, Interface to the LP solver CLP.
 * cbc-interface, Interface to the LP solver CBC.
 * snopt-interface, Interface to the NLP solver SNOPT.
 * hsl-interface, Interface to HSL.
 * mumps-interface, Interface to MUMPS.
 * ooqp-interface, Interface to the QP solver OOQP (requires BLAS and HSL libraries).
 * gurobi-interface, Interface to the (mixed-integer) QP solver GUROBI
 * sqic-interface, Interface to the QP solver SQIC.
 * ampl-interface, Interface to the AMPL solver library.
 * slicot-interface, Interface to the controls library SLICOT.
 * worhp-interface, Interface to the NLP solver Worhp (requires gfortran, gomp).

-- Found Boost: /usr/lib/arm-linux-gnueabihf/cmake/Boost-1.71.0/BoostConfig.cmake (found version "1.71.0") found components: thread system 
-- pybind11 v2.6.1 
-- Found PythonInterp: /usr/bin/python3 (found suitable version "3.8.5", minimum required is "3") 
[STATUS] CYAN_STARTUP:  comm_slave_cyan: ON 
[STATUS] Executing: comm_slave_cyan 
[STATUS] Completed:   comm_slave_cyan 
-- Configuring done
-- Generating done
-- Build files have been written to: /home/biorob/krock-controller-farms/build
[  1%] Built target vectornav
[  3%] Built target ps3joy
[  5%] Built target dxlite
[  7%] Built target config++
Scanning dependencies of target casadi
[ 11%] Built target qpOASES
[ 12%] Built target casadi_tinyxml
[ 23%] Built target casadi_csparse
[ 23%] Building CXX object lib/casadi/casadi/core/CMakeFiles/casadi.dir/code_generator.cpp.o
[ 33%] Built target casadi_sundials
[ 35%] Built target config
[ 35%] Built target c++_example4
[ 36%] Built target c++_example3
[ 36%] Built target c++_example2
[ 37%] Built target c++_example1
[ 37%] Built target libtinytest
[ 37%] Built target example1b
[ 38%] Built target example1
[ 39%] Built target example5
[ 40%] Built target example2
[ 40%] Built target example3
...
[100%] Linking CXX executable ../../../bin/rocket_single_shooting
[100%] Built target rocket_single_shooting
./install_controller.sh: line 235: [: ==: unary operator expected
./install_controller.sh: line 242: [: ==: unary operator expected
casadi path already added:  /home/biorob/krock-controller-farms/build/lib/casadi/lib 
LD_LIBRARY_PATH:  :/home/biorob/krock-controller-farms/build/lib/casadi/lib:/home/biorob/krock-controller-farms/build/lib/vectornav:/opt/ros/noetic/lib:/usr/lib/arm-linux-gnueabihf/hdf5/serial:/usr/lib/arm-linux-gnueabihf/hdf5/serial/:/usr/lib/arm-linux-gnueabihf/hdf5 
vectornav path already added:  /home/biorob/krock-controller-farms/build/lib/vectornav 
LD_LIBRARY_PATH:  :/home/biorob/krock-controller-farms/build/lib/casadi/lib:/home/biorob/krock-controller-farms/build/lib/vectornav:/opt/ros/noetic/lib:/usr/lib/arm-linux-gnueabihf/hdf5/serial:/usr/lib/arm-linux-gnueabihf/hdf5/serial/:/usr/lib/arm-linux-gnueabihf/hdf5 
build path already added:  /home/biorob/krock-controller-farms/build 
PYTHONPATH:  /opt/ros/noetic/lib/python3/dist-packages:/home/biorob/krock-controller-farms/build 
  If you use zsh, kindly export the paths to .zshrc : Installed 
 Build complete 
```


## Check communication with the robot

- Check if comm_slave_cyan is running 
```bash
biorob@odroid:~/krock-controller-farms$ top
top - 11:51:07 up 6 min,  1 user,  load average: 0.89, 0.49, 0.21
Tasks: 139 total,   2 running, 137 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.7 us,  2.2 sy,  0.0 ni, 95.7 id,  0.0 wa,  1.3 hi,  0.1 si,  0.0 st
MiB Mem :   1993.4 total,   1573.6 free,    122.0 used,    297.8 buff/cache
MiB Swap:      0.0 total,      0.0 free,      0.0 used.   1807.4 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                                                                                               
  695 biorob    20   0   13260   1780   1588 R  22.5   0.1   1:14.31 comm_slave_cyan                                                                                                                       
  118 root      20   0       0      0      0 I   1.1   0.0   0:01.85 kworker/u16:1-phy0                                                                                                                    
    7 root      20   0       0      0      0 I   0.8   0.0   0:02.01 kworker/u16:0-events_unbound   
```

## Check for hardware status  

### Check for hardware logs  are created

Hardware logs are created by `comm_slave_cyan`

```bash
biorob@odroid:~/krock-controller-farms$ ls -l log/comm_slave/
total 196240
-rw-r--r-- 1 biorob biorob 41729499 Feb  5 13:36 2021_02_04-17_26_24_cerr.log
-rw-r--r-- 1 biorob biorob 55623680 Feb  5 13:36 2021_02_04-17_26_24_cout.log
-rw-r--r-- 1 biorob biorob 33408239 Feb 11 10:51 2021_02_05-13_36_37_cerr.log
-rw-r--r-- 1 biorob biorob 44716032 Feb 11 10:51 2021_02_05-13_36_37_cout.log
-rw-r--r-- 1 biorob biorob 10879352 Feb 11 11:54 2021_02_11-10_51_27_cerr.log
-rw-r--r-- 1 biorob biorob 14561280 Feb 11 11:54 2021_02_11-10_51_27_cout.log
biorob@odroid:~/krock-controller-farms$ rm log/comm_slave/2021_02_04-17_26_24_c*
biorob@odroid:~/krock-controller-farms$ rm log/comm_slave/2021_02_05-13_36_37_c*
biorob@odroid:~/krock-controller-farms$ ls -l log/comm_slave/
total 28484
-rw-r--r-- 1 biorob biorob 12468356 Feb 11 11:56 2021_02_11-10_51_27_cerr.log
-rw-r--r-- 1 biorob biorob 16687104 Feb 11 11:56 2021_02_11-10_51_27_cout.log
```

### Check the status of the peripheral devices 

```bash
biorob@odroid:~/krock-controller-farms$ watch -c -n 2 tail -n 15 log/comm_slave/2021_02_11-10_51_27_cout.log 
```

Starts displaying the following: 
```bash
Every 2.0s: tail -n 15 log/comm_slave/2021_02_11-10_51_27_cout.log                                                                                                         odroid: Thu Feb 11 11:57:48 2021

[02/11/21 11:57:48.-71][SENSOR-IMU] initialized:          true
[02/11/21 11:57:48.-71][JOYSTICK] initialized:            false
[02/11/21 11:57:48.-62]imu 
[02/11/21 11:57:48.-61][MOTOR-VALUES] TTLSwitch Status:   false
[02/11/21 11:57:48.-61][SENSOR-IMU] initialized:          true
[02/11/21 11:57:48.-61][JOYSTICK] initialized:            false
[02/11/21 11:57:48.-52]imu 
[02/11/21 11:57:48.-51][MOTOR-VALUES] TTLSwitch Status:   false
[02/11/21 11:57:48.-51][SENSOR-IMU] initialized:          true
[02/11/21 11:57:48.-51][JOYSTICK] initialized:            false
[02/11/21 11:57:48.-42]imu
```

Do a Ctrl+c to exit watch and return to normal terminal 


### Turn on all the peripherals required 


**Step 1**: press the PS button on the dualshock controller. 

Observed behaviour: 
    - a light blue light on the back of the dualshock flickers for few seconds
    - after that a stable blue light is on 

This means that the controller is connected with odroid 

**Step 2**: check if comm_slave_cyan can communicate with the controller 

```bash
biorob@odroid:~/krock-controller-farms$ watch -c -n 2 tail -n 15 log/comm_slave/2021_02_11-10_51_27_cout.log 
```

Starts displaying the following: 
```bash
Every 2.0s: tail -n 15 log/comm_slave/2021_02_11-10_51_27_cout.log                                                                                                         odroid: Thu Feb 11 11:57:48 2021

[02/11/21 12:02:56.-394]imu
[02/11/21 12:02:56.-392][MOTOR-VALUES] TTLSwitch Status:   false
[02/11/21 12:02:56.-392][SENSOR-IMU] initialized:          true
[02/11/21 12:02:56.-392][JOYSTICK] initialized:            true
[02/11/21 12:02:56.-384]imu
[02/11/21 12:02:56.-382][MOTOR-VALUES] TTLSwitch Status:   false
[02/11/21 12:02:56.-381][SENSOR-IMU] initialized:          true
[02/11/21 12:02:56.-381][JOYSTICK] initialized:            true
[02/11/21 12:02:56.-374]imu
[02/11/21 12:02:56.-371][MOTOR-VALUES] TTLSwitch Status:   false
[02/11/21 12:02:56.-371][SENSOR-IMU] initialized:          true
[02/11/21 12:02:56.-371][JOYSTICK] initialized:            true

```

Observed behaviour: `[02/11/21 12:02:56.-371][JOYSTICK] initialized:            true` 


**Step 3**: switch on the ttl switch that powers the motors 

* start the watch statement 
```bash
biorob@odroid:~/krock-controller-farms$ watch -c -n 2 tail -n 15 log/comm_slave/2021_02_11-10_51_27_cout.log 
```

```bash
[02/11/21 12:02:56.-374]imu
[02/11/21 12:02:56.-371][MOTOR-VALUES] TTLSwitch Status:   false
[02/11/21 12:02:56.-371][SENSOR-IMU] initialized:          true
[02/11/21 12:02:56.-371][JOYSTICK] initialized:            true
```

Observed:
- `[02/11/21 12:02:56.-371][MOTOR-VALUES] TTLSwitch Status:   false` 
- current drawn: 0.2 -0.4 A

* Press the share button on the left side of the controller 

```bash
[02/11/21 12:04:39.-801][MOTOR-VALUES] TTLSwitch Status:   true
[02/11/21 12:04:39.-801][SENSOR-IMU] initialized:          true
[02/11/21 12:04:39.-801][JOYSTICK] initialized:            true
[02/11/21 12:04:39.-793]imu
```

Observed:
- `[02/11/21 12:02:56.-371][MOTOR-VALUES] TTLSwitch Status:   true` 
- current drawn: 1.5 - 1.6 A


## Executing Legacy controller 

### Check the contents of the krock-controller folder 

Executables created and present in the source folder:
- robot_main
- comm_slave_cyan
```
biorob@odroid:~/krock-controller-farms$ ls -l 
total 2868
-rw-rw-r--  1 biorob biorob   16528 Feb 11 10:23 CMakeLists.txt
-rw-rw-r--  1 biorob biorob   11140 Feb 11 10:23 README.md
drwxrwxr-x 13 biorob biorob    4096 Feb 11 11:50 build
-rwxrwxr-x  1 biorob biorob  344728 Feb 11 10:34 comm_slave_cyan
drwxrwxr-x  3 biorob biorob    4096 Jan  5 10:23 config
drwxrwxr-x  3 biorob biorob    4096 Feb 11 10:23 doc
-rw-rw-r--  1 biorob biorob  111617 Feb 11 10:23 doxygenconfig
drwxrwxr-x  3 biorob biorob    4096 Feb 11 10:23 farms_scripts
drwxrwxr-x  8 biorob biorob    4096 Jan  5 10:23 include
-rwxrwxr-x  1 biorob biorob   11706 Feb 11 10:23 install_controller.sh
drwxrwxr-x 10 biorob biorob    4096 Jan  5 10:23 lib
drwxr-xr-x  3 biorob biorob    4096 Jan 15 12:58 log
-rwxrwxr-x  1 biorob biorob 2387020 Feb 11 11:50 robot_main
drwxrwxr-x  9 biorob biorob    4096 Feb 11 10:23 source
drwxrwxr-x  4 biorob biorob    4096 Feb 11 10:23 tests
-rw-rw-r--  1 biorob biorob      60 Jan 14 14:31 ws_krock_controller.code-workspace
```
### Start roscore on host computer 

biorobpclab4 is the host computer in my case:

```bash
asgupta@biorobpclab4:~$ roscore 
... logging to /home/asgupta/.ros/log/73ef92c6-6c62-11eb-8590-091fbabb7a72/roslaunch-biorobpclab4-751175.log
Checking log directory for disk usage. This may take a while.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://biorobpclab4:39911/
ros_comm version 1.15.8


SUMMARY
========

PARAMETERS
 * /rosdistro: noetic
 * /rosversion: 1.15.8

NODES

auto-starting new master
process[master]: started with pid [751183]
ROS_MASTER_URI=http://biorobpclab4:11311/

setting /run_id to 73ef92c6-6c62-11eb-8590-091fbabb7a72
process[rosout-1]: started with pid [751193]
started core service [/rosout]
```

### Check if host computer is reachable 
Check if the hostcomputer is pingable 
```bash
biorob@odroid:~/krock-controller-farms$ ping biorobpclab4
PING biorobpclab4.epfl.ch (128.178.148.174) 56(84) bytes of data.
64 bytes from biorobpclab4.epfl.ch (128.178.148.174): icmp_seq=1 ttl=61 time=0.956 ms
64 bytes from biorobpclab4.epfl.ch (128.178.148.174): icmp_seq=2 ttl=61 time=0.980 ms
64 bytes from biorobpclab4.epfl.ch (128.178.148.174): icmp_seq=3 ttl=61 time=1.21 ms
```
### Export ROS MASTER's URI on krock

```
biorob@odroid:~/krock-controller-farms$ export ROS_MASTER_URI=http://biorobpclab4:11311/
biorob@odroid:~/krock-controller-farms$ echo $ROS_MASTER_URI 
http://biorobpclab4:11311/

```

### Run robot_main 

robot_main
- enables the motors
- sets them to zero positon 
    - the robot arms at this stage move to get to their initial position
- starts the main loop 
    - once ther robot starts printing  the command and feedback values 
    `[02/11/21 12:20:31.-425]		 COMMANDED 	 FEEDBACK`
    `[02/11/21 12:20:31.-424]Motor 0:	0		-0.01304 `
    `[02/11/21 12:20:31.-424]Motor 1:	0		0.00537  `
    - the robot can se set into swimming(Triangle buton) or walking mode(cross button,X)
    - use the joyticks to move the robot  
        swimming: 1. Press triangle button to put the robot in swimming mode 2. use right joystick to swim 
        walking: 1. Press cross button,X to put the robot in walking mode 2. use left joystick to walk 
    - use the `options`button on dualshock to exit the loop

```bash
biorob@odroid:~/krock-controller-farms$ ./robot_main
```

```bash
biorob@odroid:~/krock-controller-farms$ ./robot_main 
2021-02-11, 12:20:15.210023 [info] - STARTING MAIN

[02/11/21 12:20:15.-85]2021-02-11, 12:20:15.211194 [info] - motor 0: 0	true
[02/11/21 12:20:15.-83]2021-02-11, 12:20:15.213179 [info] - motor 1: 1	true
[02/11/21 12:20:15.-82]2021-02-11, 12:20:15.214903 [info] - motor 2: 2	true
[02/11/21 12:20:15.-80]2021-02-11, 12:20:15.216460 [info] - motor 3: 3	true
[02/11/21 12:20:15.-78]2021-02-11, 12:20:15.218058 [info] - motor 4: 4	true
[02/11/21 12:20:15.-77]2021-02-11, 12:20:15.219460 [info] - motor 5: 5	true
[02/11/21 12:20:15.-75]2021-02-11, 12:20:15.221273 [info] - motor 6: 6	true
[02/11/21 12:20:15.-74]2021-02-11, 12:20:15.222799 [info] - motor 7: 7	true
[02/11/21 12:20:15.-72]2021-02-11, 12:20:15.224523 [info] - motor 8: 8	true
[02/11/21 12:20:15.-70]2021-02-11, 12:20:15.226283 [info] - motor 9: 9	true
[02/11/21 12:20:15.-69]2021-02-11, 12:20:15.227633 [info] - motor 10: 10	true
[02/11/21 12:20:15.-67]2021-02-11, 12:20:15.229543 [info] - motor 11: 11	true
[02/11/21 12:20:15.-65]2021-02-11, 12:20:15.231479 [info] - motor 12: 12	true
[02/11/21 12:20:15.-64]2021-02-11, 12:20:15.232736 [info] - motor 13: 13	true
[02/11/21 12:20:15.-62]2021-02-11, 12:20:15.234168 [info] - motor 14: 14	true
[02/11/21 12:20:15.-61]2021-02-11, 12:20:15.235478 [info] - motor 15: 15	true
[02/11/21 12:20:15.-60]2021-02-11, 12:20:15.236930 [info] - motor 16: 20	true
[02/11/21 12:20:15.-58]2021-02-11, 12:20:15.238147 [info] - motor 17: 21	true
[02/11/21 12:20:15.-57]2021-02-11, 12:20:15.239532 [info] - motor 18: 31	true
[02/11/21 12:20:15.-56]2021-02-11, 12:20:15.240758 [info] - motor 19: 32	true
[02/11/21 12:20:15.-54]2021-02-11, 12:20:15.242258 [info] - motor 20: 33	true
[02/11/21 12:20:15.-919]
[02/11/21 12:20:15.-919]Configuration Toggles: 
[02/11/21 12:20:15.-919]Use ROS: 		TRUE
[02/11/21 12:20:15.-919]Simulation: 		FALSE
[02/11/21 12:20:15.-918]High Level Controller: 	FALSE
[02/11/21 12:20:15.-918]Enable joystick: 	TRUE
[02/11/21 12:20:15.-918]Skip Hardware: 	FALSE
[02/11/21 12:20:15.-918]
[02/11/21 12:20:15.-917]
[02/11/21 12:20:15.-917]using ROS
[02/11/21 12:20:20.-706]1
[02/11/21 12:20:20.-706]calling: dxl_initialize
device open error: somethingRandom
[02/11/21 12:20:20.-705]calling: dxl_initialize
[02/11/21 12:20:20.-392]Port 1: Succeed to open USB2Dynamixel!
[02/11/21 12:20:20.-391]2021-02-11, 12:20:20.904981 [info] - Correct number of motors were found: 21
[02/11/21 12:20:21.-453]Init posture
[02/11/21 12:20:24.0-2]max torque: 800	max speed: 450
[02/11/21 12:20:24.-980]2021-02-11, 12:20:24.316645 [info] - written to a shm
...........
[02/11/21 12:20:31.-425]
[02/11/21 12:20:31.-425]State: 	INITIAL	
[02/11/21 12:20:31.-425]System time: 16.63
[02/11/21 12:20:31.-425]		 COMMANDED 	 FEEDBACK
[02/11/21 12:20:31.-424]Motor 0:	0		-0.01304
[02/11/21 12:20:31.-424]Motor 1:	0		0.00537
[02/11/21 12:20:31.-424]Motor 2:	0		0.002302
[02/11/21 12:20:31.-424]Motor 3:	0		0.01458
[02/11/21 12:20:31.-424]Motor 4:	0		0.01151
[02/11/21 12:20:31.-424]Motor 5:	0		-0.003836
[02/11/21 12:20:31.-423]Motor 6:	0		-0.0007672
[02/11/21 12:20:31.-423]Motor 7:	0		-0.00537
[02/11/21 12:20:31.-423]Motor 8:	0		-0.002302
[02/11/21 12:20:31.-423]Motor 9:	0		0.003836
[02/11/21 12:20:31.-423]Motor 10:	0		-0.006905
[02/11/21 12:20:31.-423]Motor 11:	0		-0.003836
[02/11/21 12:20:31.-423]Motor 12:	0		0.01151
[02/11/21 12:20:31.-422]Motor 13:	0		-0.003836
[02/11/21 12:20:31.-422]Motor 14:	0		-0.003836
[02/11/21 12:20:31.-422]Motor 15:	0		0.0007672
[02/11/21 12:20:31.-422]Motor 16:	0		0.002302
[02/11/21 12:20:31.-422]Motor 17:	0		0.006905
[02/11/21 12:20:31.-422]Motor 18:	0		-0.003836
[02/11/21 12:20:31.-422]Motor 19:	0		-0.00537
[02/11/21 12:20:31.-422]Motor 20:	0		-0.002302
[02/11/21 12:20:31.-399]Robot thread ending 
[02/11/21 12:20:32.-105]
[02/11/21 12:20:32.-104]----------
[02/11/21 12:20:32.-104]Leaving Loop
[02/11/21 12:20:32.-104]----------
[02/11/21 12:20:32.-104]
[02/11/21 12:20:32.-104]Starting RobotHandle out
[02/11/21 12:20:32.-94]Init posture
[02/11/21 12:20:34.-94]Set the initial posture back
[02/11/21 12:20:36.-94]SHUTING DOWN Robot Servo motors
[02/11/21 12:20:36.-968]Completed SHUTING DOWN Robot Servo motors
[02/11/21 12:20:37.-965]Destructor Controller
[02/11/21 12:20:37.-965]Destructor MPC
Segmentation fault (core dumped)
```

## Running CPG Controller

- CPG controller's main loop in real_krock.py 
- This does not require ROS functionality at the moment
- use the `options` button on dualshock to exit the loop

```bash
biorob@odroid:~/krock-controller-farms$ watch -c -n 5 tail -n 15 log/comm_slave/2021_02_11-13_34_47_cout.log 
biorob@odroid:~/krock-controller-farms$ python3 farms_scripts/real_krock.py 

pybullet build time: Feb  2 2021 09:54:35
Unable to init server: Could not connect: Connection refused
Unable to init server: Could not connect: Connection refused

(real_krock.py:1030): Gdk-CRITICAL **: 13:38:40.469: gdk_cursor_new_for_display: assertion 'GDK_IS_DISPLAY (display)' failed
[PYLOG-1030] 2021-02-11 13:38:40,903 - [INFO] - krock_options.py::390::get_krock_options():
Model SDF: /home/biorob/farms-integration/krock-sdf-model/krock.sdf
-
calling: dxl_initialize
device open error: somethingRandom
calling: dxl_initialize
Port 1: Succeed to open USB2Dynamixel!
2021-02-11, 13:38:41.262618 [info] - Correct number of motors were found: 21
Init posture
max torque: 800	max speed: 450
2021-02-11, 13:38:44.672968 [info] - written to a shm
Time:  11.484930276870728
Iterations:  10000
Command frequency:  870.7062001184979
Switching off the robot..
Starting RobotHandle out
Robot thread ending 
Init posture
Set the initial posture back
SHUTING DOWN Robot Servo motors
Completed SHUTING DOWN Robot Servo motors
[PYLOG-1030] 2021-02-11 13:39:01,299 - [INFO] - real_krock.py::170::<module>():
Total simulation time: 20.39709758758545 [s]
-

```

In case testing for a new parameters or new functionality, edit the `farms_scripts/real_krock.py` as shown below 
- timestep 
- total number of iterations for 10s 
- lnumpy of size 21 , with 0 or 1 (0 -> set the motor to 0 position 1-> command the motor according to cpg)
- slow down the execution loop

```python
    run_krock_cpg(
        timestep=0.00100, 
        n_iterations=1000*10, 
        motior_activate=activate, 
        slow_down=1) 

```
## Debugging 


biorob@odroid:~/krock-controller-farms$ pkill comm_slave_cyan 
biorob@odroid:~/krock-controller-farms$ watch -c -n 5 tail -n 15 log/comm_slave/2021_02_11-10_51_27_cout.log 
biorob@odroid:~/krock-controller-farms$ ls -l log/comm_slave/
total 116820
-rw-r--r-- 1 biorob biorob 51288167 Feb 11 12:30 2021_02_11-10_51_27_cerr.log
-rw-r--r-- 1 biorob biorob 68325376 Feb 11 12:30 2021_02_11-10_51_27_cout.log

- Top command 
```bash
biorob@odroid:~/krock-controller-farms$ top
%Cpu(s):  0.0 us,  0.1 sy,  0.0 ni, 99.2 id,  0.0 wa,  0.6 hi,  0.0 si,  0.0 st
MiB Mem :   1993.4 total,   1448.6 free,    136.8 used,    408.0 buff/cache
MiB Swap:      0.0 total,      0.0 free,      0.0 used.   1792.7 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                        
 2754 root      20   0       0      0      0 I   0.7   0.0   0:05.93 kworker/u16:2-phy0             
 2964 biorob    20   0    6808   2632   2060 R   0.7   0.1   0:00.07 top                            
   42 root       0 -20       0      0      0 I   0.3   0.0   0:01.07 kworker/6:0H-kblockd           
 2842 root      20   0       0      0      0 I   0.3   0.0   0:02.23 kworker/u16:0-phy0             
    1 root      20   0   33420   7860   5468 S   0.0   0.4   0:10.00 systemd                        
    2 root      20   0       0      0      0 S   0.0   0.0   0:00.02 kthreadd 
```

- after a few minutes comm_slave_cyan is restarted by startup service

```bash
biorob@odroid:~/krock-controller-farms$ top

top - 12:33:31 up 48 min,  1 user,  load average: 0.22, 0.38, 0.37
Tasks: 149 total,   1 running, 147 sleeping,   1 stopped,   0 zombie
%Cpu(s):  0.5 us,  2.6 sy,  0.0 ni, 94.9 id,  0.0 wa,  1.9 hi,  0.1 si,  0.0 st
MiB Mem :   1993.4 total,   1446.7 free,    136.2 used,    410.5 buff/cache
MiB Swap:      0.0 total,      0.0 free,      0.0 used.   1793.2 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                        
 2974 biorob    20   0   13260   1760   1568 S  26.5   0.1   0:12.96 comm_slave_cyan                
 2754 root      20   0       0      0      0 I   1.0   0.0   0:06.27 kworker/u16:2-events_unbound   
   42 root       0 -20       0      0      0 I   0.7   0.0   0:01.09 kworker/6:0H-kblockd           
 2786 root      20   0       0      0      0 I   0.7   0.0   0:05.25 kworker/u16:4-phy0             
 2966 biorob    20   0    6808   2612   2040 R   0.7   0.1   0:00.46 top                            
   10 root      20   0       0      0      0 I   0.3   0.0   0:03.59 rcu_preempt                    
 2842 root      20   0       0      0      0 I   0.3   0.0   0:02.69 kworker/u16:0
```
- follow the procedure as normal 


## Editing files on odroid 

- Use sshfs: `sshfs [user@]host:[remote_directory] mountpoint [options]`
- create a empty directory 
```bash
asgupta@biorobpclab4:/data/asgupta$ mkdir krock-U20 
asgupta@biorobpclab4:/data/asgupta$ cd krock-U20 
asgupta@biorobpclab4:/data/asgupta/krock-U20$ ls
asgupta@biorobpclab4:/data/asgupta/krock-U20$ 
```
- Mount Krock's desired folder on krock-U20 
```bash
asgupta@biorobpclab4:~$ sshfs biorob@128.179.184.68:/home/biorob/krock-controller-farms /data/asgupta/krock-U20/
biorob@128.179.184.68's password: 
asgupta@biorobpclab4:/data/asgupta/krock-U20$ ls
build            doc            install_controller.sh  robot_main
CMakeLists.txt   doxygenconfig  lib                    source
comm_slave_cyan  farms_scripts  log                    tests
config           include        README.md              ws_krock_controller.code-workspace
```
- Now you can edit the files as local host files!
- Remove mount using `asgupta@biorobpclab4:/data/asgupta$ umount krock-U20`




