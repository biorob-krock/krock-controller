# Joystick Operation and Pairing

Krock-2 is controlled with a Dual Shock 4 controller. 

## Controller Mapping

![DS4 Controller](./img/ds4controller.jpg "DS4 Controller")

Hardware 
- **Share**: Toggle motor power (red lights will blink on Dynamixels)
- **Start**: Start/exit robot executable. The robot won't start if the motors are not enabled. 

Gait selection 
- **X**: Initial position (laying flat)
- **Circle**: Walking gait 
	- **L joy** Walk forward/backward and strafing left/right
	- **R joy** Turn left/right 
- **Triangle** Swimming gait 

## Battery level 

You can test the battery level when the controller is plugged into Ubuntu with the terminal command: 

	upower -i  /org/freedesktop/UPower/devices/gaming_input_sony_controller_battery_1coa0ob8o8bo89o2f

## Setup 

### Pairing

Pairing is handled through the `bluetoothctl` standard on Linux. There as a couple controllers in lab: 

| Controller | MAC | Note | 
|----------------|--|--|
| DualShock4 #1 | `1C:A0:B8:8B:89:2F` | Buggy as of 2020-07-20 |
| DualShock4 #2 | `1C:A0:B8:8B:91:29` | Paired as of 2020-07-20|

[Bluetoothctl tutorial](https://computingforgeeks.com/connect-to-bluetooth-device-from-linux-terminal/). Auto connect should be [automatic](https://unix.stackexchange.com/questions/334386/how-to-set-up-automatic-connection-of-bluetooth-headset)

	sudo bluetoothctl 

Within the bluetoothctl interface: 

	scan on 
	trust 1C:A0:B8:8B:89:2F (MAC address of device)
	pair 1C:A0:B8:8B:89:2F

To double check if the device is **both** trusted and paired 

	info 78:44:aa:bb:cc:dd' (MAC address of device)
	paired-devices

For connecting upon start, make sure the configuration file: 

	sudo vim /etc/bluetooth/main.conf

Has autoenable crrectly set:  

	[Policy]
	AutoEnable=true

[Previously](https://biorob2.epfl.ch/wiki/projects:krock-2:robot:overview?s[]=bluetooth) was using `sidad` library but this appears to be solely for the PS3 controller. 

### Debugging 
Astha's notes: 
- `1C:A0:B8:8B:91:29`is the MAC address of the current controller.
- Solution: [hold down the ps & share botton together till it starts blinking to put the controller in the pairing mode](https://www.playstation.com/en-us/support/hardware/ps4-pair-dualshock-4-wireless-with-sony-xperia-and-android/)

- Error
```
[bluetooth]# pair 1C:A0:B8:8B:91:29
Attempting to pair with 1C:A0:B8:8B:91:29
Failed to pair: org.bluez.Error.ConnectionAttemptFailed
[CHG] Device 1C:A0:B8:8B:91:29 Connected: yes
[CHG] Device 1C:A0:B8:8B:91:29 UUIDs: 00001124-0000-1000-8000-00805f9b34fb
[CHG] Device 1C:A0:B8:8B:91:29 UUIDs: 00001200-0000-1000-8000-00805f9b34fb
[CHG] Device 1C:A0:B8:8B:91:29 ServicesResolved: yes
[CHG] Device 1C:A0:B8:8B:91:29 Paired: yes
```


### Testing the Joystick

The controller shows up as `/dev/input/js0` when properly connected to the computer.  

Alternatively, you can plug the DS4 controller in directly via USB cable and it should show up 

To test whether the controller is registering anything, you can print it out in terminal: 

```
jstest /dev/input/js0
```
If you are testing on a PC (i.e. not running headless)you can display the results with a graphical interface: 
 
```
sudo apt-get install jstest-gtk

jstest-gtk
```
For reading with the C++ ps3joy program, you can compile and run `krock-controller/lib/Comm/SimplePS3Shm` to get a printout. After that, the executable in `krock-controller/lib/Comm/Comm_slave` is the actual process that should be running the the background of the robot. 

Useful command for restarting the systemd process responsible for bluetooth when you adjusting things: 

```bash
systemctl restart robot_startup
```

### Troubleshooting

If joystick is not responding on the robot or the Webots simulation check the `USE_JOYSTICK` variable in the [GLOBAL.cfg](GLOBAL) or [GLOBAL_WEBOTS](../config/GLOBAL_WEBOTS.cfg) config files. 

---

Getting the error below probably means that you are "paired" but the device is not trusted: 

	[CHG] Device 1C:A0:B8:8B:89:2F Connected: yes
	[CHG] Device 1C:A0:B8:8B:89:2F Connected: no

If it is trusted, removed the device (`remove aa:bb:cc:dd:ee:ff`) then continue with the sequence described in this [superuser post](https://superuser.com/questions/1160934/bluetoothctl-connects-and-disconnects-repeatedly). 

## Known Issues

The controller was adapted from a Dual Shock 3 PS3 joystick, so we've put some remapping in [ps3joy.cpp](../lib/Comm/source/ps3joy.cpp), [ps3joy.hpp](../lib/Comm/header/ps3joy.hpp), [struct_defs.hpp](../lib/Comm/header/struct_defs.hpp) to patch this. 
