# Odroid-XU4 Clean Setup Ubuntu 18.04 (Minimal)

Below are notes on how to set up a clean installation of Ubuntu 18.04 on an ODROID computer. 

## Step 1: Download and Flash Ubuntu on SD card
[Ubunutu minimal 18.04.1 image](https://wiki.odroid.com/odroid-xu4/os_images/linux/ubuntu_4.14/20181203-minimal) (EU west download : `east.us.odroid.in/ubuntu_18.04lts/XU3_XU4_MC1_HC1_HC2/ubuntu-18.04.1-4.14-minimal-odroid-xu4-20181203.img.xz`) 

Flash the above image on the odroid (use [Etcher](https://www.balena.io/etcher/))

### Step 2: Network Interfaces
To set up the internet connection copy the following files : `/etc/network/interfaces`

```bash
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet dhcp
      wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
      post-up /usr/local/bin/updateip.sh
      pre-down /usr/local/bin/updateip.sh release
```

### Step 3: Add Network with WPA Supplicant
- `/etc/wpa_supplicant/wpa_supplicant.conf` (note: the password for the WiFi network is not supposed to be published online, please ask for it.)

```bash
ctrl_interface=DIR=/var/run/wpa_supplicant

update_config=1
country=CH
network={
        ssid="epfl"
        key_mgmt=WPA-EAP
        proto=WPA2
        eap=PEAP
        identity="biorob-robots@epfl.ch"
        password="................."
        anonymous_identity="anonymous@epfl.ch"
        phase2="auth=MSCHAPV2"
        ca_cert="/etc/ssl/certs/thawte_Primary_Root_CA.pem"
        subject_match="CN=radius.epfl.ch"
        priority=8
}
```

- `/usr/local/bin/updateip.sh`: (the hostname can be modified to be more meaningful and mail acquisition of the IP can be added)

### Step 4: Upload IP of the odroid automatically 

```bash 
#!/bin/bash

# file   updateip.sh
# brief  BIOROB dynamic IP notification script
# author alessandro.crespi@epfl.ch
# date   July 2019

# host name of the machine (mandatory to distinguish it in the list)
# please use something meaningful, generic meaningless names might be blocked
hostname="Krockbot-XU4-Ubuntu18"

# options for wget. Use -4 to force using IPv4 connectivity (if your host has
# IPv6 but you want the IPv4 address recorded), or -6 to force IPv6
wgetopt=

# set name of desired network interface (e.g. eth0, wlan1, etc.) if needed;
# by default the first active interface with an IP address and broadcast
# capability (i.e., not the loopback) is taken
interface=wlan0

# send a mail with the info to the listed addresses (separated by comma)
mail=

# extracts the local address of the network interface (or of the first one)
lip="/sbin/ifconfig $interface | grep -Eo "inet.+cast" | grep -Eo '([0-9]{1,3}[\.]){3}[0-9]{1,3}' | head -1"

# extracts the MAC address of the network interface (useful e.g. for debugging)
mac="/sbin/ifconfig $interface | grep -E "HWaddr|ether" | grep -Eo '([0-9a-f]{2}[\:]){5}[0-9a-f]{2}' | head -1"

# adapts request parameters depending if we are updating or releasing the IP
if [ "$1" == "release" ]; then
  urladd='&release_ip'
  textop="Releasing";
else
  urladd=''
  textop="Updating";
fi

# adds mail field if needed
if [ ! -z $mail ]; then
  urladd="${urladd}&mail=${mail}";
fi

# makes the actual request to update the address
echo -n "$textop IP address on server... "
wget ${wgetopt} -O - -q "http://biorob2.epfl.ch/utils/dynupd.php?hostname=${hostname}&internal_ip=${lip}&mac=${mac}${urladd}"
echo
```

## Debugging help

- Make the script executable : chmod +x /usr/local/bin/updateip.sh
- Start with ethernet to download first needed packages :
  - ip link set eth0 up
  - dhclient -v eth0
  - (Check if it works : ping 1.1.1.1)
- (if present : apt-get purge network-manager)
- Install needed packages (apt-get install x):
  - ifupdown
  - net-tools
  - wpasupplicant
  - iptables
  - htop
  - git
  - vim
- Upgrade : apt dist-upgrade
- Remove network manager (had a problem getting hung up on boot waiting for eth0 connection)
sudo apt-get purge network-manager
- Start the wifi : ifup wlan0
(Might be helpful : Alessandro used 'iwconfig', 'iwlist scanning', 'ps -C wpa_supplicant', 'ifdown wlan0', 'ifup wlan0')

Change the user with a safe password.

## ROS Installation 
ROS installation (http://wiki.ros.org/melodic/Installation/Ubuntu) -> ROS-base bare bones 

Create workspace :
    * mkdir -p ~/catkin_ws/src
    * cd ~/catkin_ws/
    * catkin_make
    * To avoid sourcing every new tab:
      * %%echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc%% 
      * %%source ~/.bashrc%%

## Cloning the ODROID 

Notes on cloning 

```bash
#!/bin/bash

# THIS SCRIPT IS NOT MEANT TO BE RUN 

# Adapting it for Mac ports 
# pv is the Pipe Viewer package, showing the rate of data exchange 
# dd is terminal reading/writing 

TAR_DISK=/dev/disk2
TAR_IMG=~/Desktop/krock1_properbackup_2019_14_2019.img

# diskutil list will show you the ports 
diskutil list 

# Read disk 
sudo dd if=$TAR_DISK | pv | sudo dd of=$TAR_IMG bs=1024

# Common error is "resource busy" 
# Found solution here: 
# 	https://raspberrypi.stackexchange.com/questions/9217/resource-busy-error-when-using-dd-to-copy-disk-img-to-sd-cardcloneODROID.sh

# Write image
sudo dd if=$TAR_IMG | pv | sudo dd of=$TAR_DISK bs=1024

####################
# Notes from Ale on files to be changed after closing
####################
# Be sure to change these files 
# /etc/hostname (change)
# /etc/hosts (replace host -> hostname must resolve to 127.0.0.1, remove unrelated old hostnames to prevent conflicts)
# /etc/udev/rules.d/70-persistent-net.rules (EMPTY IT NO DELETE)
# /etc/network/interfaces
```

