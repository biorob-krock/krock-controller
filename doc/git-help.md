# Good Practices

## Make and CMake 
- Please use CMakeLists.txt to compile your subproject or addd code 
- Please make use of the CMake's ability to invoke other CMakeLists from the high level CMakeLists.txt 
    - This will allow us to know what exactly does the project contain



## Git 

- **Contribute by opening up issues**: 
    - for bugs as well as new feature 
    - create a branch from the issue page, make sure to set the` source branch `correctly 
    - make changes to the branch (as any other branch)
    - come back to the issue's page, create a merge request to merge the changes back to a developer branch 
- **Make use of tags**: 
    - There are two kinds of tags. Try to use temporary tags to mark a checkpoint that we might need to come back during development. 


### Git commands 
 - `git pull --rebase`: 
    - before a push
    - or when there are changes to remote repository, while you were editing in your local 
    - This command is used to pull the remote changes and try to overlay the unpushed commits/changes on top of remote changes
### Commits
- Please add **only** source code to the repository. Please do not add files that can be **generated** by the code itself. For example:
    - **Log files**
    - **Build files**
- Its a good practive to always check which files have been staged (added) for final commit (changes that will be reflected once pushed)
    - Use `git status` to check the files modified but not staged for commit(red)  modified and staged(green) 
    - Use `git diff`, or vscode editor's git feature to check the changes between previous commit and current changes
    - Make sure to add files using `git add <filename>`, add files _one by one_.
    - Try **not** to use `git add .` , or any other command that stages all the files together, undesired files or changes can slip into the commits. 
    - Use `git status` to check the status of the repository, as frequenty as possible 
- It is good to use a one-word keyword to define the kind of commit. For example:
    - `git commit -m "[REFACTOR] .."`
    - `git commit -m "[BUG] .. "`
    - `git commit -m "[CONTROLLER] .."`
    - `git commit -m "[TRAVERSABILITY] .."`
    - `git commit -m "[MOTION-CAPTURE] .."`
    - `git commit -m "[ROSBAG] .."`
    - `git commit -m "[README] .."`
    - `git commit -m "[PARSING] .."`

## Optional 
Use milestones and labels to provide more information on the issues 
- Create milestones from 
    - [krock-controller milestones page](https://gitlab.com/biorob-krock/krock-controller/-/milestones)
    - Issues -> milestones -> new milestones
- Create labels if rquired 
    - [krock-controller labels page](https://gitlab.com/biorob-krock/krock-controller/-/labels)
    - Issues -> labels -> new labels

