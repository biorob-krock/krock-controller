"""[Generate the krock-controller directory graph]
"""
import os
from graphviz import Digraph

invisible_folder = set(['doxygen_html', 'doc', 'log', 'build', '.git', '.vscode', 'test-output','krock_results', 'data', 'backup'])
exc_level1_folders = set(['lib', 'config',])
invisible_files = set(['doxygenconfig', 'robot_main', 'comm_slave_cyan'])
exc_extensions = set(['md', 'out', 'code-workspace', 'xml'])

def generate_dir_graph(main_folder):
    global invisible_folder
    global exc_level1_folders
    global invisible_files
    global exc_extensions
    dot = Digraph(comment='Directory Structure', format='png')
    dict_no_traverse = set([])
    
    main_folder_name = main_folder.split('/')[-1]
    main_path = main_folder.split('/'+ main_folder_name)[0]

    for root, dirs, files in os.walk(main_folder):
        parent_folder_ns = root.split(main_path)[-1]
        parent_folder_set = set(parent_folder_ns.split('/'))
        folder_l = len(parent_folder_set)
        
        parent_folder_name = parent_folder_ns.split('/')[folder_l-1]
        
        if len(parent_folder_set) > 1:
            parent_node_name = parent_folder_ns.split('/')[folder_l-2] + "-" + parent_folder_name
        else:
            parent_node_name = parent_folder_name
            dot.node(parent_node_name, parent_node_name, shape='Mdiamond', color='orange', style='filled')

        parent_folder_set.discard('')
        if len(parent_folder_set.intersection(invisible_folder)):
            continue
        
        parent_folder_set.discard(parent_folder_name)
        if len(parent_folder_set.intersection(exc_level1_folders)):
            continue
        
        if parent_folder_name in dict_no_traverse:
            continue
        
        if parent_folder_name in exc_level1_folders:
            dict_no_traverse.union(set(dirs))
        
        for child_folder_name in dirs:
            if child_folder_name in invisible_folder:
                pass
            else:
                child_node_name = parent_folder_name + "-" + child_folder_name
                dot.node(child_node_name, child_folder_name, shape='box', color='lightblue', style='filled')
                dot.edge(parent_node_name, child_node_name)
        
        for f in files:
            m = f.split('.')
            if m[0] == '' or m[-1] in exc_extensions:
                continue
            if f in invisible_files:
                continue
            f_name = parent_folder_name +  "-" + f
            dot.node(f_name, f, color='lightgrey', style='filled')
            dot.edge(parent_node_name, f_name)         

    dot = dot.unflatten(stagger=10)
    dot.render(os.path.dirname(os.path.realpath(__file__)) + '/img/krock-dir-structure.gv', view=True) 

def main():
    this_file_path = os.path.dirname(os.path.realpath(__file__))
    # main_folder = '/data/asgupta/krock_project/farms-integration/krock-controller'
    # main_folder = '/data/asgupta/krock_project/krock-controller'
    main_folder = this_file_path.split('/doc')[0]
    generate_dir_graph(main_folder)

if __name__ == '__main__':
    main()