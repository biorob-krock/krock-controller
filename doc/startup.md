# Krock Startup 

Using krock-controller on its odroid computer after a clean install of ubuntu requires the following: 
- Setting services 
- Setting udev rules 
- Setting wifi configurations 

**Note**: At the time of writing only cyan is being used on krock. 

**Note**: command flow of `updateip.sh`: as this script is called on startup and shutdown of odroid computer 
    - The ip gets updated on the dynamic ip page with the correct host name with help of `/etc/network/interfaces` (check odroid-xu4.md for more details)
    - `updateip.sh` is been explicitly called during shutdown

## Services 
### Startup Service

- The `robot_startup.service` needs to be created under `/etc/systemd/system/`
- requires sudo rights for creating and editing 
- (astha added chmod +x permission, is probably not required)
- file abs path: `/etc/systemd/system/robot_startup.service`

```bash
[Unit]
Description=Automatic startup
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
Restart=on-failure
RestartSec=60
ExecStart=/usr/local/bin/robot_startup.sh
StartLimitInterval=350
StartLimitBurst=10
User=biorob

[Install]
WantedBy=multi-user.target
```

### Startup script called by the Startup service 

- The startup service will restart every 60s in case it fails for some reason
- The statup service will in turn call `/usr/local/bin/robot_startup.sh`
- The contents of the file are specified below 
- Change the file according to your needs

- Astha's Ubuntu20 script: 
        - running `/usr/local/bin/auto_connect_PS3_joy.sh &` is not required as comm_slave_cyan takes care of it 
        - Make sure `controller_folder="/home/biorob/path/to/krock-controller"` is correctly set 
        - `export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${controller_folder}/build/lib/vectornav` should already be exported by install_controller.sh script
```bash 
#!/bin/bash
 
service acpid start &
 
#/usr/local/bin/auto_connect_PS3_joy.sh &

## comm_slave with stdio logs saving ###
current_time=$(date "+%Y_%m_%d-%H_%M_%S")
controller_folder="/home/biorob/krock-controller"
log_folder="${controller_folder}/log/comm_slave"
# export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${controller_folder}/build/lib/vectornav
# Make sure the log folder exists
mkdir -p ${log_folder}
cout_log_file="${log_folder}/${current_time}_cout.log"
cerr_log_file="${log_folder}/${current_time}_cerr.log"
${controller_folder}/lib/Comm/Comm_slave/comm_slave 1>${cout_log_file} 2>${cerr_log_file}
## End of comm_slave with logging ###

### comm_slave without stdio logs saving ###
# ${controller_folder}/lib/Comm/Comm_slave/comm_slave
```

- Original script
```bash
#!/bin/bash
 
service acpid start &
 
#/usr/local/bin/auto_connect_PS3_joy.sh &
 
path/to/comm_slave_cyan 
 
# need to sleep a bit and kill and restart comm_slave_cyan process, otherwise Optoforce sensors hang (the reading gets stuc$
#sleep 2 && pkill comm_slave_cyan
#sleep 20 && /home/biorob/libkrock2/Comm/Comm_slave/comm_slave_cyan 
#/home/odroid/Libraries/Comm/SimplePS3shm/comm_master &
```

- Make sure to add the biorob user to following groups. This allows biorob to access serial ports
- Otherwise error will be thrown regarding 'LibSerial::OpenFailed'
- This also allows the comm_slave_cyan to run open shared memory without 'root' access 
        - thus avoiding previous issues with opening shared memory space by robot_main (run as biorob)
        - the issue was reported as 'shm permission denied'

```bash
sudo usermod -a -G dialout $USER
sudo usermod -a -G plugdev $USER
```

**Note**: While debugging comm_slave_cyan use the below commands 
```bash
# need to sleep a bit and kill and restart comm_slave_cyan process
sleep 2 && pkill comm_slave_cyan
sleep 20 && path/to/comm_slave_cyan 
```

### Shutdown Service 

- Managed by systemd saved in `/etc/systemd/system/robot_shutdown.service`
- The `robot_shutdown.service` needs to be created under `/etc/systemd/system/`
- requires sudo rights for creating and editing 
- (astha added chmod +x permission, is probably not required)

```bash 
[Unit]
Description=Remove dynamic IP address

[Service]
Type=oneshot
RemainAfterExit=true
ExecStop=/usr/local/bin/updateip.sh release

[Install]
WantedBy=multi-user.target
```

### Shutdown script called by the service 
- file abs path: `/usr/local/bin/updateip.sh` 
- this should already be present (from odroid-clean-setup-U20.md or odroid-xu4.md)

```bash 
#!/bin/bash

## \file   updateip.sh
## \brief  BIOROB dynamic IP notification script
## \author alessandro.crespi@epfl.ch
## \date   July 2019

# host name of the machine (mandatory to distinguish it in the list)
# please use something meaningful, generic meaningless names might be blocked
hostname="Krockbot-XU4-Ubuntu18"

# options for wget. Use -4 to force using IPv4 connectivity (if your host has
# IPv6 but you want the IPv4 address recorded), or -6 to force IPv6
wgetopt=

# set name of desired network interface (e.g. eth0, wlan1, etc.) if needed;
# by default the first active interface with an IP address and broadcast
# capability (i.e., not the loopback) is taken
interface=wlan0

# send a mail with the info to the listed addresses (separated by comma)
mail=

# extracts the local address of the network interface (or of the first one)
lip="/sbin/ifconfig $interface | grep -Eo "inet.+cast" | grep -Eo '([0-9]{1,3}[\.]){3}[0-9]{1,3}' | head -1"

# extracts the MAC address of the network interface (useful e.g. for debugging)
mac="/sbin/ifconfig $interface | grep -E "HWaddr|ether" | grep -Eo '([0-9a-f]{2}[\:]){5}[0-9a-f]{2}' | head -1"

# adapts request parameters depending if we are updating or releasing the IP
if [ "$1" == "release" ]; then
  urladd='&release_ip'
  textop="Releasing";
else
  urladd=''
  textop="Updating";
fi

# adds mail field if needed
if [ ! -z $mail ]; then
  urladd="${urladd}&mail=${mail}";
fi

# makes the actual request to update the address
echo -n "$textop IP address on server... "
wget ${wgetopt} -O - -q "http://biorob2.epfl.ch/utils/dynupd.php?hostname=${hostname}&internal_ip=${lip}&mac=${mac}${urladd}"
echo

```
### Debugging services 

- [Writing a service](https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6)
```
systemctl start required-service
systemctl enable required-service
```

- [Blog on systemcl commands](https://www.tecmint.com/list-all-running-services-under-systemd-in-linux/)
- List all the services enabled/disbaled `systemctl list-unit-files | grep enabled`
- List of running services `systemctl | grep running.`
**Enabled, doesn't mean it's running. And running doesn't mean it's enabled. They are two different things.**

- **Note**: Astha enabled both robot_startup and shutdown service
- **Note**: Make sure the services are enabled (startup service should be running)

## Udev rules

[Blog Udev](https://opensource.com/article/18/11/udev#:~:text=A%20udev%20rule%20must%20contain,this%20is%20a%20removable%20device.)

- Udev is the Linux subsystem that supplies your computer with device events.
- helps detects when you have things plugged into your computer, like a network card, external hard drives (including USB thumb drives), mouses, keyboards, joysticks and gamepads..
- Potentially useful utility
    - well-enough exposed that a standard user can manually script it to do things like performing certain tasks when a certain hard drive is plugged in.
- to get information about a device: `udevadm info -a -n /dev/<device>`

- Add Udev rules to allow peripheral devices to be associated with  SYMLINK names. 
- This allows the krock-controller code to open devices with help of SYMLINK names.

Steps for krock:

1. add a file with abs path `/etc/udev/rules.d/90-krock.rules`
    - note: `90-krock.rules` is the file name used in ubuntu18 krock sd 
    - not sure how much importance the name has

```bash
ACTION=="add", ATTRS{product}=="USB-RS485 Cable", SYMLINK+="usb2rs485"
ACTION=="add", ATTRS{product}=="FT232R USB UART", SYMLINK+="usb2rs485"
ACTION=="add", ATTRS{product}=="TTL232R", SYMLINK+="ttl_switch"
ACTION=="add", ATTRS{serial}=="FT0GY4UF", SYMLINK+="vectornav_vn100"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.1", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor2"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.2", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor1"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.3", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor3"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.4", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor4"
```

2. Also add the below line to `/etc/udev/rules.d/10-odroid.rules`
    - `ACTION=="add", SUBSYSTEM=="usb-serial", DRIVER=="ftdi_sio", ATTR{latency_timer}="1"`
    - This line was present in the ubuntu18 krock sd as well 
The final contents of the `10-odroid.rules` look like 

```bash
KERNEL=="mali",SUBSYSTEM=="misc",MODE="0777"
KERNEL=="mali0",SUBSYSTEM=="misc",MODE="0777"
KERNEL=="ump",SUBSYSTEM=="ump",MODE="0777"
KERNEL=="ttySAC0", SYMLINK+="ttyACM99"
KERNEL=="event*", SUBSYSTEM=="input", MODE="0777"
KERNEL=="CEC", MODE="0777"
KERNEL=="cec*", MODE="0777"
KERNEL=="amvideo", MODE="0666"
KERNEL=="amstream*", MODE="0666"

ACTION=="add", SUBSYSTEM=="usb-serial", DRIVER=="ftdi_sio", ATTR{latency_timer}="1"
```

3. run `sudo udevadm control --reload-rules` such that rules take effect 


#### Old rules from Ubuntu18 documentation 
Recognize hardware and give static name. For the USB camera, find id information

        lsusb

grab the device id and grep `idProduct` and `idVendor`

        lsusb -vd 046d:0892 | grep idProduct

to eventually define `etc/udev/rules/rules.d/10-local.rules` as: 

        ACTION=="add", ATTRS{idVendor}=="046d", ATTRS{idProduct}=="0892", SYMLINK+="video-pool-exp"

Reload udev rules

        sudo udevadm control --reload-rules

Which will now show up as `/dev/video-pool-exp`. 

Additionaly, located on CYAN computer in `/etc/udev/rules.d/20-local.rules` that let us recognize sensors and the other ODROID

```
ACTION=="add", ATTRS{product}=="CP2102 USB to UART Bridge Controller", SYMLINK+="xbee_uart"
ACTION=="add", ATTRS{product}=="USB-RS485 Cable", SYMLINK+="usb2rs485"
ACTION=="add", ATTRS{product}=="FT232R USB UART", SYMLINK+="usb2rs485"
ACTION=="add", ATTRS{product}=="TTL232R", SYMLINK+="ttl_switch"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.1", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor2"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.2", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor1"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.3", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor3"
ACTION=="add", SUBSYSTEM=="tty", ATTRS{devpath}=="*.4", ATTRS{manufacturer}=="OptoForce", SYMLINK+="optfor4"
```

## WIFI configuration

Copy the contents of the `etc/wpa_supplicant/wpa_supplicant.conf` from old sd card :P

## Old notes from Matt
To connect to EPFL\'s wifi, 2 files should be modified. The first file
is `/etc/network/interfaces`, which configures the wireless dongle that is
plugged into Odroid.

```bash
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
#source-directory /etc/network/interfaces.d

                                                                                                                      

# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug eth2
iface eth2 inet static
        address 192.168.1.191

auto wlan#
iface wlan# inet dhcp
        wpa-conf /etc/wpa_supplicant.conf
        post-up /usr/local/bin/updateip.sh
        pre-down /usr/local/bin/updateip.sh release
```


**The wlan\# should be replaced** with the current name of the wifi
interface. The name can be retrieved by typing the command
`%%ifconfig%%`. The name is linked to wifi dongle, so every time it is
replaced, the interface name should be updated (e.g. from wlan0 to
wlan1)

The wifi networks the robot should connect to are specified inside
/etc/wpa\_supplicant.conf. The following example allows robot to connect
to EPFL wifi (lowest priority), mini portable router
(dlink\_portable\_robohub) and biorob-local2 router (highest priority).


    ctrl_interface=DIR=/var/run/wpa_supplicant

    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    country=CH
    network={
            ssid="epfl"
            key_mgmt=WPA-EAP
            proto=WPA2
            eap=PEAP
            identity="biorob-robots@epfl.ch"
            password="..........."
            anonymous_identity="anonymous@epfl.ch"
            phase2="auth=MSCHAPV2"
            ca_cert="/etc/ssl/certs/thawte_Primary_Root_CA.pem"
            subject_match="CN=radius.epfl.ch"
            priority=8
    }

    network={
            priority=800
            ssid="biorob-local2"
            proto=RSN
            key_mgmt=WPA-PSK
            pairwise=CCMP
            auth_alg=OPEN
            psk=0057fee9346f85538be102e1da50c992b654ad3c6a319173d4a2f1d889b13cc2
    }


The robot\'s IP is automatically updated on biorob\'s website by the
script /usr/local/bin/updateip.sh. The script can also send IP via email
(needed to specify in the script).


## Magenta 

Magenta operation is using an oudated way to run startup using ` /etc/rc.local/`: 

```bash 
    #!/bin/sh -e
    #
    # rc.local
    #
    # This script is executed at the end of each multiuser runlevel.
    # Make sure that the script will "exit 0" on success or any other
    # value on error.
    #
    # In order to enable or disable this script just change the execution
    # bits.
    #
    # By default this script does nothing.
     
     
     
     
    sudo echo "40 50 70 95" > /sys/devices/odroid_fan.13/fan_speeds
    sudo echo "50 70 80" > /sys/devices/odroid_fan.13/temp_levels
     
    #/usr/local/bin/auto_connect_PS3_joy.sh &
    #/home/odroid/Libraries/XBEE_joy_comm/xbee_main &
     
    #sudo service acpid start &
     
    /usr/local/bin/robot_startup_script.sh &
     
    exit 0
```

The startup script called is `usr/local/bin/robot_startup_script.sh`: 

```bash
    #!/bin/bash
     
    service acpid start &
     
    /usr/local/bin/auto_connect_PS3_joy.sh &
     
    sleep 20
     
    /home/odroid/Libraries/Comm/Comm_master/comm_master &
    sleep 2 && pkill comm_master
    sleep 2 && /home/odroid/Libraries/Comm/Comm_master/comm_master &
     
    #sleep 20
    #/home/odroid/PROJECTS/cameraSystem/camera_main &
     
    #/usr/local/bin/network-monitor.sh &
     
    #/home/odroid/Libraries/XBEE_joy_comm/xbee_main &
```