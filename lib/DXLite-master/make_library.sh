#!/bin/bash



LIB_HOME = /home/odroid/Libraries



INCLUDE_DIRS = -I$(LIB_HOME)/DXLite-master -I$(LIB_HOME)/DXLite-master/dxsystem 



CXX = g++
CXXFLAGS += -std=c++11	-O3  -DLINUX  -Wall -fmessage-length=0 `pkg-config --cflags opencv` $(INCLUDE_DIRS)
LIBS += -lpthread -lrt -ljpeg  `pkg-config --libs opencv` -lserial -lboost_system -lboost_thread


SOURCES = \
    $(LIB_HOME)/DXLite-master/dxsystem/userinputfn.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/setup.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/dxsystem.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/dxsinglemotor.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/dxsetupmotor.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/dxmotorsystem.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/dmodeldetails.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/additionaltools.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/dxllibclass/dynamixelclass.cpp \
    $(LIB_HOME)/DXLite-master/dxsystem/dxllibclass/dxlhal.cpp



OBJS = $(SOURCES:.cpp=.o)

$(CXX) -o $(TARGET) $(OBJS)  $(LIBS)