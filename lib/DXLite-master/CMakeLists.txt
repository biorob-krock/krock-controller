cmake_minimum_required(VERSION 3.10)

################################################################################
# Project definition ###########################################################
################################################################################
project("DXLite")

################################################################################
# Get dependencies #############################################################
################################################################################
# pthread
set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)
find_package(Threads REQUIRED)

# Boost
find_package(Boost COMPONENTS thread system REQUIRED)
include_directories(${Boost_INCLUDE_DIR})

# Eigen
# find_package(Eigen3 REQUIRED)

# Real-time library
find_library(LIBRT rt)

# JPEG library
# find_package(jpeg REQUIRED)

################################################################################
# Source files #################################################################
################################################################################

# Include header folder (.h)
include_directories(dxsystem)


list(APPEND SOURCES "dxsystem/userinputfn.cpp"
	"dxsystem/setup.cpp"
	"dxsystem/dxsystem.cpp"
	"dxsystem/dxsinglemotor.cpp"
	"dxsystem/dxsetupmotor.cpp"
	"dxsystem/dxmotorsystem.cpp"
	"dxsystem/dmodeldetails.cpp"
	"dxsystem/additionaltools.cpp"
	"dxsystem/dxllibclass/dynamixelclass.cpp"
	"dxsystem/dxllibclass/dxlhal.cpp")
################################################################################
# Compilation settings #########################################################
################################################################################


# TODO: compilation instructions may not be a one to one translation of the
#       makefile. Need to check for them. 
# Use C++11 standard
set_property(GLOBAL PROPERTY CXX_STANDARD 11)

# Activate all warnings
string(APPEND CMAKE_CXX_FLAGS " -Wall")

# Disable some warnings :(
# TODO: solve the issues instead of hiding them
string(APPEND CMAKE_CXX_FLAGS " -Wno-unused-variable" " -Wno-write-strings"
                        " -Wno-strict-overflow" " -Wno-unused-but-set-variable"
                        " -Wno-unused-value" " -Wno-misleading-indentation"
                        " -Wno-deprecated" " -Wno-sign-compare")

string(APPEND CMAKE_CXX_FLAGS " -O3")
add_library(dxlite SHARED ${SOURCES})
