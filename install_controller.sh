# !/bin/bash


# Assumption: 
# 1. Git would already be installed 
# 2. the controller code has been cloned
# 3. Run the script with source comand 'source install_controller.sh'
#    if not, then source the bashrc manually on the open terminal to 
#    see its effects on the open terminal 
# 4. Make sure to check that the following lines can be found in the 
#    bashrc file after the script is successful 
#       1. source /opt/ros/melodic/setup.bash
#       2. export LD_LIBRARY_PATH=.../casadi/..


# Color of the output
C_RED='\033[0;31m'
C_GREEN='\033[0;32m'
C_YELLOW='\033[0;33m'
C_ORANGE='\033[1;33m'
C_BLUE='\033[1;34m'
C_PURPLE='\033[1;35m'
C_CYAN='\033[1;36m'
C_NC='\033[0m'

FOR_WEBOTS=ON           # Build for webots
WITH_ROS=ON             # Build with ros
JOB_THREADS=8           # No of jobs threads for make
VAR_USER=''             # '' if no sudo user rights else 'sudo'
ROS_INSTALL_FLAG='n'    # 'Y' if webots ros be installed else 'n'
WEBOTS_INSTALL_FLAG='n' # 'Y' if webots should be installed else 'n'
ASSUME_YES=''           # '--assume-yes' if yes for all package install else '' 
DEBUG_MODE=OFF          # Build the cmake in debug mode

IFS="="
for cmd_index in "$@"; do
    read -a cmd_input <<< "$cmd_index"
    if [ ${cmd_input[0]} == '--FOR_WEBOTS' ]; then
        FOR_WEBOTS=${cmd_input[1]}
    elif [ ${cmd_input[0]} == '--WITH_ROS' ]; then
        WITH_ROS=${cmd_input[1]}
    elif [ ${cmd_input[0]} == '--JOB_THREADS' ]; then 
        JOB_THREADS=${cmd_input[1]}
    elif [ ${cmd_input[0]} == '--VAR_USER' ]; then 
        VAR_USER='sudo'
    elif [ ${cmd_input[0]} == '--INSTALL_ROS' ]; then 
        ROS_INSTALL_FLAG='Y'
    elif [ ${cmd_input[0]} == '--INSTALL_WEBOTS' ]; then 
        WEBOTS_INSTALL_FLAG='Y'
    elif [ ${cmd_input[0]} == '--ASSUME_YES' ]; then 
        ASSUME_YES='--assume-yes'
    elif [ ${cmd_input[0]} == '--DEBUG_MODE' ]; then 
        DEBUG_MODE=${cmd_input[1]}
    fi
done
IFS=''
echo -e $C_BLUE "FOR_WEBOTS="           ${C_YELLOW} ${FOR_WEBOTS}           ${C_NC} "Build for webots"
echo -e $C_BLUE "WITH_ROS="             ${C_YELLOW} ${WITH_ROS}             ${C_NC} "Build with ros"
echo -e $C_BLUE "JOB_THREADS="          ${C_YELLOW} ${JOB_THREADS}          ${C_NC} "No of jobs threads for make"
echo -e $C_BLUE "VAR_USER="             ${C_YELLOW} ${VAR_USER}             ${C_NC} "'' if no sudo user rights else 'sudo'"
echo -e $C_BLUE "ROS_INSTALL_FLAG="     ${C_YELLOW} ${ROS_INSTALL_FLAG}     ${C_NC} "'Y' if webots ros be installed else 'n'"
echo -e $C_BLUE "WEBOTS_INSTALL_FLAG="  ${C_YELLOW} ${WEBOTS_INSTALL_FLAG}  ${C_NC} "'Y' if webots should be installed else 'n'"
echo -e $C_BLUE "DEBUG_MODE="           ${C_YELLOW} ${DEBUG_MODE}           ${C_NC} "Cmake debug mode"
# Names of all the dependent packages that needs to be installed 
# This script uses ASSUME_YES and VAR_USER to correctly run apt 
# commands

libnames=(
    "libboost-all-dev"
    "libserial-dev"
    "libdlib-dev"
    "libeigen3-dev"
    "coinor-libipopt-dev"
    "gcc"
    "g++"
    "gfortran"
    "git"
    "cmake"
    "liblapack-dev"
    "pkg-config"
    )


# ##################################################################
# ############ Install Ros  ########################################
# ##################################################################
# Ask user whether to install ROS 
# read -p "Install Ross? (Y/n): " ROS_INSTALL_FLAG
if [ $ROS_INSTALL_FLAG == 'n' ]
then
    echo -e $C_ORANGE 'ROS is not being installed' $C_NC
else
    # Find the Ros and Ubuntu version name
    UBUNTU_RELEASE_NAME=$(eval "lsb_release -sc")
    if [ $UBUNTU_RELEASE_NAME == 'bionic' ]
    then
        ROS_RELESE_NAME='melodic'
    elif [ $UBUNTU_RELEASE_NAME == 'focal' ]
    then
        ROS_RELESE_NAME='noetic'
    else
        echo -e $C_BLUE  $UBUNTU_RELEASE_NAME "${C_YELLOW}: No Ros instruction found ${C_NC}"
        exit 1
    fi
    ROS_PACKAGE_NAME="ros-${ROS_RELESE_NAME}-desktop"
    
    if (dpkg -s $ROS_PACKAGE_NAME > /dev/null)
    then
        echo -e $C_BLUE $ROS_PACKAGE_NAME "${C_GREEN}: Already installed ${C_NC}"
    else
        echo -e $C_BLUE $ROS_PACKAGE_NAME "${C_YELLOW}: Installing ${C_NC}"

        # ros install instruction as given in their guide 
        $VAR_USER sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
        $VAR_USER apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
        $VAR_USER apt-get update
        $VAR_USER apt-get --assume-yes install ros-${ROS_RELESE_NAME}-desktop
        $VAR_USER apt-get --assume-yes install ros-${ROS_RELESE_NAME}-tf2-eigen ros-${ROS_RELESE_NAME}-tf2 ros-${ROS_RELESE_NAME}-ros

        # Export environment variables if Ros correctly installed
        if (dpkg -s $ROS_PACKAGE_NAME > /dev/null)
        then 
            echo "source /opt/ros/${ROS_RELESE_NAME}/setup.bash" >> ~/.bashrc
            source /opt/ros/${ROS_RELESE_NAME}/setup.bash
            echo -e $C_BLUE $ROS_PACKAGE_NAME "${C_GREEN}: Installed ${C_NC}"
        else
            echo -e $C_BLUE  $ROS_PACKAGE_NAME "${C_RED}: Not Installed ${C_NC}"
            exit 1
        fi
    fi
fi

# ##################################################################
# ############ Install Webots  #####################################
# ##################################################################
# read -p "Install Ross? (Y/n): " WEBOTS_INSTALL_FLAG
if [ $WEBOTS_INSTALL_FLAG == 'n' ]
then
    echo -e $C_ORANGE 'Webots is not being installed' $C_NC
else
    WEBOTS_PACKAGE_NAME='webots'
    if (dpkg -s $WEBOTS_PACKAGE_NAME > /dev/null)
    then
        echo -e $C_BLUE $WEBOTS_PACKAGE_NAME "${C_GREEN}: Already installed ${C_NC}"
    else
        echo -e $C_BLUE $WEBOTS_PACKAGE_NAME "${C_YELLOW}: Installing ${C_NC}"
        #  Get Cyberbotics.asc signature file to authenticate webots 
        wget -qO- https://cyberbotics.com/Cyberbotics.asc | sudo apt-key add -
        # Add Cyberbotics repository to apt package manager 
        $VAR_USER apt-add-repository 'deb https://cyberbotics.com/debian/ binary-amd64/'
        # Update apt-get to reflect new additions
        $VAR_USER apt-get update
        # Install webots 
        $VAR_USER apt-get --assume-yes install webots
        if (dpkg -s $WEBOTS_PACKAGE_NAME > /dev/null)
        then 
            echo -e $C_BLUE $WEBOTS_PACKAGE_NAME "${C_GREEN}: Installed ${C_NC}"
        else
            echo -e $C_BLUE $WEBOTS_PACKAGE_NAME "${C_RED}: Not Installed ${C_NC}"
            exit 1
        fi
    fi
fi

# ##################################################################
# ############ Loop over each package and try installing ###########
# ##################################################################
echo -e $C_ORANGE '# Installing ...' $C_NC
for PACKAGE in ${libnames[@]}; do
    echo 
    if (dpkg -s $PACKAGE > /dev/null)
    then
        # package has been installed 
        echo -e "${C_BLUE} $PACKAGE ${C_YELLOW} already installed ${C_NC}"
    else
        # package is not installed, thus attempt to install it 
        echo -e "${C_BLUE} $PACKAGE ${C_YELLOW} Not Found, Intalling ${C_NC}"
        $VAR_USER apt-get $ASSUME_YES install $PACKAGE
    fi
done

# ##################################################################
# ############ Loop over each package and give status ##############
# ##################################################################
echo
echo '# Status'
SUCCESS_FLAG=1
for PACKAGE in ${libnames[@]}; do
    if (dpkg -s $PACKAGE > /dev/null)
    then
        # package has been installed 
        echo -e $C_BLUE $PACKAGE "${C_GREEN}: Installed ${C_NC}"
    else
        # package is not installed
        echo -e $C_BLUE $PACKAGE "${C_RED}: Not Installed ${C_NC}"
        SUCCESS_FLAG=0
    fi
done

if [ $SUCCESS_FLAG == 0 ]
then
    # exit with a error(1) if all the dependencies were not installed 
    exit 1
fi

# ##################################################################
# ########### Krock-Controller Build Commands ######################
# ##################################################################
# Note: remove the export command from the bash folder when casadi 
#       is not being used.
# make build folder if it does not exist
[ ! -d "./build" ] && mkdir build 
cd build

# run cmake and make commands
cmake .. -DFOR_WEBOTS=$FOR_WEBOTS -DWITH_ROS=$WITH_ROS -DDEBUG_MODE=$DEBUG_MODE
make -j $JOB_THREADS

CASADI_PATH='/lib/casadi/lib'
CASADI_ABSOLUTE_PATH="${PWD}${CASADI_PATH}"
CASADI_ALREADY_ADDED_FLAG=0

VECTORNAV_PATH='/lib/vectornav'
VECTORNAV_ABSOLUTE_PATH="${PWD}${VECTORNAV_PATH}"
VECTORNAV_ALREADY_ADDED_FLAG=0

################################# LD_LIBRARY_PATH ##################################
# Take the current LD_LIBRARY_PATH from environment variable 
# and split string with ':' delimiter
IFS=':'
read -ra arrIN <<< "$LD_LIBRARY_PATH" 

# Check if the CASADI_ABSOLUTE_PATH is already present in the LD_LIBRARY_PATH
for LD_LIB_NAMES in ${arrIN[@]}; do
    if [ $LD_LIB_NAMES == $CASADI_ABSOLUTE_PATH ]
    then
        # casadi path has already been added
        echo -e "casadi path already added: ${C_PURPLE} $CASADI_ABSOLUTE_PATH ${C_NC}"
        CASADI_ALREADY_ADDED_FLAG=1
        echo -e "LD_LIBRARY_PATH: ${C_PURPLE} $LD_LIBRARY_PATH ${C_NC}"
    fi
    if [ $LD_LIB_NAMES == $VECTORNAV_ABSOLUTE_PATH ]
    then
        # casadi path has already been added
        echo -e "vectornav path already added: ${C_PURPLE} $VECTORNAV_ABSOLUTE_PATH ${C_NC}"
        VECTORNAV_ALREADY_ADDED_FLAG=1
        echo -e "LD_LIBRARY_PATH: ${C_PURPLE} $LD_LIBRARY_PATH ${C_NC}"
    fi
done

if [ $CASADI_ALREADY_ADDED_FLAG == 0 ]
then
    # casadi has not been added yet
    echo 'casadi not found in $LD_LIBRARY_PATH, exporting'
    # export casadi's lib folder in lib
    echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD$CASADI_PATH" >> ~/.bashrc
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD$CASADI_PATH
    echo -e "LD_LIBRARY_PATH: ${C_PURPLE} $LD_LIBRARY_PATH ${C_NC}"
fi

if [ $VECTORNAV_ALREADY_ADDED_FLAG == 0 ]
then
    # casadi has not been added yet
    echo 'vectornav not found in $LD_LIBRARY_PATH, exporting'
    # export casadi's lib folder in lib
    echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD$VECTORNAV_PATH" >> ~/.bashrc
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD$VECTORNAV_PATH
    echo -e "LD_LIBRARY_PATH: ${C_PURPLE} $LD_LIBRARY_PATH ${C_NC}"
fi


####################################################################################
################################# PYTHONPATH #######################################

BUILD_ALREADY_ADDED_FLAG=0
# This is probably not a good method!
IFS=':'
read -ra arrIN <<< "$PYTHONPATH" 
for PY_LIB_NAMES in ${arrIN[@]}; do
    if [ $PY_LIB_NAMES == $PWD ]
    then
        # casadi path has already been added
        echo -e "build path already added: ${C_PURPLE} $PWD ${C_NC}"
        BUILD_ALREADY_ADDED_FLAG=1
        echo -e "PYTHONPATH: ${C_PURPLE} $PYTHONPATH ${C_NC}"
    fi
done

if [ $BUILD_ALREADY_ADDED_FLAG == 0 ]
then
    # casadi has not been added yet
    echo 'krock-controller/build not found in $PYTHONPATH, exporting'
    # export casadi's lib folder in lib
    echo "export PYTHONPATH=$PYTHONPATH:$PWD" >> ~/.bashrc
    # exporting to reflect changes in the current shell and avoid sourcing 
    export PYTHONPATH=$PYTHONPATH:$PWD
    # printing out what has been exported
    echo -e "PYTHONPATH: ${C_PURPLE} $PYTHONPATH ${C_NC}"
fi

echo -e "$C_BLUE  If you use zsh, kindly export the paths to .zshrc ${C_GREEN}: Installed ${C_NC}"

# exit from the build folder 
cd ..

echo -e "$C_BLUE Build complete ${C_NC}"


